package fr.afcepf.ai107.nomads.partenaire.ibusiness;

import java.io.EOFException;
import java.util.List;

import fr.afcepf.ai107.nomads.entities.ContratPartenaire;
import fr.afcepf.ai107.nomads.entities.IndisponibilitePartenaire;
import fr.afcepf.ai107.nomads.entities.InterventionReelle;
import fr.afcepf.ai107.nomads.entities.MotifFinContratPartenaire;
import fr.afcepf.ai107.nomads.entities.MotifIndisponibilitePartenaire;
import fr.afcepf.ai107.nomads.entities.Partenaire;
import fr.afcepf.ai107.nomads.entities.Prestation;
import fr.afcepf.ai107.nomads.entities.TypePartenaire;

public interface PartenaireIBusiness {

	Partenaire connection(String login, String password);
	List<TypePartenaire> getTypePartenaire();
	Partenaire ajouterPartenaire(Partenaire partenaire);
	TypePartenaire getTypePartenaireById(int id);	
	Partenaire modifierPartenaire(Partenaire partenaireRecupere);
	Partenaire getInfosPartenaireById(int partenaireid);
	ContratPartenaire ajouterContratPartenaire(ContratPartenaire nouveauContrat);
	ContratPartenaire desactiverPartenaire(ContratPartenaire contratPartenaire);
	ContratPartenaire getContratPartenaireById(Partenaire partenaire);
	List<String> getAllMotifsFinContratPartenaire();
	MotifFinContratPartenaire getMotifByLibelleMotif(String motif);
	IndisponibilitePartenaire updateIndispoPart(IndisponibilitePartenaire indispoPart);
	IndisponibilitePartenaire getIndispoEnCoursPart(Partenaire partenaire) throws Exception;
	IndisponibilitePartenaire ajouterIndispoPart(IndisponibilitePartenaire indispoPart);
	List<Prestation> getListePrestaAVenirByIdPartenaire(int partenaireId);
	List<Prestation> getListePrestaPasseByIdPartenaire(int partenaireId);
	List<Partenaire> getAllPartenaires();
	List<Partenaire> getAllPartenaireInactifs();
	List<Partenaire> getAllPartenaireActifs();
	List<Partenaire> getAllPartenaireTypePart(String typePartenaire);
	boolean verifierDisponiblePartenaire(Partenaire partenaire);
	List<IndisponibilitePartenaire> getHistoriqueIndispoByPart(Partenaire partenaire) throws Exception;
	List<String> getAllMotifsIndispoPartenaire();
	MotifIndisponibilitePartenaire getMotifIndispoByLibelleMotifIndispo(String motif);
	List<InterventionReelle>getAllInterventionAvenirByPArtenaireBu(int partenaireId);
    List<InterventionReelle>getAllInterventionPasseesByPArtenaireBu(int partenaireId);
    TypePartenaire getTypePartenaireByIdIntervention(Integer idInterv);
	List<Partenaire> getPartenairesActifsAjD();
	List<Partenaire> getNouveauxPartenaires1Month();
	List<Partenaire> getDepartPartenaires1Mois();



}
