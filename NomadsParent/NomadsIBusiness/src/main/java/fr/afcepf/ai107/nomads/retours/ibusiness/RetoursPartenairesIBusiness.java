package fr.afcepf.ai107.nomads.retours.ibusiness;

import java.util.List;

import fr.afcepf.ai107.nomads.entities.InterventionReelle;
import fr.afcepf.ai107.nomads.entities.Masseur;
import fr.afcepf.ai107.nomads.entities.Partenaire;
import fr.afcepf.ai107.nomads.entities.RetourExperiencePartenaire;

public interface RetoursPartenairesIBusiness {

	public List<RetourExperiencePartenaire> getCurrentMonthRetourPartenaire();
	RetourExperiencePartenaire ajouterUnRetour(RetourExperiencePartenaire ret);
	List<RetourExperiencePartenaire> getRetourByIntervEtMasseur(InterventionReelle interv, Partenaire part);
}
