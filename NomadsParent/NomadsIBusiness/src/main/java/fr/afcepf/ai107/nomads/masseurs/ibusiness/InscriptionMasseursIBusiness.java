package fr.afcepf.ai107.nomads.masseurs.ibusiness;

import java.util.Date;
import java.util.List;

import fr.afcepf.ai107.nomads.entities.InscriptionMasseurs;
import fr.afcepf.ai107.nomads.entities.InterventionReelle;
import fr.afcepf.ai107.nomads.entities.Masseur;

public interface InscriptionMasseursIBusiness {

	List<Masseur> getMasseursInscritsPourUneIntervention(Integer idIntervention);
	List<InscriptionMasseurs>getInscriptionMasseurFuturesByIdMasseur(Integer idMasseur, Date dateDesinscri);
	void mettreAJourInscriptionMasseur (InscriptionMasseurs inscription);
	InscriptionMasseurs GetInscriptionByMasseurAndInterv (Masseur masseur, InterventionReelle interv);
}
