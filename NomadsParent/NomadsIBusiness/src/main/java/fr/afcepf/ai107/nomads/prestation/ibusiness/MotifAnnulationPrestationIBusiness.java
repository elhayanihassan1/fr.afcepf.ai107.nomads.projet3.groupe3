package fr.afcepf.ai107.nomads.prestation.ibusiness;

import java.util.List;

import fr.afcepf.ai107.nomads.entities.InterventionReelle;
import fr.afcepf.ai107.nomads.entities.MotifAnnulationPrestation;

public interface MotifAnnulationPrestationIBusiness {

	List<String> getListeMotifsAnnulationPrestation();
	InterventionReelle updateInterventionAvecMotif(InterventionReelle interv);
	MotifAnnulationPrestation getMotifByLibelleMotif(String motif);
}
