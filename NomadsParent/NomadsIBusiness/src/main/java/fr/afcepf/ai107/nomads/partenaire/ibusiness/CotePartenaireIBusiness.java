package fr.afcepf.ai107.nomads.partenaire.ibusiness;

import fr.afcepf.ai107.nomads.entities.InterventionReelle;
import fr.afcepf.ai107.nomads.entities.Prestation;

public interface CotePartenaireIBusiness {
	
	
	void ajoutDemandeDevisPartenaire(Prestation prestation);
	void AjoutIntervention(InterventionReelle interventionReelle);
}
