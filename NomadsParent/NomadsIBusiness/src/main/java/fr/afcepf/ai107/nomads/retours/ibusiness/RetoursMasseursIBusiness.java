package fr.afcepf.ai107.nomads.retours.ibusiness;

import java.util.List;
import java.util.Set;

import fr.afcepf.ai107.nomads.entities.InterventionReelle;
import fr.afcepf.ai107.nomads.entities.Masseur;
import fr.afcepf.ai107.nomads.entities.RetourExperienceMasseur;

public interface RetoursMasseursIBusiness {
	
	List<RetourExperienceMasseur> getCurrentMonthRetourMasseur();
	RetourExperienceMasseur ajouterUnRetour(RetourExperienceMasseur monRetour);
	List<RetourExperienceMasseur> getRetourExByIdIntervention(InterventionReelle intervention, Masseur masseur);
}
