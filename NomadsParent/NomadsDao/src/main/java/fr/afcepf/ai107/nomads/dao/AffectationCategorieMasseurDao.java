package fr.afcepf.ai107.nomads.dao;

import java.util.List;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import fr.afcepf.ai107.nomads.entities.AffectationCategorieMasseur;
import fr.afcepf.ai107.nomads.idao.AffectationCategorieMasseurIDao;

@Remote(AffectationCategorieMasseurIDao.class)
@Stateless
public class AffectationCategorieMasseurDao extends GenericDao<AffectationCategorieMasseur>
		implements AffectationCategorieMasseurIDao {

	@PersistenceContext(unitName = "NomadsPU")
	private EntityManager em;
	
	private final String GET_AFFECTATION_BY_ID_MASS ="SELECT acm FROM AffectationCategorieMasseur acm INNER JOIN FETCH acm.categorieMasseur where acm.masseur.id_masseur= :paramIdMasseur and acm.dateSortieCategorieMasseur is null";
	
	@Override
	public List<AffectationCategorieMasseur> getListAffectationCatMassByIdMass(Integer idMasseur) {
		List<AffectationCategorieMasseur> affectationsM = null;
		Query requete = em.createQuery(GET_AFFECTATION_BY_ID_MASS);
		requete.setParameter("paramIdMasseur", idMasseur);
		affectationsM = requete.getResultList();
		return affectationsM;
	}


}
