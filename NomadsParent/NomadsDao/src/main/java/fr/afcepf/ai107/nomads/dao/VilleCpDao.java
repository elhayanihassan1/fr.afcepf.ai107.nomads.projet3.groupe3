package fr.afcepf.ai107.nomads.dao;

import java.util.List;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import fr.afcepf.ai107.nomads.entities.VilleCp;
import fr.afcepf.ai107.nomads.idao.VilleCpIDao;



@Remote(VilleCpIDao.class)
@Stateless
public class VilleCpDao extends GenericDao<VilleCp> implements VilleCpIDao {

	@PersistenceContext (unitName = "NomadsPU")
	private EntityManager em;
	
	private String REQ_CP = "SELECT v.libelleCp FROM VilleCp v";
	private String REQ_VILLE_BY_CP = "SELECT v.libelleVille FROM VilleCp v WHERE v.libelleCp = :paramLibelCp";
	//private String requeteIdByVille = "SELECT v.idVilleCp FROM VilleCp v WHERE v.libelleVille = :paramLibelVille";
	private String REQ_CP_BY_VILLE = "SELECT v FROM VilleCp v WHERE v.libelleVille = :paramLibelVille";
	
	
	@Override
	public VilleCp add(VilleCp t) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean delete(VilleCp t) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public VilleCp update(VilleCp t) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public VilleCp getById(int i) {
		// TODO Auto-generated method stub
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<String> getListCp() {
		List<String> codesP = null;
		
		Query requete = em.createQuery(REQ_CP);
		codesP = requete.getResultList();
		
		return codesP;
	}

	/**
	 * Renvoie la liste des villes assiciées à un code postal donnée
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<String> getVilleByCp(String codePostal) {
		List<String> villesByCp = null;
		
		Query requete = em.createQuery(REQ_VILLE_BY_CP);
		requete.setParameter("paramLibelCp", codePostal);
		villesByCp = requete.getResultList();
		
		return villesByCp;
	}

	/**
	 * Permet de récupérer l'ID correspondant à une ville donnée pour insertion dans table sous forme de clé étrangère
	 */
	@Override
	public VilleCp getVilleCpbyNomVille(String ville) {
		VilleCp vc = null;
		Query requete = em.createQuery(REQ_CP_BY_VILLE);
		requete.setParameter("paramLibelVille", ville);
		
		vc = (VilleCp) requete.getSingleResult();
		return vc;
	}

}
