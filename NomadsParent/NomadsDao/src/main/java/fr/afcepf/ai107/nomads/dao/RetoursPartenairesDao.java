package fr.afcepf.ai107.nomads.dao;

import java.util.List;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import fr.afcepf.ai107.nomads.entities.InterventionReelle;
import fr.afcepf.ai107.nomads.entities.Masseur;
import fr.afcepf.ai107.nomads.entities.Partenaire;
import fr.afcepf.ai107.nomads.entities.RetourExperiencePartenaire;
import fr.afcepf.ai107.nomads.idao.RetoursPartenairesIDao;

@Remote(RetoursPartenairesIDao.class)
@Stateless
public class RetoursPartenairesDao implements RetoursPartenairesIDao {
	

	@PersistenceContext(unitName = "NomadsPU")
	private EntityManager em;
	
	/**
	 * Permet la récupération des retours partenaire du mois en cours
	 */
	private final String REQ_GET_RETOUR_PART_MOIS_COURS = "SELECT rp FROM RetourExperiencePartenaire rp WHERE YEAR(date_retour_experience_partenaire) = YEAR(CURRENT_DATE()) AND MONTH(date_retour_experience_partenaire) = MONTH(CURRENT_DATE())";	
	private final String REQ_GET_RET_BY_INTERVENTION = "SELECT m FROM RetourExperiencePartenaire m WHERE m.interventionReelle = :paramInterv AND"
			+ " m.partenaire = :paramPart";
	
	@SuppressWarnings("unchecked")
	@Override
	public List<RetourExperiencePartenaire> getCurrentMonthRetourPartenaire(){

			List<RetourExperiencePartenaire> retourMoisEnCours = null;
			Query req = em.createQuery(REQ_GET_RETOUR_PART_MOIS_COURS);
		
			retourMoisEnCours = req.getResultList();
			return retourMoisEnCours;
		}

	@Override
	public RetourExperiencePartenaire add(RetourExperiencePartenaire t) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean delete(RetourExperiencePartenaire t) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public RetourExperiencePartenaire update(RetourExperiencePartenaire t) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public RetourExperiencePartenaire getById(int i) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<RetourExperiencePartenaire> findAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public RetourExperiencePartenaire insertRetourPartenaire(RetourExperiencePartenaire ret) {
		// TODO Auto-generated method stub
		em.persist(ret);
		return ret;
	}

	@Override
	public List<RetourExperiencePartenaire> getRetourByInterventionEtMasseur(InterventionReelle interv, Partenaire part) {
		// TODO Auto-generated method stub
		List<RetourExperiencePartenaire> retourPart = null;
		Query query = em.createQuery(REQ_GET_RET_BY_INTERVENTION);
		query.setParameter("paramInterv", interv);
		query.setParameter("paramPart", part);
		retourPart = query.getResultList();
		if(retourPart.size() == 0) {
			retourPart = null;
		}
		return retourPart;
	}


}
