package fr.afcepf.ai107.nomads.dao;

import java.util.List;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import fr.afcepf.ai107.nomads.entities.EvolutionProfessionnelle;
import fr.afcepf.ai107.nomads.idao.EvolutionProIDao;


@Remote(EvolutionProIDao.class)
@Stateless
public class EvolutionProDao extends GenericDao<EvolutionProfessionnelle> implements EvolutionProIDao {

	@PersistenceContext (unitName = "NomadsPU")
	private EntityManager em;
	
	private final String GET_BY_ID_MASSEUR = "SELECT ep FROM EvolutionProfessionnelle ep INNER JOIN FETCH ep.historiqueEvolutionProfessionnelle hep WHERE hep.masseur.id_masseur = :paramIdMasseur";
	
	
	private final String GE="select hep.masseur.id_masseur from HistoriqueEvolutionProfessionnelleMasseur hep";
	@Override
	public List<EvolutionProfessionnelle> getEvoProByIdMasseur(Integer idMasseur) {
		List<EvolutionProfessionnelle> listeEvoPrM = null;
		Query requEvoP = em.createQuery(GET_BY_ID_MASSEUR);
		requEvoP.setParameter("paramIdMasseur", idMasseur);
		listeEvoPrM = requEvoP.getResultList();
		return listeEvoPrM;
	}

}
