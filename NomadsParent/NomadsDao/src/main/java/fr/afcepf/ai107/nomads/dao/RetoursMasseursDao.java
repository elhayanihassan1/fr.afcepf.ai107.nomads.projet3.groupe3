package fr.afcepf.ai107.nomads.dao;

import java.util.List;
import java.util.Set;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import fr.afcepf.ai107.nomads.entities.InterventionReelle;
import fr.afcepf.ai107.nomads.entities.Masseur;
import fr.afcepf.ai107.nomads.entities.RetourExperienceMasseur;
import fr.afcepf.ai107.nomads.idao.RetoursMasseursIDao;

@Remote(RetoursMasseursIDao.class)
@Stateless
public class RetoursMasseursDao extends GenericDao<RetourExperienceMasseur> implements RetoursMasseursIDao {

	
	@PersistenceContext(unitName = "NomadsPU")
	private EntityManager em;
	
	/**
	 * Permet la récupération des retours masseurs du mois en cours
	 */
	private final String REQ_GET_RETOUR_MASS_MOIS_COURS = "SELECT rm FROM RetourExperienceMasseur rm WHERE YEAR(date_retour_experience_masseur) = YEAR(CURRENT_DATE()) AND MONTH(date_retour_experience_masseur) = MONTH(CURRENT_DATE())";	
	private final String REQ_GET_RET_BY_ID_INTERVENTION = "SELECT m FROM RetourExperienceMasseur m WHERE m.interventionReelle = :paramInterv AND"
			+ " m.masseur = :paramMasseur";
	
	@SuppressWarnings("unchecked")
	@Override
	public List<RetourExperienceMasseur> getCurrentMonthRetourMasseur(){

			List<RetourExperienceMasseur> retourMoisEnCours = null;
			Query req = em.createQuery(REQ_GET_RETOUR_MASS_MOIS_COURS);
		
			retourMoisEnCours = req.getResultList();
			return retourMoisEnCours;
		}
	 
	@Override
	public RetourExperienceMasseur add(RetourExperienceMasseur t) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean delete(RetourExperienceMasseur t) {
		// TODO Auto-generated method stub
		return false;
	}

//	@Override
//	public RetourExperienceMasseur update(RetourExperienceMasseur t) {
//		// TODO Auto-generated method stub
//		return null;
//	}

	@Override
	public RetourExperienceMasseur getById(int i) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<RetourExperienceMasseur> findAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public RetourExperienceMasseur insertRetour(RetourExperienceMasseur monRetour) {
		// TODO Auto-generated method stub
		em.persist(monRetour);
		return monRetour;
	}

	@Override
	public List<RetourExperienceMasseur> getRetourByIdIntervention(InterventionReelle intervention, Masseur masseur) {
		// TODO Auto-generated method stub
		List<RetourExperienceMasseur> retour = null;
		Query query = em.createQuery(REQ_GET_RET_BY_ID_INTERVENTION);
		query.setParameter("paramInterv", intervention);
		query.setParameter("paramMasseur", masseur);
		retour = query.getResultList();
		if(retour.size() == 0) {
		retour = null;
		
		}
		return retour;
	}
	
	

}
