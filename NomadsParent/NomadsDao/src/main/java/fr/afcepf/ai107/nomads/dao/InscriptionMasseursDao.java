package fr.afcepf.ai107.nomads.dao;

import java.util.Date;
import java.util.List;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import fr.afcepf.ai107.nomads.entities.InscriptionMasseurs;
import fr.afcepf.ai107.nomads.entities.InterventionReelle;
import fr.afcepf.ai107.nomads.entities.Masseur;
import fr.afcepf.ai107.nomads.idao.InscriptionMasseursIDao;

@Remote(InscriptionMasseursIDao.class)
@Stateless
public class InscriptionMasseursDao extends GenericDao<InscriptionMasseurs> implements InscriptionMasseursIDao {

	@PersistenceContext(unitName = "NomadsPU")
	private EntityManager em;
	

	private final String REQ_GET_MASSEUR_INSCRITS_SUR_UNE_INTERVENTION ="SELECT m FROM Masseur m INNER JOIN m.inscriptionsMasseurs i"
			+ " WHERE i.interventionReelle.idInterventionReelle = :paramId AND i.dateDesinscriptionMasseur is null";

	private final String REQ_GET_INSCRI_FUTURES_BY_MASSEUR = "SELECT im, ir FROM InscriptionMasseurs im INNER JOIN FETCH im.interventionReelle ir WHERE ir.datePrestation > :paramDate AND im.masseur.id_masseur= :paramIdMasseur";
	
	private final String REQ_INSCRI_BY_MASS_AND_INTERV = "SELECT im FROM InscriptionMasseurs im WHERE im.masseur= :paramMasseur AND im.interventionReelle= :paramInterv AND im.dateDesinscriptionMasseur is null";
	
	
	@Override
	public List<Masseur> getMasseursInscritsSurUneIntervention(Integer idIntervention) {
		// TODO Auto-generated method stub
		List<Masseur> mesMass= null;
		Query query = em.createQuery(REQ_GET_MASSEUR_INSCRITS_SUR_UNE_INTERVENTION);
		query.setParameter("paramId", idIntervention);
		mesMass = query.getResultList();
		return mesMass;
	}

	@Override
	public List<InscriptionMasseurs> getInscriptionMasseurIntervAVenirByIdMasseur(Integer idMasseur, Date dateDesinscri) {
		List<InscriptionMasseurs> inscri = null;
		Query requete = em.createQuery(REQ_GET_INSCRI_FUTURES_BY_MASSEUR);
		requete.setParameter("paramDate", dateDesinscri);
		requete.setParameter("paramIdMasseur", idMasseur);
		inscri = requete.getResultList();
		return inscri;
	}

	@Override
	public InscriptionMasseurs GetInscriptionByMasseurAndInterv(Masseur masseur, InterventionReelle interv) {
		InscriptionMasseurs inscri = null;
		List<InscriptionMasseurs> inscriptions = null;
		Query requete = em.createQuery(REQ_INSCRI_BY_MASS_AND_INTERV);
		requete.setParameter("paramMasseur", masseur);
		requete.setParameter("paramInterv", interv);
		inscriptions = requete.getResultList();
		inscri = inscriptions.get(0);
		return inscri;
	}

}
