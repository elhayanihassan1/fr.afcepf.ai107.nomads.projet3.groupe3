package fr.afcepf.ai107.nomads.dao;

import java.util.List;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import fr.afcepf.ai107.nomads.entities.MotifFinContratMasseur;
import fr.afcepf.ai107.nomads.idao.MotifsFinContratMasseurIDao;

@Remote (MotifsFinContratMasseurIDao.class)
@Stateless
public class MotifsFinContratMasseurDao extends GenericDao<MotifFinContratMasseur>
		implements MotifsFinContratMasseurIDao {

	@PersistenceContext (unitName = "NomadsPU")
	private EntityManager em;
	
	private final String REQ_GET_BY_LIBELLE = "SELECT mfcm FROM MotifFinContratMasseur mfcm WHERE mfcm.libelleMotifFinContratMasseur= :paramLibelle";
	
	@Override
	public MotifFinContratMasseur getMotifByLibelle(String libelle) {
		List<MotifFinContratMasseur> motifs = null;
		MotifFinContratMasseur motif = null;
		Query req = em.createQuery(REQ_GET_BY_LIBELLE);
		req.setParameter("paramLibelle", libelle);
		motifs = req.getResultList();
		motif = motifs.get(0);
		return motif;
	}

//	@Override
//	public List<MotifFinContratMasseur> getAllMotifs() {
//		// TODO Auto-generated method stub
//		return null;
//	}

}
