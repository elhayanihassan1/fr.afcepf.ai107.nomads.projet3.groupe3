package fr.afcepf.ai107.nomads.dao;

import java.util.List;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import fr.afcepf.ai107.nomads.entities.CategorieMasseur;
import fr.afcepf.ai107.nomads.idao.CategorieMasseurIDao;

@Remote (CategorieMasseurIDao.class)
@Stateless
public class CategorieMasseurDao extends GenericDao<CategorieMasseur> implements CategorieMasseurIDao {

	@PersistenceContext (unitName = "NomadsPU")
	private EntityManager em;
	
	//private final String REQ_CAT_BY_ID_MASS="SELECT cat.libelleCategorieMasseur FROM CategorieMasseur cat JOI";
	//private final String REQ_CAT_BY_ID_MASS2 = "SELECT cat FROM CategorieMasseur cat INNER JOIN Masseur m WHERE m.id_masseur= :paramIdMasseur";
	private final String REQ_CAT_BY_ID_MASS3 = "SELECT hist.evolutionsProfessionnelle.libelleEvolutionProfessionnelle FROM HistoriqueEvolutionProfessionnelleMasseur hist INNER JOIN Masseur m WHERE m.id_masseur= :paramIdMasseur";
	private final String REQ_GET_CATEGO_BY_ID_MASSEUR = "SELECT cat FROM CategorieMasseur cat INNER JOIN FETCH cat.affectationsCategorieMasseur acm WHERE acm.dateSortieCategorieMasseur is null AND acm.masseur.id_masseur= :paramIdMasseur";
	private final String REQ_GET_CATEGO_DISPO_BY_ID_MASSEUR = "SELECT cat FROM CategorieMasseur cat INNER JOIN FETCH cat.affectationsCategorieMasseur acm WHERE acm.dateEntreeCategorieMasseur is null AND acm.masseur.id_masseur= :paramIdMasseur";
//	private final String REQ_GET_ID_CATEGO_BY_ID_MASSEUR = "SELECT cat.idCategorieMasseur FROM CategorieMasseur cat INNER JOIN FETCH cat.affectationsCategorieMasseur acm WHERE acm.dateSortieCategorieMasseur is null AND acm.masseur.id_masseur= :paramIdMasseur";

		String r="SELECT cat.idCategorieMasseur FROM CategorieMasseur cat INNER JOIN FETCH cat.affectationsCategorieMasseur";
	
	@Override
	public List<String> categoriesByIdMasseur(int IdMasseur) {
		List<String> cat = null;
		Query requete = em.createQuery(REQ_CAT_BY_ID_MASS3);
		requete.setParameter("paramIdMasseur", IdMasseur);
		cat = requete.getResultList();
		return cat;
	}

	@Override
	public List<CategorieMasseur> listCategoMasseurByIdMasseur(Integer idMasseur) {
		System.out.println("/////////////////////////////////////////////////////////////////////////////////////////////////Je débute la requete de List<CategorieMasseur> listCategoMasseurByIdMasseur(Integer idMasseur)");
		List<CategorieMasseur> listCatMass = null;
		Query reqCatMass = em.createQuery(REQ_GET_CATEGO_BY_ID_MASSEUR);
		reqCatMass.setParameter("paramIdMasseur", idMasseur);
		listCatMass = reqCatMass.getResultList();
		System.out.println("///////////////////////////////////////////////////////////////////////////////////Cette requete renvoie une liste de taille : " + listCatMass.size());
		return listCatMass;
	}

	@Override
	public List<CategorieMasseur> listCategoDispoMasseurByIdMasseur(Integer idMasseur) {
		List<CategorieMasseur> listCatDispoMass = null;
		Query reqCatMass = em.createQuery(REQ_GET_CATEGO_DISPO_BY_ID_MASSEUR);
		reqCatMass.setParameter("paramIdMasseur", idMasseur);
		listCatDispoMass = reqCatMass.getResultList();
		System.out.println("DANS LE DAO, LA TAILLE DE LA LISTE DEMANDEE EST DE " + listCatDispoMass.size());
		return listCatDispoMass;
	}

//	@Override
//	public List<Integer> IdCategoriesByMasseur(Integer idMasseur) {
//		List<Integer> ids = null;
//		Query reqIdCatMass = em.createQuery(REQ_GET_ID_CATEGO_BY_ID_MASSEUR );
//		reqIdCatMass.setParameter("paramIdMasseur", idMasseur);
//		ids = reqIdCatMass.getResultList();
//		return ids;
//	}

}
