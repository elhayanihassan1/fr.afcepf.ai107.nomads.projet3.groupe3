package fr.afcepf.ai107.nomads.dao;

import javax.ejb.Remote;
import javax.ejb.Stateless;

import fr.afcepf.ai107.nomads.entities.IndisponibiliteMasseur;
import fr.afcepf.ai107.nomads.entities.MotifIndisponibiliteMasseur;
import fr.afcepf.ai107.nomads.idao.IndisponibiliteMasseurIDao;
import fr.afcepf.ai107.nomads.idao.MotifIndispoMassIDao;

@Remote (MotifIndispoMassIDao.class)
@Stateless
public class MotifIndispoMasseurDao extends GenericDao<MotifIndisponibiliteMasseur> implements MotifIndispoMassIDao {

	

}
