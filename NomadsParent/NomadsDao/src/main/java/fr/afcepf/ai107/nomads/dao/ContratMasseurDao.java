package fr.afcepf.ai107.nomads.dao;

import java.util.List;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import fr.afcepf.ai107.nomads.entities.ContratMasseur;
import fr.afcepf.ai107.nomads.idao.ContratMasseurIDao;

@Remote (ContratMasseurIDao.class)
@Stateless
public class ContratMasseurDao extends GenericDao<ContratMasseur> implements ContratMasseurIDao {

	@PersistenceContext(unitName = "NomadsPU")
	private EntityManager em;
	
	private final String GET_CONTRAT_BY_ID_MASSEUR ="SELECT c FROM ContratMasseur c WHERE c.dateFinContratMasseur is null AND c.masseur.id_masseur= :paramIdMasseur";
	
	@Override
	public ContratMasseur getContratByIdMasseur(Integer idMasseur) {
		List<ContratMasseur> contrats = null;
		ContratMasseur contrat = null;
		Query requete = em.createQuery(GET_CONTRAT_BY_ID_MASSEUR);
		requete.setParameter("paramIdMasseur", idMasseur);
		contrats=requete.getResultList();
		System.out.println("§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§DANS LE DAO LA LISTE EST DE TAILLE " + contrats.size() + "§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§");
		contrat = contrats.get(contrats.size()-1);
		return contrat;
	}

}
