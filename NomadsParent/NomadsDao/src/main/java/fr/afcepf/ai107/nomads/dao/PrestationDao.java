package fr.afcepf.ai107.nomads.dao;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.Id;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import fr.afcepf.ai107.nomads.entities.CategorieMasseur;
import fr.afcepf.ai107.nomads.entities.InterventionReelle;
import fr.afcepf.ai107.nomads.entities.Masseur;
import fr.afcepf.ai107.nomads.entities.Partenaire;
import fr.afcepf.ai107.nomads.entities.Prestation;
import fr.afcepf.ai107.nomads.entities.TypePaiement;
import fr.afcepf.ai107.nomads.entities.TypePartenaire;
import fr.afcepf.ai107.nomads.idao.CategorieMasseurIDao;
import fr.afcepf.ai107.nomads.idao.PrestationIDao;

@Remote(PrestationIDao.class)
@Stateless
public class PrestationDao extends GenericDao<Prestation> implements PrestationIDao {

	
	@PersistenceContext (unitName = "NomadsPU")
	private EntityManager em;
	
	@EJB
	CategorieMasseurIDao proxyCatMass;
	
	private final String REQ_GET_LISTE_PRESTATION_A_VENIR ="SELECT p FROM Prestation p INNER JOIN FETCH p.interventionReelle i" +
			" WHERE p.dateAnnulationPrestation is null AND" + 
			" i.datePrestation >= CURRENT_DATE  AND p.dateValidationPrestation is Not null";
	private final String REQ_GET_PARTENAIRE_BY_TYPE_PARTENAIRE = "SELECT p FROM Partenaire p INNER JOIN FETCH p.contratPartenaire i WHERE i.dateFinContratPartenaire is null AND p.typePartenaire.idTypePartenaire = :paramType";
	private final String REQ_LIST_TYPE_PARTENAIRE ="SELECT l FROM TypePartenaire l";
	private final String REQ_LIST_PARTENAIRE = "SELECT p FROM Partenaire p INNER JOIN FETCH p.indisponibilitePartenaire i INNER JOIN FETCH p.contratPartenaire c WHERE i.dateFinIndisponibilitePartenaire >= CURRENT_DATE AND p.dateFinContratPartenaire is null";
	private final String REQ_LIST_MASSEUR = "SELECT m FROM Masseur m INNER JOIN FETCH m.indisponibilitesMasseur i WHERE i.dateFinIndisponibiliteMasseur >= CURRENT_DATE AND m.dateDepartDefinitifMasseur is null";
	private final String REQ_LIST_TYPE_PAIEMENT = "SELECT p FROM TypePaiement p";
	private final String REQ_LIST_PRESTATION_PASSEES ="SELECT p FROM Prestation p INNER JOIN FETCH p.interventionReelle i WHERE"
			+ " p.dateAnnulationPrestation is null AND i.datePrestation < CURRENT_DATE";
	private final String REQ_LIST_INTERVENTION_REELLE_PASSEES = "SELECT i,p FROM InterventionReelle i INNER JOIN FETCH i.prestation p"
			+ " WHERE p.dateAnnulationPrestation is null AND i.datePrestation < CURRENT_DATE AND i.dateAnnulationIntervention is null";
	private final String REQ_LIST_INTERVENTION_REELLE = "SELECT i,p FROM InterventionReelle i INNER JOIN FETCH i.prestation p"
			+ " WHERE i.dateAnnulationIntervention is null AND p.dateAnnulationPrestation is null AND i.datePrestation >= CURRENT_DATE";

	
//	private String unzz ="SELECT cat.idCategorieMasseur FROM InterventionReelle i INNER  JOIN i.inscriptionMasseur ins INNER JOIN ins.masseur m INNER JOIN m.affectactionsCategorieMasseur af INNER JOIN af.categorieMasseur cat WHERE cat.affectationsCategorieMasseur.masseur.id_masseur = 1";
//	private String unzz ="SELECT cat.idCategorieMasseur FROM InterventionReelle i INNER  JOIN i.inscriptionMasseur ins INNER JOIN ins.masseur m INNER JOIN m.affectactionsCategorieMasseur af INNER JOIN af.categorieMasseur cat WHERE m.id_masseur = 1";
	
	private String REQ_LIST_INTERVENTION_REELLE2 = "SELECT distinct i,p FROM InterventionReelle i INNER JOIN FETCH i.prestation p WHERE p.partenaire.typePartenaire.idTypePartenaire in :listeCatMass AND i.datePrestation >= CURRENT_DATE AND i.dateAnnulationIntervention is null";
				private String REQ_LIST_INTERVENTION_REELLE3 = "SELECT distinct i,p FROM InterventionReelle i INNER JOIN FETCH i.prestation p WHERE p.dateAnnulationPrestation is null AND p.partenaire.typePartenaire.idTypePartenaire in :listeCatMass AND i.datePrestation >= CURRENT_DATE";
				private String compte = "SELECT distinct ir, , SUM(case WHEN i.inscriptionMasseur is not null then 1 else 0 end) FROM InterventionReelle ir INNER JOIN FETCH ir.inscriptionMasseur im WHERE im.dateDesinscriptionMasseur is null and ir in :paramIr";
//				private String retest = "SELECT distinct ir FROM InterventionReelle ir INNER JOIN FETCH ir.inscriptionMasseur im ";
				
												//SUM( CASE WHEN a.sexe='F' THEN 1 ELSE 0 end)
	private String un ="SELECT p.dateDesinscriptionMasseur FROM InscriptionMasseurs p";
	private String uneee ="SELECT cat. FROM CategorieMasseur cat";
	private  final String REQ_GETBYID_PRESTATION = "SELECT p FROM Prestation p WHERE p.idPrestation = :paramIdPrestation";
	private final String REQ_GET_LISTE_DEMANDE_PRESTATION ="SELECT p FROM Prestation p INNER JOIN FETCH p.interventionReelle i" +
			" WHERE p.dateValidationPrestation is null AND" + 
			" i.datePrestation >= CURRENT_DATE";
	@SuppressWarnings("unchecked")
	@Override
	public List<Prestation> getAll() {
		// TODO Auto-generated method stub
		Query query = em.createQuery(REQ_GET_LISTE_PRESTATION_A_VENIR); 
		List <Prestation> prestations = query.getResultList();
		return prestations;
	}

	public Prestation addPrestation(Prestation prestation) {
		em.persist(prestation);
		return prestation;
		
	}
	
	public InterventionReelle addIntervention(InterventionReelle interventionReelle) {
		em.persist(interventionReelle);
		return interventionReelle;
	}

	//Utiliser dans une drop down list pour la création de la prestation
	@SuppressWarnings("unchecked")
	@Override
	public List<Partenaire> getPartenaireByTypePartenaire(Integer idTypePartenaire) {
		List<Partenaire> PartenaireByTypePart = null;
		Query query = em.createQuery(REQ_GET_PARTENAIRE_BY_TYPE_PARTENAIRE);
		query.setParameter("paramType", idTypePartenaire);
		PartenaireByTypePart = query.getResultList();
		return PartenaireByTypePart;
	}

	//Utiliser dans une drop down list pour la création de la prestation
	@Override
	public List<TypePartenaire> getTypePartenaire() {
		// TODO Auto-generated method stub
		List<TypePartenaire> listeTypePartenaire = null;
		Query query = em.createQuery(REQ_LIST_TYPE_PARTENAIRE);
		listeTypePartenaire = query.getResultList();
		return listeTypePartenaire;
	}
	 
	@Override
	public List<Partenaire> getListePartenaire(){
		List<Partenaire> partenaires = null;
		Query query = em.createQuery(REQ_LIST_PARTENAIRE);
		partenaires = query.getResultList();
		return partenaires;
		
	}

	@Override
	public List<Masseur> getListeMasseur() {
		// TODO Auto-generated method stub
		List<Masseur> listeMasseur = null;
		Query query = em.createQuery(REQ_LIST_MASSEUR);
		listeMasseur = query.getResultList();
		return listeMasseur;
	}
	
	@Override
	public List<Masseur> getListeMasseursByTypePartenaire(Integer idCategorie){
		List<Masseur> mesMasseurs = null;
		if(idCategorie < 1) {
			Query query = em.createQuery(REQ_LIST_MASSEUR);
			mesMasseurs = query.getResultList();
		}
		if (idCategorie == 1) {
			Query query = em.createQuery("SELECT m FROM Masseur m INNER JOIN FETCH m.affectactionsCategorieMasseur a "
					+ "INNER JOIN FETCH a.categorieMasseur f WHERE f.idCategorieMasseur = 1");
			mesMasseurs = query.getResultList();
		
		}
		else if (idCategorie == 2) {
			Query query = em.createQuery("SELECT m FROM Masseur m INNER JOIN FETCH m.affectactionsCategorieMasseur a INNER JOIN"
					+ "INNER JOIN FETCH a.categorieMasseur f WHERE f.idCategorieMasseur = 2");
			mesMasseurs = query.getResultList();
		}
		else if (idCategorie == 3) {
			Query query = em.createQuery("SELECT m FROM Masseur m INNER JOIN FETCH m.affectactionsCategorieMasseur a "
					+ "INNER JOIN FETCH a.categorieMasseur f WHERE f.idCategorieMasseur = 3");
			mesMasseurs = query.getResultList();
		}
		return mesMasseurs;
	}

	@Override
	public List<TypePaiement> getAllTypePaiement() {
		// TODO Auto-generated method stub
		List<TypePaiement> typePaiement = null;
		Query query = em.createQuery(REQ_LIST_TYPE_PAIEMENT);
		typePaiement = query.getResultList();
		return typePaiement;
	}

	@Override
	public List<Prestation> getAllHistoriquePrestation() {
		// TODO Auto-generated method stub
		List<Prestation> mesPresta = null;
		Query query = em.createQuery(REQ_LIST_PRESTATION_PASSEES);
		mesPresta = query.getResultList();
		return mesPresta;
	}

	@Override
	public List<InterventionReelle> getListeInterventionReelle() {
		// TODO Auto-generated method stub
		List<InterventionReelle> mesInter = null;
		Query query = em.createQuery(REQ_LIST_INTERVENTION_REELLE);
		mesInter = query.getResultList();
		return mesInter;
	}

	@Override
	public List<InterventionReelle> getListeInterventionReellePassees() {
		// TODO Auto-generated method stub
		List<InterventionReelle> mesInter = null;
		Query query = em.createQuery(REQ_LIST_INTERVENTION_REELLE_PASSEES);
		mesInter = query.getResultList();
		return mesInter;
	}

	@Override
	public List<InterventionReelle> getListeInterventionReelleDispoForMasseur(Integer idMasseur) {
		List<InterventionReelle> interventionsDispo = null;
		
	//	Query test =em.createQuery(unzz);
	//	List<Integer> entiers = test.getResultList();
	//	System.out.println("&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&" + entiers.size());
		List<CategorieMasseur> categoMass = proxyCatMass.listCategoMasseurByIdMasseur(idMasseur);
		System.out.println("************************************************************JE RECUPERE LA LISTE DE L4AUTRE DAO; la taille est toujours de : " + categoMass.size());
		List<Integer> idCategoMasseur = new ArrayList<Integer>();
		
		for (CategorieMasseur categorieMasseur : categoMass) {
			Integer a = categorieMasseur.getIdCategorieMasseur();
			System.out.println("{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{ a vaut " + a + "}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}");
			idCategoMasseur.add(a);
		}
		System.out.println("JE SORS DE CE *ù**&é&&ù&&^&&^$*&*&ù$^&*^de foreach");
		System.out.println("&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&" + idCategoMasseur.size());
		
		Query query = em.createQuery(REQ_LIST_INTERVENTION_REELLE2);
		query.setParameter("listeCatMass", idCategoMasseur);
		interventionsDispo = query.getResultList();
		
		
		return interventionsDispo;
	}
	
	@Override
	public Prestation validerDemandePrestation(Prestation prestation) {
		em.merge(prestation);
		em.flush();
		return prestation;
	}


	@Override
	public Prestation getPrestationById(int prestationId) {
		List<Prestation> prestations = null;
		Query query = em.createQuery(REQ_GETBYID_PRESTATION);
		query.setParameter("paramIdPrestation", prestationId);
		prestations= query.getResultList();
		return prestations.get(0);
	}
	
	public List<Prestation> getAllDemande() {
		// TODO Auto-generated method stub
		Query query = em.createQuery(REQ_GET_LISTE_DEMANDE_PRESTATION); 
		List <Prestation> prestations = query.getResultList();
		return prestations;
	}

	
//	@Override
//	public List<InterventionReelle> getListeInterventionReelleDispoForMasseur(Integer idMasseur) {
//		List<InterventionReelle> interventionsDispo = null;
//		
//		List<CategorieMasseur> categoMass = proxyCatMass.listCategoMasseurByIdMasseur(idMasseur);
//		System.out.println("************************************************************JE RECUPERE LA LISTE DE L4AUTRE DAO; la taille est toujours de : " + categoMass.size());
//		List<Integer> idCategoMasseur = new ArrayList<Integer>();
//		
//		for (CategorieMasseur categorieMasseur : categoMass) {
//			Integer a = categorieMasseur.getIdCategorieMasseur();
//			System.out.println("{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{ a vaut " + a + "}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}");
//			idCategoMasseur.add(a);
//		}
//		System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!J'appelle la seconde query");
//		Query query = em.createQuery(REQ_LIST_INTERVENTION_REELLE3);
//		query.setParameter("listeCatMass", idCategoMasseur);
//		interventionsDispo = query.getResultList();
//		System.out.println("/**/**/*///*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/* Je charge cette seconde query en résultat");
//		
//		Query query3 = em.createQuery(compte);
//		query3.setParameter("paramIr", interventionsDispo);
//		interventionsDispo = query3.getResultList();
//		
//		return interventionsDispo;
//	}
//	
	
	
	
}

