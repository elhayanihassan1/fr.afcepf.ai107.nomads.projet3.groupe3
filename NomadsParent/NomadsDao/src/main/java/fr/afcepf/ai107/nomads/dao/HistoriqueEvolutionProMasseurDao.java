package fr.afcepf.ai107.nomads.dao;

import java.util.List;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import fr.afcepf.ai107.nomads.entities.HistoriqueEvolutionProfessionnelleMasseur;
import fr.afcepf.ai107.nomads.idao.HistoriqueEvoProMasseurIDao;
import fr.afcepf.ai107.nomads.idao.PartenaireIDao;

@Remote(HistoriqueEvoProMasseurIDao.class)
@Stateless
public class HistoriqueEvolutionProMasseurDao extends GenericDao<HistoriqueEvolutionProfessionnelleMasseur>  implements HistoriqueEvoProMasseurIDao{

	@PersistenceContext (unitName = "NomadsPU")
	private EntityManager em;
	
	private final String REQ_LIST_HISTO_BY_ID_MASS = "SELECT hep FROM HistoriqueEvolutionProfessionnelleMasseur hep INNER JOIN FETCH hep.evolutionsProfessionnelle WHERE hep.masseur.id_masseur= :paramIdMasseur AND hep.dateFinEvolutionProfessionnelleMasseur is null";   //Date à null est utile?
	String gdf="";
	private final String REQ_BOOL_NEO = "SELECT hep FROM HistoriqueEvolutionProfessionnelleMasseur hep WHERE (hep.dateFinEvolutionProfessionnelleMasseur is null AND hep.evolutionsProfessionnelle.idEvolutionProfessionnelle = 1) AND hep.masseur.id_masseur = :paramId";
	
	
	@Override
	public List<HistoriqueEvolutionProfessionnelleMasseur> historiqueByIdMasseur(Integer idMasseur) {
		List<HistoriqueEvolutionProfessionnelleMasseur> listHistoByMass = null;
		Query requete = em.createQuery(REQ_LIST_HISTO_BY_ID_MASS);
		requete.setParameter("paramIdMasseur", idMasseur);
		listHistoByMass=requete.getResultList();
		return listHistoByMass;
	}

	@Override
	public boolean EstNeonomad(Integer idMasseur) {
		boolean estNeo = false;
		List<HistoriqueEvolutionProfessionnelleMasseur> list = null;
		Query requete = em.createQuery(REQ_BOOL_NEO);
		requete.setParameter("paramId", idMasseur);
		list = requete.getResultList();
		System.out.println(list.size());
		if (list.size() == 0) {
			estNeo = false;
		} else
		{estNeo = true;}
		
			return estNeo;
		}
	

}
