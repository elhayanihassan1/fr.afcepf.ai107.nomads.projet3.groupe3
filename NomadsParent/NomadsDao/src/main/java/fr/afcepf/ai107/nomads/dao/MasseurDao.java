package fr.afcepf.ai107.nomads.dao;

import java.time.LocalDate;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import fr.afcepf.ai107.nomads.entities.Masseur;
import fr.afcepf.ai107.nomads.entities.Sexe;
import fr.afcepf.ai107.nomads.idao.IndisponibiliteMasseurIDao;
import fr.afcepf.ai107.nomads.idao.MasseurIDao;


@Remote(MasseurIDao.class)
@Stateless
public class MasseurDao extends GenericDao<Masseur> implements MasseurIDao{
	
	@EJB 
	IndisponibiliteMasseurIDao proxyIndi;
	
	
	@PersistenceContext (unitName = "NomadsPU")
	private EntityManager em;

	private final String reqGetListBySexe = "SELECT m FROM Masseur m WHERE m.sexe.id=:paramIdSexe";
	private final String requAuthenticate ="SELECT m  FROM  Masseur m  WHERE m.loginMasseur = :paramLogin AND m.passwordMasseur = :paramPassword";
	private final String reqExists = "SELECT m FROM Masseur m WHERE m.nomMasseur = :paramNom AND m.prenomMasseur = :paramPrenom AND m.dateDeNaissanceMasseur = :paramDateNaissance";
	
	private final String REQ_ALL_INFOS_MASSEUR_ACTIFS = "SELECT distinct m, af FROM Masseur m INNER JOIN FETCH m.affectactionsCategorieMasseur af WHERE af.dateSortieCategorieMasseur is null AND m IN :paramActifs";
	private final String REQ_ALL_INFOS_MASSEUR_INACTIFS = "SELECT distinct m, af FROM Masseur m INNER JOIN FETCH m.affectactionsCategorieMasseur af WHERE af.dateSortieCategorieMasseur is null AND m IN :paramInactifs";
	private final String REQ_ALL_INFOS_MASSEUR_PT2 ="SELECT distinct m, ep FROM Masseur m INNER JOIN FETCH m.evolutionsProfessionnelles ep WHERE m IN :paramMasseurs AND ep.dateFinEvolutionProfessionnelleMasseur is null";
	
	private final String REQ_ALL_INFOS_MASSEUR_PT2_BY_LVL ="SELECT distinct m, ep FROM Masseur m INNER JOIN FETCH m.evolutionsProfessionnelles ep WHERE m IN :paramMasseurs AND ep.dateFinEvolutionProfessionnelleMasseur is null AND ep.evolutionsProfessionnelle.libelleEvolutionProfessionnelle= :paramEvo";

	private final String REQ_ALL_INFOS_MASSEUR_ACTIFS_BY_CATEGO = "SELECT distinct m, af FROM Masseur m INNER JOIN FETCH m.affectactionsCategorieMasseur af WHERE af.dateSortieCategorieMasseur is null AND m IN :paramActifs AND af.categorieMasseur.libelleCategorieMasseur = :paramCatego";
	private final String REQ_ALL_RECENT_MASSEUR = "SELECT m FROM Masseur m WHERE m.datePremierEnregistrementMasseur >= :paramDateRecent";
	private final String REQ_ALL_DEPART_RECENT_MASSEUR = "SELECT m FROM Masseur m WHERE m.dateDepartDefinitifMasseur >= :paramDateRecent";
	
	private final String REQ_GET_INFO_BY_ID = "SELECT m FROM Masseur m WHERE m.id_masseur = :paramId";
	
	
	String test = "SELECT af.categorieMasseur.libelleCategorieMasseur FROM AffectationCategorieMasseur af";
	String test2 = "SELECT ep.libelleEvolutionProfessionnelle FROM EvolutionProfessionnelle ep";
	String test3 = "SELECT hep.evolutionsProfessionnelle.libelleEvolutionProfessionnelle FROM HistoriqueEvolutionProfessionnelleMasseur hep";

	
	@SuppressWarnings("unchecked")
	@Override
	public List<Masseur> getListMasseurs(Sexe sexe) {
		List<Masseur> masseurs=null;
		Query query = em.createQuery(reqGetListBySexe);
		query.setParameter("paramIdSexe", sexe.getIdSexe());
		masseurs = query.getResultList();
		return masseurs;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Masseur authenticate(String login, String password) {
	
		Masseur masseur = null;
		List<Masseur> masseurs = null;
		Query queryMasseur = em.createQuery(requAuthenticate);		
		queryMasseur.setParameter("paramLogin", login);
		queryMasseur.setParameter("paramPassword", password);
		masseurs = queryMasseur.getResultList();
		if(masseurs.size() > 0) {
			masseur = masseurs.get(0);
		}
		return masseur;
	}


	@SuppressWarnings("unchecked")
	@Override
	public boolean exists(Masseur masseur) {
		boolean exists = false;
		List<Masseur> masseurs = null;
		Query requete = em.createQuery(reqExists);
		requete.setParameter("paramNom", masseur.getNomMasseur());
		requete.setParameter("paramPrenom", masseur.getPrenomMasseur());
		requete.setParameter("paramDateNaissance", masseur.getDateDeNaissanceMasseur());
		
		masseurs = requete.getResultList();
		if (masseurs.size() > 0) {
			exists = true;
		}
		return exists;
	}
	
	public Masseur add(Masseur masseur) {
		em.persist(masseur);
		return masseur;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Masseur> getAllInfoForMasseursActifs() {
		List<Masseur> masseurs = null;
		
				Query requete = em.createQuery(REQ_ALL_INFOS_MASSEUR_ACTIFS); //.setHint(QueryHints.PASS_DISTINCT_THROUGH, false)
				requete.setParameter("paramActifs", proxyIndi.getListeMasseursActifs());
				masseurs = requete.getResultList();
				Query query2 = em.createQuery(REQ_ALL_INFOS_MASSEUR_PT2);
				query2.setParameter("paramMasseurs", masseurs);
				masseurs = query2.getResultList();
		return masseurs;
	}

	@Override
	public List<Masseur> getAllInfoForMasseursInactifs() {
		List<Masseur> masseurs = null;
		
		Query requete = em.createQuery(REQ_ALL_INFOS_MASSEUR_INACTIFS); 
		requete.setParameter("paramInactifs", proxyIndi.getListeMasseursIndispo());
		masseurs = requete.getResultList();
		Query query2 = em.createQuery(REQ_ALL_INFOS_MASSEUR_PT2);
		query2.setParameter("paramMasseurs", masseurs);
		masseurs = query2.getResultList();
return masseurs;
	}

	@Override
	public List<Masseur> getAllInfoForMasseursActifsByLvl(String niveau) {
		List<Masseur> masseurs = null;
		
		Query requete = em.createQuery(REQ_ALL_INFOS_MASSEUR_ACTIFS); 
		requete.setParameter("paramActifs", proxyIndi.getListeMasseursActifs());
		masseurs = requete.getResultList();
		Query query2 = em.createQuery(REQ_ALL_INFOS_MASSEUR_PT2_BY_LVL);
		query2.setParameter("paramMasseurs", masseurs);
		query2.setParameter("paramEvo", niveau);
		masseurs = query2.getResultList();
return masseurs;
		
	}


	@Override
	public List<Masseur> getAllInfosForMasseursActifsByCatego(String categorie) {
		List<Masseur> masseursCat = null;
		Query requeteCat = em.createQuery(REQ_ALL_INFOS_MASSEUR_ACTIFS_BY_CATEGO);
		requeteCat.setParameter("paramActifs", proxyIndi.getListeMasseursActifs());
		requeteCat.setParameter("paramCatego", categorie);
		masseursCat = requeteCat.getResultList();
		Query query2 = em.createQuery(REQ_ALL_INFOS_MASSEUR_PT2);
		query2.setParameter("paramMasseurs", masseursCat);
		masseursCat = query2.getResultList();
		return masseursCat;
	}

	@Override
	public Masseur getInfosbyId(Integer idMasseur) {
		Masseur masseur = null;
		Query requeteById = em.createQuery(REQ_GET_INFO_BY_ID);
		requeteById.setParameter("paramId", idMasseur);
		masseur = (Masseur) requeteById.getSingleResult();
		return masseur;
	}


	@Override
	public List<Masseur> getRecentMasseurs() {
		List<Masseur> recentMasseurs = null;
		Query reqRecent = em.createQuery(REQ_ALL_RECENT_MASSEUR);
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.MONTH, -3);
		Date date = (Date) cal.getTime();
		reqRecent.setParameter("paramDateRecent", date);
		recentMasseurs = reqRecent.getResultList();
		return recentMasseurs;
	}



	@Override
	public List<Masseur> getMasseurForCreationPresta(Integer idCategorie) {
		List<Masseur> masseurs = null;

		if(idCategorie == 1) {
		Query query = em.createQuery("SELECT distinct m, af, f FROM Masseur m INNER JOIN FETCH "
				+ "m.affectactionsCategorieMasseur af INNER JOIN FETCH af.categorieMasseur f "
				+ "WHERE f.idCategorieMasseur = 1 AND af.dateSortieCategorieMasseur is null AND m IN :paramActifs");
		query.setParameter("paramActifs", proxyIndi.getListeMasseursActifs());
		masseurs = query.getResultList();
		Query requete = em.createQuery("SELECT m FROM Masseur m INNER JOIN FETCH m.evolutionsProfessionnelles ep INNER JOIN FETCH "
				+ "ep.evolutionsProfessionnelle i WHERE i.idEvolutionProfessionnelle = 1 AND ep.dateFinEvolutionProfessionnelleMasseur is null AND m IN :paramDeux"); 
		requete.setParameter("paramDeux", masseurs);
		masseurs = requete.getResultList();
		}

		else if(idCategorie == 2) {
			Query requete = em.createQuery("SELECT distinct m, af, f FROM Masseur m INNER JOIN FETCH "
					+ "m.affectactionsCategorieMasseur af INNER JOIN FETCH af.categorieMasseur f "
					+ "WHERE f.idCategorieMasseur = 2 AND af.dateSortieCategorieMasseur is null AND m IN :paramActifs "); 
			requete.setParameter("paramActifs", proxyIndi.getListeMasseursActifs());
			masseurs = requete.getResultList();
		}else if (idCategorie == 3) {
			Query requete = em.createQuery("SELECT distinct m, af, f FROM Masseur m INNER JOIN FETCH "
					+ "m.affectactionsCategorieMasseur af INNER JOIN FETCH af.categorieMasseur f "
					+ "WHERE f.idCategorieMasseur = 3 AND af.dateSortieCategorieMasseur is null AND m IN :paramActifs "); 
			requete.setParameter("paramActifs", proxyIndi.getListeMasseursActifs());
			masseurs = requete.getResultList();
		}
		return masseurs;
	}

	@Override
	public List<Masseur> getRecentMasseurs1month() {
		List<Masseur> recentMasseurs = null;
		Query reqRecent = em.createQuery(REQ_ALL_RECENT_MASSEUR);
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.MONTH, -1);
		Date date = (Date) cal.getTime();
		reqRecent.setParameter("paramDateRecent", date);
		recentMasseurs = reqRecent.getResultList();
		return recentMasseurs;

	}

	@Override
	public List<Masseur> getRecentDepartMasseurs1month() {
		List<Masseur> departsRecentMasseurs = null;
		Query reqRecent = em.createQuery(REQ_ALL_DEPART_RECENT_MASSEUR);
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.MONTH, -1);
		Date date = (Date) cal.getTime();
		reqRecent.setParameter("paramDateRecent", date);
		departsRecentMasseurs = reqRecent.getResultList();
		return departsRecentMasseurs;

	}


}
