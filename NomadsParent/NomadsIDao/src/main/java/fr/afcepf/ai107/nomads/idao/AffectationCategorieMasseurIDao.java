package fr.afcepf.ai107.nomads.idao;

import java.util.List;

import fr.afcepf.ai107.nomads.entities.AffectationCategorieMasseur;

public interface AffectationCategorieMasseurIDao extends GenericIDao<AffectationCategorieMasseur> {

	List<AffectationCategorieMasseur> getListAffectationCatMassByIdMass(Integer idMasseur);
//	void retirerCategorieMasseur (AffectationCategorieMasseur categoMasseur);
}
