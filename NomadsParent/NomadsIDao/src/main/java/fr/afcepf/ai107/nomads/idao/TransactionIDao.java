package fr.afcepf.ai107.nomads.idao;

import java.util.List;

import fr.afcepf.ai107.nomads.entities.Transaction;
import fr.afcepf.ai107.nomads.entities.TypePackMads;

public interface TransactionIDao extends GenericIDao<Transaction> {
	List<Transaction> getListTransactionByIdMasseur (Integer idMasseur);
	List<TypePackMads> getAllTypePackMads();
}
