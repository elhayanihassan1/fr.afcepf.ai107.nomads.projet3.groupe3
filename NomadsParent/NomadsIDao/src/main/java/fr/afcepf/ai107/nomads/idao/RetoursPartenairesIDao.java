package fr.afcepf.ai107.nomads.idao;

import java.util.List;

import fr.afcepf.ai107.nomads.entities.InterventionReelle;
import fr.afcepf.ai107.nomads.entities.Partenaire;
import fr.afcepf.ai107.nomads.entities.RetourExperiencePartenaire;

public interface RetoursPartenairesIDao extends GenericIDao<RetourExperiencePartenaire> {

	List<RetourExperiencePartenaire> getCurrentMonthRetourPartenaire();
	RetourExperiencePartenaire insertRetourPartenaire(RetourExperiencePartenaire ret);
	List<RetourExperiencePartenaire> getRetourByInterventionEtMasseur(InterventionReelle interv, Partenaire part);
}
