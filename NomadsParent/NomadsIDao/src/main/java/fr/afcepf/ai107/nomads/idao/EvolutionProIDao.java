package fr.afcepf.ai107.nomads.idao;

import java.util.List;

import fr.afcepf.ai107.nomads.entities.EvolutionProfessionnelle;

public interface EvolutionProIDao extends GenericIDao<EvolutionProfessionnelle>{
List<EvolutionProfessionnelle> getEvoProByIdMasseur (Integer idMasseur);
}
