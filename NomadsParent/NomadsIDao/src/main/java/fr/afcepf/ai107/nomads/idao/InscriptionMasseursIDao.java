package fr.afcepf.ai107.nomads.idao;

import java.util.Date;
import java.util.List;

import fr.afcepf.ai107.nomads.entities.InscriptionMasseurs;
import fr.afcepf.ai107.nomads.entities.InterventionReelle;
import fr.afcepf.ai107.nomads.entities.Masseur;


public interface InscriptionMasseursIDao extends GenericIDao<InscriptionMasseurs>  {

	List<Masseur> getMasseursInscritsSurUneIntervention(Integer idIntervention);
	List<InscriptionMasseurs>getInscriptionMasseurIntervAVenirByIdMasseur(Integer idMasseur, Date dateDesinscri);
	InscriptionMasseurs GetInscriptionByMasseurAndInterv(Masseur masseur, InterventionReelle interv);
}
