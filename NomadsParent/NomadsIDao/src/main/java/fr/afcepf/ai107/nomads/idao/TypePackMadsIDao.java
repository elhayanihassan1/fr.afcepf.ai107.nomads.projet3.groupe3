package fr.afcepf.ai107.nomads.idao;

import java.util.List;

import fr.afcepf.ai107.nomads.entities.TypePackMads;


public interface TypePackMadsIDao extends GenericIDao<TypePackMads>{

	List<TypePackMads> getAllNonRemovedPacks();

	List<TypePackMads> getAllRemovedPacks();

}
