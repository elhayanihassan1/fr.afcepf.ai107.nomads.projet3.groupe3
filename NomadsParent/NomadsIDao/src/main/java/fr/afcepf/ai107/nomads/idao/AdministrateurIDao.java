package fr.afcepf.ai107.nomads.idao;

import fr.afcepf.ai107.nomads.entities.Administrateur;


public interface AdministrateurIDao extends GenericIDao<Administrateur> {

	Administrateur authenticate(String login, String password);
}
