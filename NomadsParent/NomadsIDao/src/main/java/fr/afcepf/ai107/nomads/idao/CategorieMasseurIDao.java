package fr.afcepf.ai107.nomads.idao;

import java.util.List;

import fr.afcepf.ai107.nomads.entities.CategorieMasseur;

public interface CategorieMasseurIDao extends GenericIDao<CategorieMasseur> {
	
	List<String> categoriesByIdMasseur (int IdMasseur);
	List<CategorieMasseur> listCategoMasseurByIdMasseur (Integer idMasseur);
	List<CategorieMasseur> listCategoDispoMasseurByIdMasseur (Integer idMasseur);
//	List<Integer> IdCategoriesByMasseur (Integer idMasseur);
}
