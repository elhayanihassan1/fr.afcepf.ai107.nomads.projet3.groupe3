package fr.afcepf.ai107.nomads.idao;

import java.util.List;
import java.util.Set;

import fr.afcepf.ai107.nomads.entities.InterventionReelle;
import fr.afcepf.ai107.nomads.entities.Masseur;
import fr.afcepf.ai107.nomads.entities.RetourExperienceMasseur;

public interface RetoursMasseursIDao extends GenericIDao<RetourExperienceMasseur> {

	List<RetourExperienceMasseur> getCurrentMonthRetourMasseur();
	RetourExperienceMasseur insertRetour(RetourExperienceMasseur monRetour);
	List<RetourExperienceMasseur> getRetourByIdIntervention(InterventionReelle intervention, Masseur masseur);
}
