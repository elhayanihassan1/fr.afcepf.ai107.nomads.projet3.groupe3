package fr.afcepf.ai107.nomads.idao;

import java.util.List;

import fr.afcepf.ai107.nomads.entities.MotifFinContratMasseur;

public interface MotifsFinContratMasseurIDao extends GenericIDao<MotifFinContratMasseur>{

		MotifFinContratMasseur getMotifByLibelle(String libelle);
}
