package fr.afcepf.ai107.nomads.idao;

import java.util.List;

import fr.afcepf.ai107.nomads.entities.InterventionReelle;
import fr.afcepf.ai107.nomads.entities.MotifAnnulationPrestation;

public interface MotifAnnulationPrestationIDao extends GenericIDao<MotifAnnulationPrestation>{

	List<String> getAllMotifAnnulation();
	InterventionReelle updateInterventionAvecMotifAnnulation(InterventionReelle interv);
	MotifAnnulationPrestation getMotifByLibelle(String motif);

}
