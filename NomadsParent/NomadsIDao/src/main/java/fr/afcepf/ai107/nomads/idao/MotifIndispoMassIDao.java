package fr.afcepf.ai107.nomads.idao;

import fr.afcepf.ai107.nomads.entities.MotifIndisponibiliteMasseur;

public interface MotifIndispoMassIDao extends GenericIDao<MotifIndisponibiliteMasseur> {

}
