package fr.afcepf.ai107.nomads.outils.business;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;

import fr.afcepf.ai107.nomads.entities.ContratMasseur;
import fr.afcepf.ai107.nomads.entities.MotifFinContratMasseur;
import fr.afcepf.ai107.nomads.idao.ContratMasseurIDao;
import fr.afcepf.ai107.nomads.idao.MotifsFinContratMasseurIDao;
import fr.afcepf.ai107.nomads.outils.ibusiness.GestionContratsIBusiness;


@Remote (GestionContratsIBusiness.class)
@Stateless
public class GestionContratsBusiness implements GestionContratsIBusiness {

	@EJB
	ContratMasseurIDao proxyContratMasseurDao;
	
	@EJB
	MotifsFinContratMasseurIDao proxyMotifFCMasseur;
	
	@Override
	public void CreateNewContractMasseur(ContratMasseur contratM) {
		proxyContratMasseurDao.add(contratM);
	}

	@Override
	public List<MotifFinContratMasseur> getAllMotifsFinCOntratMasseur() {
		List<MotifFinContratMasseur> motifs = null;
		motifs = proxyMotifFCMasseur.findAll();
		
		return motifs;
	}

	@Override
	public ContratMasseur getContratMasseurByIdMasseur(Integer idMasseur) {
		ContratMasseur contrat = null;
		contrat = proxyContratMasseurDao.getContratByIdMasseur(idMasseur);
		return contrat;
	}

	@Override
	public MotifFinContratMasseur getMotifByLibelleMotif(String libelleMotif) {
		MotifFinContratMasseur motif;
		motif=proxyMotifFCMasseur.getMotifByLibelle(libelleMotif);
		return motif;
	}

	@Override
	public void modifierContrat(ContratMasseur contrat) {
		proxyContratMasseurDao.update(contrat);
		
	}

}
