package fr.afcepf.ai107.nomads.outils.business;


import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;


import fr.afcepf.ai107.nomads.entities.TypePackMads;
import fr.afcepf.ai107.nomads.idao.TypePackMadsIDao;
import fr.afcepf.ai107.nomads.outils.ibusiness.TypePackMadsIBusiness;

@Remote(TypePackMadsIBusiness.class)
@Stateless
public class TypePackMadsBusiness implements TypePackMadsIBusiness {
	
	@EJB
	private TypePackMadsIDao proxyTypePackMadsDao;
	
	public List<TypePackMads> allPackMads() {
		return proxyTypePackMadsDao.findAll();
	}

	@Override
	public TypePackMads add(TypePackMads newPack) {
		return proxyTypePackMadsDao.add(newPack);
	}

	public boolean delete(TypePackMads selectedPack) {
		return proxyTypePackMadsDao.delete(selectedPack);
	}

	@Override
	public TypePackMads udpate(TypePackMads selectedPack) {
		return proxyTypePackMadsDao.update(selectedPack);
		
	}
	
	@Override
	public List<TypePackMads> allNonRemovedPackMads(){
		return proxyTypePackMadsDao.getAllNonRemovedPacks();

	}
	
	@Override
	public List<TypePackMads> allRemovedPackMads(){
		return proxyTypePackMadsDao.getAllRemovedPacks();

	}

	@Override
	public TypePackMads getByIdPack(int idPackMads) {
		return proxyTypePackMadsDao.getById(idPackMads);
	}
	
	}