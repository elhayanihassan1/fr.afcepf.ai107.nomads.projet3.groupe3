package fr.afcepf.ai107.nomads.partenaire.business;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;

import fr.afcepf.ai107.nomads.entities.InterventionReelle;
import fr.afcepf.ai107.nomads.entities.Prestation;
import fr.afcepf.ai107.nomads.idao.PartenaireIDao;
import fr.afcepf.ai107.nomads.partenaire.ibusiness.CotePartenaireIBusiness;

@Remote(CotePartenaireIBusiness.class)
@Stateless
public class CotePartenaireBusiness implements CotePartenaireIBusiness {
	
    @EJB
	private PartenaireIDao proxyCotePartenaireDao;
	@Override
	public void ajoutDemandeDevisPartenaire(Prestation prestation) {
		proxyCotePartenaireDao.addPrestation(prestation);	
	}

	@Override
	public void AjoutIntervention(InterventionReelle interventionReelle) {
		
		proxyCotePartenaireDao.addIntervention(interventionReelle);
		
	}

}
