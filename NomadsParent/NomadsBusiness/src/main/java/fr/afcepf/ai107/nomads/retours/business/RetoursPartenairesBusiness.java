package fr.afcepf.ai107.nomads.retours.business;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;

import fr.afcepf.ai107.nomads.entities.InterventionReelle;
import fr.afcepf.ai107.nomads.entities.Masseur;
import fr.afcepf.ai107.nomads.entities.Partenaire;
import fr.afcepf.ai107.nomads.entities.RetourExperiencePartenaire;
import fr.afcepf.ai107.nomads.idao.InterventionReelleIDao;
import fr.afcepf.ai107.nomads.idao.MasseurIDao;
import fr.afcepf.ai107.nomads.idao.PartenaireIDao;
import fr.afcepf.ai107.nomads.idao.PrestationIDao;
import fr.afcepf.ai107.nomads.idao.RetoursPartenairesIDao;
import fr.afcepf.ai107.nomads.retours.ibusiness.RetoursPartenairesIBusiness;

@Remote(RetoursPartenairesIBusiness.class)
@Stateless
public class RetoursPartenairesBusiness implements RetoursPartenairesIBusiness {
	
	@EJB
	private PrestationIDao proxyPrestationDao;
	
	@EJB
	private PartenaireIDao proxyPartenaireDao;
	
	@EJB
	private InterventionReelleIDao proxyInterventionReelleDao;
	
	@EJB
	private RetoursPartenairesIDao proxyRetoursPartenairesDao;
	
	@EJB
	private MasseurIDao proxyMasseurDao;
	
	public List<RetourExperiencePartenaire> getCurrentMonthRetourPartenaire() {
		return proxyRetoursPartenairesDao.getCurrentMonthRetourPartenaire();
	}

	@Override
	public RetourExperiencePartenaire ajouterUnRetour(RetourExperiencePartenaire ret) {
		// TODO Auto-generated method stub
		return proxyRetoursPartenairesDao.insertRetourPartenaire(ret);
	}

	@Override
	public List<RetourExperiencePartenaire> getRetourByIntervEtMasseur(InterventionReelle interv, Partenaire part) {
		// TODO Auto-generated method stub
		return proxyRetoursPartenairesDao.getRetourByInterventionEtMasseur(interv, part);
	}

}
