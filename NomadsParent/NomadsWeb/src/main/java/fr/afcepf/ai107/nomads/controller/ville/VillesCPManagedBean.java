package fr.afcepf.ai107.nomads.controller.ville;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import fr.afcepf.ai107.nomads.outils.ibusiness.OutilsIBusiness;

@ManagedBean (name="mbCPVilles")
@ViewScoped
public class VillesCPManagedBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	
	private List<String> codesPostaux = new ArrayList<String>();
	 
	private List<String> villes = new ArrayList<String>();
	 
	private String selectedCP;
	 
	private String selectedVille;
	 

	@EJB
	private OutilsIBusiness proxyOutilsBusiness;
	 
	public void onCPChange() { 
		if (selectedCP !=null && !selectedCP.equals("")) {
			villes = proxyOutilsBusiness.listeVilleByCp(selectedCP);
		}else {
			villes = new ArrayList<String>();
		}
	}

	 
	@PostConstruct
	public void init() {
		
		codesPostaux = proxyOutilsBusiness.listeCp();
		
	
	}


	public List<String> getCodesPostaux() {
		return codesPostaux;
	}


	public void setCodesPostaux(List<String> codesPostaux) {
		this.codesPostaux = codesPostaux;
	}


	public List<String> getVilles() {
		return villes;
	}


	public void setVilles(List<String> villes) {
		this.villes = villes;
	}


	public String getSelectedCP() {
		return selectedCP;
	}


	public void setSelectedCP(String selectedCP) {
		this.selectedCP = selectedCP;
	}


	public String getSelectedVille() {
		return selectedVille;
	}


	public void setSelectedVille(String selectedVille) {
		this.selectedVille = selectedVille;
	}


}
