package fr.afcepf.ai107.nomads.contoller.menu;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import org.primefaces.model.menu.DefaultMenuItem;
import org.primefaces.model.menu.DefaultMenuModel;
import org.primefaces.model.menu.DefaultSubMenu;
import org.primefaces.model.menu.MenuModel;

import fr.afcepf.ai107.nomads.entities.Masseur;

@ManagedBean
@ViewScoped
public class MenuBeanMasseur {
	private MenuModel model = new DefaultMenuModel();
	
//	@ManagedProperty (value="#{mbAccount.masseur}")
//	private Masseur masseurConnected;
//	private Integer idMasseurCo;

	@PostConstruct //à l'initialisation, création des menus?
	public void init() {
		addMenu("Accueil", "retour à mon Accueil");
	//	addMenu("Mon Profil", "mettre à jour mon profil");
		addMenu("Mon Profil", "Voir mon profil", "Acheter des Mads");
        addMenu("Mes Prestations", "Liste des prestations");
        
}


	//Constructeur par défaut pour ajouter des sous-menus?
	public DefaultSubMenu addMenu(String label, String... items) {  
		return addMenu(null, label, items);
	}

	//Constructeur surchargé avec un sous menu "Parent" en param
	public DefaultSubMenu addMenu(DefaultSubMenu parentMenu,String label, String... items) {
		
		DefaultSubMenu theMenu = new DefaultSubMenu(label);
		
		for (Object item : items) {
			DefaultMenuItem mi = new DefaultMenuItem(item);
			mi.setUrl(displayURL(item.toString()));
			theMenu.addElement(mi);
		}
		
				
		if (parentMenu == null) {
			model.addElement(theMenu);
		} else {
			parentMenu.addElement(theMenu);
		}
		return theMenu;
	}

	public MenuModel getMenuModel() {
		return model;
	}
	
	private String displayURL (String nomMenu) {
		
		String URLretournee = "#";
		switch (nomMenu) {
		case "retour à mon Accueil":
			URLretournee="AccueilMasseur.xhtml";
			break;
		case "Accueil":
			URLretournee="AccueilMasseur.xhtml";
			break;
//		case "Voir mon profil":
//			URLretournee="FicheMasseurAdm.xhtml?id=";  //PB!!!!!!!!!!!!!!!
//			break;
		case "Voir mon profil":
			URLretournee="FicheMasseurMass.xhtml";
		break;
		case "Acheter des Mads":
			URLretournee="AcheterMads.xhtml?faces-redirect=true";
			break;
		case "Liste des prestations":
			URLretournee="MesPrestationsMasseur.xhtml";
			break;
		default:
			break;
		}
	
		return URLretournee;
	}	
}
