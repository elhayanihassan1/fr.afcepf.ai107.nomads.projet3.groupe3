package fr.afcepf.ai107.nomads.controller.partenaire;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import fr.afcepf.ai107.nomads.entities.Administrateur;
import fr.afcepf.ai107.nomads.entities.IndisponibilitePartenaire;
import fr.afcepf.ai107.nomads.entities.InterventionReelle;
import fr.afcepf.ai107.nomads.entities.Partenaire;
import fr.afcepf.ai107.nomads.entities.RetourExperiencePartenaire;
import fr.afcepf.ai107.nomads.partenaire.ibusiness.PartenaireIBusiness;
import fr.afcepf.ai107.nomads.prestation.ibusiness.MotifAnnulationPrestationIBusiness;
import fr.afcepf.ai107.nomads.prestation.ibusiness.PrestationIBusiness;
import fr.afcepf.ai107.nomads.retours.ibusiness.RetoursPartenairesIBusiness;

@ManagedBean(name = "mbInfoPartenaire")
@ViewScoped
public class InfoPartenaireManagedBean implements Serializable{


	private static final long serialVersionUID = 1L;

	@EJB 
	private PartenaireIBusiness proxyInfosPartenaireBusiness;
	@EJB
	private PrestationIBusiness proxyPrestationBusiness;
	@EJB
	private MotifAnnulationPrestationIBusiness proxyMotifAnnulationPrestaBusiness;

	
	@EJB
	private RetoursPartenairesIBusiness proxyRetourPart;
	
	@ManagedProperty (value = "#{mbAccount.partenaire}" )
	private Partenaire partenaireConected;

	@ManagedProperty (value = "#{mbAccount.admin}" )
	private Administrateur administrateur;


	private Partenaire partenaire;
	private Partenaire partenaireSelected;
	private List<InterventionReelle> prestationAVenir ;
	private List<InterventionReelle> prestationPasse ;
	private String estDisponible;
	private IndisponibilitePartenaire indispoPart = null;	

	private String typeAccount = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("typeAccount");
	private InterventionReelle interventionAAnnuler;
	private String lien;
	private Date currentDate=new Date();
	private LocalDateTime localDateTime = LocalDateTime.ofInstant(currentDate.toInstant(), ZoneId.systemDefault());
	private Date dateFromLocalDT = Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
	
	//-------------- Anulation -------
	private String motifAnulation;
	private List<String> motifs;

	
	private InterventionReelle selectedIntervention;
	private List<RetourExperiencePartenaire> monRetour;
	
	//-------------- Anulation -------

	private boolean part=false;
	private boolean admin=false;
	
	@PostConstruct
	public void init(){

	
		if(administrateur!=null) {
			int idSelected=Integer.parseInt(FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("idPartSelected"));
			partenaireSelected = proxyInfosPartenaireBusiness.getInfosPartenaireById(idSelected);
			partenaire=partenaireSelected;
			admin=true;
		}
		if(partenaireConected!=null) {
			partenaire=partenaireConected;
			part=true;
		}
		prestationAVenir =  proxyInfosPartenaireBusiness.getAllInterventionAvenirByPArtenaireBu(partenaire.getIdPartenaire());
		prestationPasse =  proxyInfosPartenaireBusiness.getAllInterventionPasseesByPArtenaireBu(partenaire.getIdPartenaire());



		motifs=proxyMotifAnnulationPrestaBusiness.getListeMotifsAnnulationPrestation();
	}
		public String annulerPrestation() {
			if(motifAnulation!=null) {
				interventionAAnnuler.setMotifAnnulationPrestation(proxyMotifAnnulationPrestaBusiness.getMotifByLibelleMotif(motifAnulation));			}
			interventionAAnnuler.setDateAnnulationIntervention(dateFromLocalDT);
			proxyMotifAnnulationPrestaBusiness.updateInterventionAvecMotif(interventionAAnnuler);
			return "";
		}
		public String modifierPartenaire() {
			String lien="";
			if(administrateur!=null) {
			lien = "ModifierPartenaire.xhtml?faces-redirect=true&idPartSelected="+partenaire.getIdPartenaire();
			}
			if (partenaireConected!=null) {
			lien = "ModifierPartenaire.xhtml?faces-redirect=true";
			}
			return lien;
		}
		
		public String faireUnRetourDexperience() {
			System.out.println(selectedIntervention);
			String lol = null;
			//System.out.println(partenaire.getIdPartenaire()+"jzeifhoezi");
			//partenaire = proxyInfosPartenaireBusiness.getInfosPartenaireById(partenaireId);
			monRetour = proxyRetourPart.getRetourByIntervEtMasseur(selectedIntervention, partenaireConected);
			System.out.println(monRetour + "PKZRFGZPRIGHZOIFHZIGHZEPIFJZEPGJZP");
			if(monRetour == null) {
				//System.out.println(partenaire.getIdPartenaire() + "fojzepjfrogjproegj^repogkêprgkêprgk");
			lol = "/RetourExperiencePartenaire.xhtml?faces-redirect=true&id="+selectedIntervention.getIdInterventionReelle();
			}else {
				lol = "/AffichageRetourPartenaire.xhtml?faces-redirect=true&id="+selectedIntervention.getIdInterventionReelle();
			}
			
			return lol;
			
		}
		
		
		
		
		/**
		 * @return the selectedIntervention
		 */
		public InterventionReelle getSelectedIntervention() {
			return selectedIntervention;
		}
		/**
		 * @param selectedIntervention the selectedIntervention to set
		 */
		public void setSelectedIntervention(InterventionReelle selectedIntervention) {
			this.selectedIntervention = selectedIntervention;
		}
		/**
		 * @return the monRetour
		 */
		public List<RetourExperiencePartenaire> getMonRetour() {
			return monRetour;
		}
		/**
		 * @param monRetour the monRetour to set
		 */
		public void setMonRetour(List<RetourExperiencePartenaire> monRetour) {
			this.monRetour = monRetour;
		}
		public PartenaireIBusiness getProxyInfosPartenaireBusiness() {
			return proxyInfosPartenaireBusiness;
		}

		public void setProxyInfosPartenaireBusiness(PartenaireIBusiness proxyInfosPartenaireBusiness) {
			this.proxyInfosPartenaireBusiness = proxyInfosPartenaireBusiness;
		}

		public PrestationIBusiness getProxyPrestationBusiness() {
			return proxyPrestationBusiness;
		}

		public void setProxyPrestationBusiness(PrestationIBusiness proxyPrestationBusiness) {
			this.proxyPrestationBusiness = proxyPrestationBusiness;
		}

		public Partenaire getPartenaire() {
			return partenaire;
		}

		public void setPartenaire(Partenaire partenaire) {
			this.partenaire = partenaire;
		}

		public List<InterventionReelle> getPrestationAVenir() {
			return prestationAVenir;
		}

		public void setPrestationAVenir(List<InterventionReelle> prestationAVenir) {
			this.prestationAVenir = prestationAVenir;
		}

		public List<InterventionReelle> getPrestationPasse() {
			return prestationPasse;
		}

		public void setPrestationPasse(List<InterventionReelle> prestationPasse) {
			this.prestationPasse = prestationPasse;
		}

		public String getEstDisponible() {
			return estDisponible;
		}

		public void setEstDisponible(String estDisponible) {
			this.estDisponible = estDisponible;
		}

		public IndisponibilitePartenaire getIndispoPart() {
			return indispoPart;
		}

		public void setIndispoPart(IndisponibilitePartenaire indispoPart) {
			this.indispoPart = indispoPart;
		}
		
		public String getLien() {
			return lien;
		}
		public void setLien(String lien) {
			this.lien = lien;
		}

		public String getMotifAnulation() {
			return motifAnulation;
		}
		public void setMotifAnulation(String motifAnulation) {
			this.motifAnulation = motifAnulation;
		}
		public List<String> getMotifs() {
			return motifs;
		}

		public void setMotifs(List<String> motifs) {
			this.motifs = motifs;
		}
		public InterventionReelle getInterventionAAnnuler() {
			return interventionAAnnuler;
		}
		public void setInterventionAAnnuler(InterventionReelle interventionAAnnuler) {
			this.interventionAAnnuler = interventionAAnnuler;
		}
		public Date getCurrentDate() {
			return currentDate;
		}
		public void setCurrentDate(Date currentDate) {
			this.currentDate = currentDate;
		}
		public LocalDateTime getLocalDateTime() {
			return localDateTime;
		}
		public void setLocalDateTime(LocalDateTime localDateTime) {
			this.localDateTime = localDateTime;
		}
		public Date getDateFromLocalDT() {
			return dateFromLocalDT;
		}
		public void setDateFromLocalDT(Date dateFromLocalDT) {
			this.dateFromLocalDT = dateFromLocalDT;
		}

		public Partenaire getPartenaireSelected() {
			return partenaireSelected;
		}
		public void setPartenaireSelected(Partenaire partenaireSelected) {
			this.partenaireSelected = partenaireSelected;
		}
		public Partenaire getPartenaireConected() {
			return partenaireConected;
		}
		public void setPartenaireConected(Partenaire partenaireConected) {
			this.partenaireConected = partenaireConected;
		}
		public Administrateur getAdministrateur() {
			return administrateur;
		}
		public void setAdministrateur(Administrateur administrateur) {
			this.administrateur = administrateur;
		}
		public boolean isPart() {
			return part;
		}
		public void setPart(boolean part) {
			this.part = part;
		}
		public boolean isAdmin() {
			return admin;
		}
		public void setAdmin(boolean admin) {
			this.admin = admin;
		}

		public String getTypeAccount() {
			return typeAccount;
		}
		public void setTypeAccount(String typeAccount) {
			this.typeAccount = typeAccount;
		}

		

	}