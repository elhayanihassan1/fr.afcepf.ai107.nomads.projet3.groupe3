
package fr.afcepf.ai107.nomads.controller.prestation;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.inject.New;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

import fr.afcepf.ai107.nomads.entities.InscriptionMasseurs;
import fr.afcepf.ai107.nomads.entities.InterventionReelle;
import fr.afcepf.ai107.nomads.entities.LocationMateriel;
import fr.afcepf.ai107.nomads.entities.Masseur;
import fr.afcepf.ai107.nomads.entities.MotifAnnulationPrestation;
import fr.afcepf.ai107.nomads.entities.Partenaire;
import fr.afcepf.ai107.nomads.entities.Prestation;
import fr.afcepf.ai107.nomads.entities.RetourExperienceMasseur;
import fr.afcepf.ai107.nomads.entities.RetourExperiencePartenaire;
import fr.afcepf.ai107.nomads.entities.TypePaiement;
import fr.afcepf.ai107.nomads.entities.TypePartenaire;
import fr.afcepf.ai107.nomads.entities.VilleCp;
import fr.afcepf.ai107.nomads.masseurs.ibusiness.MasseurIBusiness;
import fr.afcepf.ai107.nomads.outils.ibusiness.OutilsIBusiness;
import fr.afcepf.ai107.nomads.partenaire.ibusiness.PartenaireIBusiness;
import fr.afcepf.ai107.nomads.prestation.ibusiness.PrestationIBusiness;


@ManagedBean(name ="mbAjoutPrestaInterv")
@ViewScoped
public class AjoutPrestationEtIntervention implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@EJB
	private PrestationIBusiness proxyPrestationBusiness;
	
	@EJB
	private OutilsIBusiness proxyOutilBusiness;
	
	@EJB
	private PartenaireIBusiness proxyPartenaireBusiness;

	@EJB
	private MasseurIBusiness proxyMasseur;
	
	private Prestation prestation = new Prestation();
	private Integer idPrestation; 
	private String libellePrestation;
	private Double coutDevisPrestation;
	private Date dateEmissionPrestation;
	private Date dateValidationPrestation;
	private Date heureDebutPrestation;
	private Date heureFinPrestation;
	private Integer nombreMasseursPrestation;
	private Integer nombreChaisesErgonomiquesPrestation;
	private Double montantRemunerationPrestation;
	private Date dateAnnulationPrestation;
	private Date datePaiementPrestation;
	private String adresseInterventionPrestation;
	private String specificitesPrestation;
	private Integer coutMads;
	private Partenaire partenaire; 
	private VilleCp villeCp;
	private MotifAnnulationPrestation motifAnnulationPrestation;
	private TypePaiement typePaiement; 
	private List<LocationMateriel> locationMateriel;
	
	//private InterventionReelle interventionReelle = new InterventionReelle();
	private Integer idInterventionReelle;
	private Date datePrestation;
	private Date secondedatePresta;
	private List<InscriptionMasseurs> inscriptionMasseur;
	private Set<RetourExperienceMasseur> retoursExperienceMasseurs;
	private List<RetourExperiencePartenaire> retourExperiencePartenaire;
	
	private List<String> codesPostaux = new ArrayList<String>();
	private List<String> villes = new ArrayList<String>();
	private String selectedCP;
	private String selectedVille;
	
	//PARTENAIRE
	private List<String> partenaires = new ArrayList<String>();
	private String unPartenaire;
	private List<Partenaire> selectedPartenaires = new ArrayList<Partenaire>();
	//POUR AFFICHAGE COMPLET DE LA LISTE DES PARTENAIRES 
	private List<Partenaire> mesPartenaires = new ArrayList<Partenaire>();
	private int idSelectedPartenaire;
	
	private List<Masseur> masseurs = new ArrayList<Masseur>();
//	private List<String> selectedMasseurs = new ArrayList<String>();
	private String masseur;
	private Masseur idSelectedMasseur;
	private List<Masseur> idSelectedMasseurs = new ArrayList<Masseur>();
	
	//TYPE PARTENAIRE
	private List<TypePartenaire> typePartenaires = new ArrayList<TypePartenaire>();
	private TypePartenaire selectedTypePartenaire;
	private Integer idTypePartenaire;
	
	private boolean disabled = false; 
	private boolean bis = false;
	private String typeDePaiement;
	@SuppressWarnings("serial")
	private List<SelectItem> typeDePaiements = new ArrayList<SelectItem>() {{
		add(new SelectItem(1, "Devis"));
		add(new SelectItem(2, "Mads"));
	}};
	private List<String> typePaiementsString = new ArrayList<String>();
	
	
	private List<Date> datesInterventions = new ArrayList<Date>();

	
	private int pas;
	private int dureeRecurrence;
	private int compteurRecurrence;


	//----------------------------------- ON SELECTION CHANGE -------------------------------------------------
	public void onCPChange() { 
		if (selectedCP !=null && !selectedCP.equals("")) {
			villes = proxyOutilBusiness.listeVilleByCp(selectedCP);
		}else {
			villes = new ArrayList<String>();
		}
	}
	
	public void onTypePartenaireChange() {
		System.out.println(idTypePartenaire);
		if (idTypePartenaire != null && !idTypePartenaire.equals("")) {
			mesPartenaires = proxyPrestationBusiness.getPartenaireParType(idTypePartenaire);
		}else {
			mesPartenaires = new ArrayList<Partenaire>();
		}
	}
	
	 public void onTypePartChangeMasseur() {

		 if (idTypePartenaire != null && !idTypePartenaire.equals("")) {
				masseurs = proxyMasseur.getMasseurByIdCategForCreationPresta(idTypePartenaire);
			}else {
				masseurs = new ArrayList<Masseur>();
			}
	 }

	public void OnTypeDePaiementChange() {
		disabled = false;
		bis = false;
		if (typeDePaiement != null) {
			if (typeDePaiement.equals("1")) {
				disabled = false;
				bis = true;
			}
			else if (typeDePaiement.equals("2")) {
				disabled = true; 
				bis = false;
			}
		}
	
	}

	
	
	
	public List<Date> getRecurrence(){
		List<Date> datesInterv = new ArrayList<Date>();
		boolean changementDateDeReference = false;
		compteurRecurrence = 0;
		Date newDate = new Date ();
		datesInterv.add(datePrestation);
		for (int i = compteurRecurrence; compteurRecurrence<dureeRecurrence; i++) {
			Calendar c = Calendar.getInstance();
			
			if (changementDateDeReference == false) {
			c.setTime(datePrestation);
			c.add(Calendar.DATE, pas);
			newDate = c.getTime();
			datesInterv.add(newDate);
			}
			else if (changementDateDeReference = true) {
					c.setTime(newDate);
					c.add(Calendar.DATE, pas);
					newDate = c.getTime();
					datesInterv.add(newDate);
				}
	
		changementDateDeReference = true;
		compteurRecurrence++;
		}
		
		return datesInterv;
	}
	
	
	
	//------------------------------------ METHODES -----------------------------------------------------------

	@PostConstruct
	public void init() {
		
		codesPostaux = proxyOutilBusiness.listeCp();
		typePartenaires = proxyPrestationBusiness.getListeTypePartenaire();
	//	masseurs = proxyPrestationBusiness.getListeMasseurs();
	//	mesPartenaires = proxyPrestationBusiness.getListePartenaire();
		
	
	}

	public String AjouterPrestationEtIntervention() {
	
		if (adresseInterventionPrestation != null) {
		prestation.setAdresseInterventionPrestation(adresseInterventionPrestation);
			}
		if (coutDevisPrestation != null) {
		prestation.setCoutDevisPrestation(coutDevisPrestation);
			}
		if(coutMads != null) {
		prestation.setCoutMads(coutMads);
			}
		prestation.setDateEmissionPrestation(new Date());
		if(datePaiementPrestation != null) {
		prestation.setDatePaiementPrestation(datePaiementPrestation);
			}
		prestation.setDateValidationPrestation(new Date());
		if(heureDebutPrestation != null) {
		prestation.setHeureDebutPrestation(heureDebutPrestation);
			}
		if(heureFinPrestation != null) {
		prestation.setHeureFinPrestation(heureFinPrestation);
			}
		prestation.setLibellePrestation(libellePrestation);
		if(montantRemunerationPrestation != null) {
		prestation.setMontantRemunerationPrestation(montantRemunerationPrestation);
			}
		if(nombreChaisesErgonomiquesPrestation != null) {
		prestation.setNombreChaisesErgonomiquesPrestation(nombreChaisesErgonomiquesPrestation);
			}
		if(nombreMasseursPrestation != null) {
		prestation.setNombreMasseursPrestation(nombreMasseursPrestation);
			}
		if(specificitesPrestation != null) {
		prestation.setSpecificitesPrestation(specificitesPrestation);
			}
		//insertion ID ville
		if(selectedVille != null) {
		prestation.setVilleCp(proxyOutilBusiness.VilleCpParNomVille(selectedVille)); 
			}
		//insertion ID Partenaire
		Partenaire partenaire = new Partenaire();
		partenaire.setIdPartenaire(idSelectedPartenaire);
		prestation.setPartenaire(partenaire);
		
	
		//insertion Intervention Reelle et inscriptions masseurs
		List<InterventionReelle> interventionR = new ArrayList<InterventionReelle>();
		for (Date datePresta : getRecurrence()) { 
		InterventionReelle inter = new InterventionReelle();
		inter.setIdInterventionReelle(null);
		inter.setDatePrestation(datePresta); 
		inter.setPrestation(prestation);
		inter.setRetoursExperienceMasseurs(null);
		Set<InscriptionMasseurs> inscriptionMasseurs = new HashSet<InscriptionMasseurs>();
		for(Masseur mass : idSelectedMasseurs) {
		InscriptionMasseurs inscription = new InscriptionMasseurs();
		inscription.setIdInscriptionMasseur(null);
		inscription.setDateDesinscriptionMasseur(null);
		inscription.setDateInscriptionMasseur(new Date());
		inscription.setInterventionReelle(inter);
		inscription.setMotifDesinscriptionPrestation(null);
		inscription.setMasseur(mass);
		inscriptionMasseurs.add(inscription);
		}
		
		inter.setInscriptionMasseur(inscriptionMasseurs);
		interventionR.add(inter);
		} // fin du for Date
		prestation.setInterventionReelle(interventionR);
		proxyPrestationBusiness.ajoutPrestation(prestation); // AJOUT DE LA PRESTATION 
	
		return "/ListePrestations.xhtml?faces-redirect=true";
		
	}
	

	public void demo() {
		
		
//		typePaiement; 
//		partenaire; 
//		
		libellePrestation = "L'Happy Bouboule";
		nombreMasseursPrestation = 6;
		coutMads =6;
//		coutDevisPrestation;
		try {
		datePrestation = (new SimpleDateFormat("dd-MM-yyyy")).parse("02-06-2020");
		pas = 7;
		dureeRecurrence = 5;
		heureDebutPrestation = (new SimpleDateFormat("hh:mm")).parse("19:00") ;
		heureFinPrestation = (new SimpleDateFormat("hh:mm")).parse("21:30");
		} catch (ParseException e) {
			e.printStackTrace();
			}
		
		nombreChaisesErgonomiquesPrestation = 0;
		adresseInterventionPrestation = "79, rue de Dunkerque";
		
//		villeCp;
		
		specificitesPrestation = "Equipe mixte souhaitée. Attention: forte affluence prévue. Désaltérations offertes";
		
		
//		dateEmissionPrestation;
//		dateValidationPrestation;
//		montantRemunerationPrestation;
//		dateAnnulationPrestation;
//		motifAnnulationPrestation;
//		locationMateriel;
	}
	

	//----------------------------------------------- GETTERS SETTERS -------------------------------------------

	
	
	
	/**
	 * @return the typeDePaiement
	 */
	public String getTypeDePaiement() {
		return typeDePaiement;
	}

	/**
	 * @return the pas
	 */
	public int getPas() {
		return pas;
	}

	/**
	 * @param pas the pas to set
	 */
	public void setPas(int pas) {
		this.pas = pas;
	}

	/**
	 * @return the dureeRecurrence
	 */
	public int getDureeRecurrence() {
		return dureeRecurrence;
	}

	/**
	 * @param dureeRecurrence the dureeRecurrence to set
	 */
	public void setDureeRecurrence(int dureeRecurrence) {
		this.dureeRecurrence = dureeRecurrence;
	}

	/**
	 * @return the compteurRecurrence
	 */
	public int getCompteurRecurrence() {
		return compteurRecurrence;
	}

	/**
	 * @param compteurRecurrence the compteurRecurrence to set
	 */
	public void setCompteurRecurrence(int compteurRecurrence) {
		this.compteurRecurrence = compteurRecurrence;
	}

	/**
	 * @return the secondedatePresta
	 */
	public Date getSecondedatePresta() {
		return secondedatePresta;
	}

	/**
	 * @param secondedatePresta the secondedatePresta to set
	 */
	public void setSecondedatePresta(Date secondedatePresta) {
		this.secondedatePresta = secondedatePresta;
	}


	/**
	 * @return the datesInterventions
	 */
	public List<Date> getDatesInterventions() {
		return datesInterventions;
	}

	/**
	 * @param datesInterventions the datesInterventions to set
	 */
	public void setDatesInterventions(List<Date> datesInterventions) {
		this.datesInterventions = datesInterventions;
	}


	/**
	 * @return the bis
	 */
	public boolean isBis() {
		return bis;
	}

	/**
	 * @param bis the bis to set
	 */
	public void setBis(boolean bis) {
		this.bis = bis;
	}

	/**
	 * @return the disabled
	 */
	public boolean isDisabled() {
		return disabled;
	}

	/**
	 * @param disabled the disabled to set
	 */
	public void setDisabled(boolean disabled) {
		this.disabled = disabled;
	}

	/**
	 * @return the typePaiementsString
	 */
	public List<String> getTypePaiementsString() {
		return typePaiementsString;
	}

	/**
	 * @param typePaiementsString the typePaiementsString to set
	 */
	public void setTypePaiementsString(List<String> typePaiementsString) {
		this.typePaiementsString = typePaiementsString;
	}

	/**
	 * @return the typeDePaiements
	 */
	public List<SelectItem> getTypeDePaiements() {
		return typeDePaiements;
	}

	/**
	 * @param typeDePaiements the typeDePaiements to set
	 */
	public void setTypeDePaiements(List<SelectItem> typeDePaiements) {
		this.typeDePaiements = typeDePaiements;
	}

	/**
	 * @param typeDePaiement the typeDePaiement to set
	 */
	public void setTypeDePaiement(String typeDePaiement) {
		this.typeDePaiement = typeDePaiement;
	}
	
	/**
	 * @return the idTypePartenaire
	 */
	public Integer getIdTypePartenaire() {
		return idTypePartenaire;
	}

	/**
	 * @param idTypePartenaire the idTypePartenaire to set
	 */
	public void setIdTypePartenaire(Integer idTypePartenaire) {
		this.idTypePartenaire = idTypePartenaire;
	}
	
	/**
	 * @return the typePartenaires
	 */
	public List<TypePartenaire> getTypePartenaires() {
		return typePartenaires;
	}

	/**
	 * @param typePartenaires the typePartenaires to set
	 */
	public void setTypePartenaires(List<TypePartenaire> typePartenaires) {
		this.typePartenaires = typePartenaires;
	}

	/**
	 * @return the selectedTypePartenaire
	 */
	public TypePartenaire getSelectedTypePartenaire() {
		return selectedTypePartenaire;
	}

	/**
	 * @param selectedTypePartenaire the selectedTypePartenaire to set
	 */
	public void setSelectedTypePartenaire(TypePartenaire selectedTypePartenaire) {
		this.selectedTypePartenaire = selectedTypePartenaire;
	}

	/**
	 * @return the idSelectedMasseurs
	 */
	public List<Masseur> getIdSelectedMasseurs() {
		return idSelectedMasseurs;
	}

	/**
	 * @param idSelectedMasseurs the idSelectedMasseurs to set
	 */
	public void setIdSelectedMasseurs(List<Masseur> idSelectedMasseurs) {
		this.idSelectedMasseurs = idSelectedMasseurs;
	}

	/**
	 * @return the idSelectedMasseur
	 */
	public Masseur getIdSelectedMasseur() {
		return idSelectedMasseur;
	}

	/**
	 * @param idSelectedMasseur the idSelectedMasseur to set
	 */
	public void setIdSelectedMasseur(Masseur idSelectedMasseur) {
		this.idSelectedMasseur = idSelectedMasseur;
	}

	/**
	 * @return the idSelectedPartenaire
	 */
	public int getIdSelectedPartenaire() {
		return idSelectedPartenaire;
	}

	/**
	 * @param idSelectedPartenaire the idSelectedPartenaire to set
	 */
	public void setIdSelectedPartenaire(int idSelectedPartenaire) {
		this.idSelectedPartenaire = idSelectedPartenaire;
	}

	/**
	 * @return the mesPartenaires
	 */
	public List<Partenaire> getMesPartenaires() {
		return mesPartenaires;
	}

	/**
	 * @param mesPartenaires the mesPartenaires to set
	 */
	public void setMesPartenaires(List<Partenaire> mesPartenaires) {
		this.mesPartenaires = mesPartenaires;
	}

	/**
	 * @return the masseur
	 */
	public String getMasseur() {
		return masseur;
	}


	/**
	 * @param masseur the masseur to set
	 */
	public void setMasseur(String masseur) {
		this.masseur = masseur;
	}


	/**
	 * @return the unPartenaire
	 */
	public String getUnPartenaire() {
		return unPartenaire;
	}

	/**
	 * @param unPartenaire the unPartenaire to set
	 */
	public void setUnPartenaire(String unPartenaire) {
		this.unPartenaire = unPartenaire;
	}

	/**
	 * @return the partenaire
	 */
	public Partenaire getPartenaire() {
		return partenaire;
	}

	/**
	 * @param partenaire the partenaire to set
	 */
	public void setPartenaire(Partenaire partenaire) {
		this.partenaire = partenaire;
	}

	/**
	 * @return the masseurs
	 */
	public List<Masseur> getMasseurs() {
		return masseurs;
	}

	/**
	 * @param masseurs the masseurs to set
	 */
	public void setMasseurs(List<Masseur> masseurs) {
		this.masseurs = masseurs;
	}

	/**
	 * @return the selectedMasseurs
//	 */
//	public List<String> getSelectedMasseurs() {
//		return selectedMasseurs;
//	}
//
//	/**
//	 * @param selectedMasseurs the selectedMasseurs to set
//	 */
//	public void setSelectedMasseurs(List<String> selectedMasseurs) {
//		this.selectedMasseurs = selectedMasseurs;
//	}

	/**
	 * @return the selectedPartenaires
	 */
	public List<Partenaire> getSelectedPartenaires() {
		return selectedPartenaires;
	}

	/**
	 * @param selectedPartenaires the selectedPartenaires to set
	 */
	public void setSelectedPartenaires(List<Partenaire> selectedPartenaires) {
		this.selectedPartenaires = selectedPartenaires;
	}

	/**
	 * @return the partenaires
	 */
	public List<String> getPartenaires() {
		return partenaires;
	}

	/**
	 * @param partenaires the partenaires to set
	 */
	public void setPartenaires(List<String> partenaires) {
		this.partenaires = partenaires;
	}


	/**
	 * @return the proxyPrestationBusiness
	 */
	public PrestationIBusiness getProxyPrestationBusiness() {
		return proxyPrestationBusiness;
	}


	/**
	 * @param proxyPrestationBusiness the proxyPrestationBusiness to set
	 */
	public void setProxyPrestationBusiness(PrestationIBusiness proxyPrestationBusiness) {
		this.proxyPrestationBusiness = proxyPrestationBusiness;
	}


	/**
	 * @return the proxyOutilBusiness
	 */
	public OutilsIBusiness getProxyOutilBusiness() {
		return proxyOutilBusiness;
	}


	/**
	 * @param proxyOutilBusiness the proxyOutilBusiness to set
	 */
	public void setProxyOutilBusiness(OutilsIBusiness proxyOutilBusiness) {
		this.proxyOutilBusiness = proxyOutilBusiness;
	}


	/**
	 * @return the prestation
	 */
	public Prestation getPrestation() {
		return prestation;
	}


	/**
	 * @param prestation the prestation to set
	 */
	public void setPrestation(Prestation prestation) {
		this.prestation = prestation;
	}


	/**
	 * @return the idPrestation
	 */
	public Integer getIdPrestation() {
		return idPrestation;
	}


	/**
	 * @param idPrestation the idPrestation to set
	 */
	public void setIdPrestation(Integer idPrestation) {
		this.idPrestation = idPrestation;
	}


	/**
	 * @return the libellePrestation
	 */
	public String getLibellePrestation() {
		return libellePrestation;
	}


	/**
	 * @param libellePrestation the libellePrestation to set
	 */
	public void setLibellePrestation(String libellePrestation) {
		this.libellePrestation = libellePrestation;
	}


	/**
	 * @return the coutDevisPrestation
	 */
	public Double getCoutDevisPrestation() {
		return coutDevisPrestation;
	}


	/**
	 * @param coutDevisPrestation the coutDevisPrestation to set
	 */
	public void setCoutDevisPrestation(Double coutDevisPrestation) {
		this.coutDevisPrestation = coutDevisPrestation;
	}


	/**
	 * @return the dateEmissionPrestation
	 */
	public Date getDateEmissionPrestation() {
		return dateEmissionPrestation;
	}


	/**
	 * @param dateEmissionPrestation the dateEmissionPrestation to set
	 */
	public void setDateEmissionPrestation(Date dateEmissionPrestation) {
		this.dateEmissionPrestation = dateEmissionPrestation;
	}


	/**
	 * @return the dateValidationPrestation
	 */
	public Date getDateValidationPrestation() {
		return dateValidationPrestation;
	}


	/**
	 * @param dateValidationPrestation the dateValidationPrestation to set
	 */
	public void setDateValidationPrestation(Date dateValidationPrestation) {
		this.dateValidationPrestation = dateValidationPrestation;
	}


	/**
	 * @return the heureDebutPrestation
	 */
	public Date getHeureDebutPrestation() {
		return heureDebutPrestation;
	}


	/**
	 * @param heureDebutPrestation the heureDebutPrestation to set
	 */
	public void setHeureDebutPrestation(Date heureDebutPrestation) {
		this.heureDebutPrestation = heureDebutPrestation;
	}


	/**
	 * @return the heureFinPrestation
	 */
	public Date getHeureFinPrestation() {
		return heureFinPrestation;
	}


	/**
	 * @param heureFinPrestation the heureFinPrestation to set
	 */
	public void setHeureFinPrestation(Date heureFinPrestation) {
		this.heureFinPrestation = heureFinPrestation;
	}


	/**
	 * @return the nombreMasseursPrestation
	 */
	public Integer getNombreMasseursPrestation() {
		return nombreMasseursPrestation;
	}


	/**
	 * @param nombreMasseursPrestation the nombreMasseursPrestation to set
	 */
	public void setNombreMasseursPrestation(Integer nombreMasseursPrestation) {
		this.nombreMasseursPrestation = nombreMasseursPrestation;
	}


	/**
	 * @return the nombreChaisesErgonomiquesPrestation
	 */
	public Integer getNombreChaisesErgonomiquesPrestation() {
		return nombreChaisesErgonomiquesPrestation;
	}


	/**
	 * @param nombreChaisesErgonomiquesPrestation the nombreChaisesErgonomiquesPrestation to set
	 */
	public void setNombreChaisesErgonomiquesPrestation(Integer nombreChaisesErgonomiquesPrestation) {
		this.nombreChaisesErgonomiquesPrestation = nombreChaisesErgonomiquesPrestation;
	}


	/**
	 * @return the montantRemunerationPrestation
	 */
	public Double getMontantRemunerationPrestation() {
		return montantRemunerationPrestation;
	}


	/**
	 * @param montantRemunerationPrestation the montantRemunerationPrestation to set
	 */
	public void setMontantRemunerationPrestation(Double montantRemunerationPrestation) {
		this.montantRemunerationPrestation = montantRemunerationPrestation;
	}


	/**
	 * @return the dateAnnulationPrestation
	 */
	public Date getDateAnnulationPrestation() {
		return dateAnnulationPrestation;
	}


	/**
	 * @param dateAnnulationPrestation the dateAnnulationPrestation to set
	 */
	public void setDateAnnulationPrestation(Date dateAnnulationPrestation) {
		this.dateAnnulationPrestation = dateAnnulationPrestation;
	}


	/**
	 * @return the datePaiementPrestation
	 */
	public Date getDatePaiementPrestation() {
		return datePaiementPrestation;
	}


	/**
	 * @param datePaiementPrestation the datePaiementPrestation to set
	 */
	public void setDatePaiementPrestation(Date datePaiementPrestation) {
		this.datePaiementPrestation = datePaiementPrestation;
	}


	/**
	 * @return the adresseInterventionPrestation
	 */
	public String getAdresseInterventionPrestation() {
		return adresseInterventionPrestation;
	}


	/**
	 * @param adresseInterventionPrestation the adresseInterventionPrestation to set
	 */
	public void setAdresseInterventionPrestation(String adresseInterventionPrestation) {
		this.adresseInterventionPrestation = adresseInterventionPrestation;
	}


	/**
	 * @return the specificitesPrestation
	 */
	public String getSpecificitesPrestation() {
		return specificitesPrestation;
	}


	/**
	 * @param specificitesPrestation the specificitesPrestation to set
	 */
	public void setSpecificitesPrestation(String specificitesPrestation) {
		this.specificitesPrestation = specificitesPrestation;
	}


	/**
	 * @return the coutMads
	 */
	public Integer getCoutMads() {
		return coutMads;
	}


	/**
	 * @param coutMads the coutMads to set
	 */
	public void setCoutMads(Integer coutMads) {
		this.coutMads = coutMads;
	}

	/**
	 * @return the villeCp
	 */
	public VilleCp getVilleCp() {
		return villeCp;
	}


	/**
	 * @param villeCp the villeCp to set
	 */
	public void setVilleCp(VilleCp villeCp) {
		this.villeCp = villeCp;
	}


	/**
	 * @return the motifAnnulationPrestation
	 */
	public MotifAnnulationPrestation getMotifAnnulationPrestation() {
		return motifAnnulationPrestation;
	}


	/**
	 * @param motifAnnulationPrestation the motifAnnulationPrestation to set
	 */
	public void setMotifAnnulationPrestation(MotifAnnulationPrestation motifAnnulationPrestation) {
		this.motifAnnulationPrestation = motifAnnulationPrestation;
	}


	/**
	 * @return the typePaiement
	 */
	public TypePaiement getTypePaiement() {
		return typePaiement;
	}


	/**
	 * @param typePaiement the typePaiement to set
	 */
	public void setTypePaiement(TypePaiement typePaiement) {
		this.typePaiement = typePaiement;
	}


	/**
	 * @return the locationMateriel
	 */
	public List<LocationMateriel> getLocationMateriel() {
		return locationMateriel;
	}


	/**
	 * @param locationMateriel the locationMateriel to set
	 */
	public void setLocationMateriel(List<LocationMateriel> locationMateriel) {
		this.locationMateriel = locationMateriel;
	}


	/**
	 * @return the interventionReelle
	 */
//	public InterventionReelle getInterventionReelle() {
//		return interventionReelle;
//	}


	/**
	 * @param interventionReelle the interventionReelle to set
	 */
//	public void setInterventionReelle(InterventionReelle interventionReelle) {
//		this.interventionReelle = interventionReelle;
//	}


	/**
	 * @return the idInterventionReelle
	 */
	public Integer getIdInterventionReelle() {
		return idInterventionReelle;
	}


	/**
	 * @param idInterventionReelle the idInterventionReelle to set
	 */
	public void setIdInterventionReelle(Integer idInterventionReelle) {
		this.idInterventionReelle = idInterventionReelle;
	}


	/**
	 * @return the datePrestation
	 */
	public Date getDatePrestation() {
		return datePrestation;
	}


	/**
	 * @param datePrestation the datePrestation to set
	 */
	public void setDatePrestation(Date datePrestation) {
		this.datePrestation = datePrestation;
	}


	/**
	 * @return the inscriptionMasseur
	 */
	public List<InscriptionMasseurs> getInscriptionMasseur() {
		return inscriptionMasseur;
	}


	/**
	 * @param inscriptionMasseur the inscriptionMasseur to set
	 */
	public void setInscriptionMasseur(List<InscriptionMasseurs> inscriptionMasseur) {
		this.inscriptionMasseur = inscriptionMasseur;
	}


	/**
	 * @return the retoursExperienceMasseurs
	 */
	public Set<RetourExperienceMasseur> getRetoursExperienceMasseurs() {
		return retoursExperienceMasseurs;
	}


	/**
	 * @param retoursExperienceMasseurs the retoursExperienceMasseurs to set
	 */
	public void setRetoursExperienceMasseurs(Set<RetourExperienceMasseur> retoursExperienceMasseurs) {
		this.retoursExperienceMasseurs = retoursExperienceMasseurs;
	}


	/**
	 * @return the retourExperiencePartenaire
	 */
	public List<RetourExperiencePartenaire> getRetourExperiencePartenaire() {
		return retourExperiencePartenaire;
	}


	/**
	 * @param retourExperiencePartenaire the retourExperiencePartenaire to set
	 */
	public void setRetourExperiencePartenaire(List<RetourExperiencePartenaire> retourExperiencePartenaire) {
		this.retourExperiencePartenaire = retourExperiencePartenaire;
	}


	/**
	 * @return the codesPostaux
	 */
	public List<String> getCodesPostaux() {
		return codesPostaux;
	}


	/**
	 * @param codesPostaux the codesPostaux to set
	 */
	public void setCodesPostaux(List<String> codesPostaux) {
		this.codesPostaux = codesPostaux;
	}


	/**
	 * @return the villes
	 */
	public List<String> getVilles() {
		return villes;
	}


	/**
	 * @param villes the villes to set
	 */
	public void setVilles(List<String> villes) {
		this.villes = villes;
	}


	/**
	 * @return the selectedCP
	 */
	public String getSelectedCP() {
		return selectedCP;
	}


	/**
	 * @param selectedCP the selectedCP to set
	 */
	public void setSelectedCP(String selectedCP) {
		this.selectedCP = selectedCP;
	}


	/**
	 * @return the selectedVille
	 */
	public String getSelectedVille() {
		return selectedVille;
	}


	/**
	 * @param selectedVille the selectedVille to set
	 */
	public void setSelectedVille(String selectedVille) {
		this.selectedVille = selectedVille;
	}
	
	
}



