package fr.afcepf.ai107.nomads.controller.masseurs;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import fr.afcepf.ai107.nomads.entities.AffectationCategorieMasseur;
import fr.afcepf.ai107.nomads.entities.CategorieMasseur;
import fr.afcepf.ai107.nomads.entities.EvolutionProfessionnelle;
import fr.afcepf.ai107.nomads.entities.HistoriqueEvolutionProfessionnelleMasseur;
import fr.afcepf.ai107.nomads.entities.Masseur;
import fr.afcepf.ai107.nomads.entities.Sexe;
import fr.afcepf.ai107.nomads.masseurs.ibusiness.MasseurIBusiness;
import fr.afcepf.ai107.nomads.outils.ibusiness.OutilsIBusiness;

//import org.omnifaces.cdi.ViewScoped;
//import org.omnifaces.util.selectitems.SelectItemsBuilder;

@ManagedBean (name = "mbMajMasseur")
@ViewScoped
public class MiseAJourMasseurManagedBean implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@EJB
	private MasseurIBusiness proxyMasseurBusiness;
	
	@EJB
	private OutilsIBusiness proxyOutilBusiness;
	
	@ManagedProperty(value = "#{mbAccount.masseur}")
	private Masseur masseurConnected;
	
	private Masseur masseur = new Masseur ();
	private int idMasseurRecupere;
	
//			private String nomRecup;
//			private String prenomRecup;
//			private String adresseRecup;
	
	private List<String> codesPostaux = new ArrayList<String>();
	private List<String> villes = new ArrayList<String>();
	private String selectedCP;
	private String selectedVille;
	private List<Sexe> listeSexe = new ArrayList<Sexe>();
	private int idSexeRecup;
	private Date dateDuJour;
	
	private List<AffectationCategorieMasseur> listeCategoriesAffectees;
	private AffectationCategorieMasseur selectedCategorieAffectee;
	private List<CategorieMasseur> listeCategoriesDisponiblesParMasseur;
	private Integer idSelectedCategorie;
	
	private EvolutionProfessionnelle selectedEvoPro;
	private HistoriqueEvolutionProfessionnelleMasseur selectedHistoEvoPro;
	private List<HistoriqueEvolutionProfessionnelleMasseur> listeHistoEvoPro;
	private Integer idSelectedEvoPro;
	private List<EvolutionProfessionnelle> listeEvoPro;
	
	
//	private String motDePasseActuel;
//	private String identifiantFourni;
//	private String nouveauMotDePasse;
//	private String nouveauMotDePasseConfirm;
//	private boolean Ok = false;


	
	@PostConstruct
	public void init() {
		System.out.println(FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("id"));
		idMasseurRecupere=Integer.parseInt(FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("id"));
		masseur = proxyMasseurBusiness.getInfosMasseurById(idMasseurRecupere);
		listeSexe = proxyMasseurBusiness.getSexes();
		codesPostaux = proxyOutilBusiness.listeCp();
		selectedCP = masseur.getVilleCp().getLibelleCp();
		selectedVille = masseur.getVilleCp().getLibelleVille();
		idSexeRecup = masseur.getSexe().getIdSexe();
		
		listeCategoriesAffectees = proxyOutilBusiness.getListAffectationCatMassByIdMass(idMasseurRecupere);
		dateDuJour = new Date();
		
		
		
		//listeCategoriesDisponiblesParMasseur = proxyOutilBusiness.getListCatMassDispoByIdMass(idMasseurRecupere);
		listeCategoriesDisponiblesParMasseur = proxyOutilBusiness.getAllCategories();
		System.out.println("//////////////////////////////////// Taille de la liste de catégories à affectées: " + listeCategoriesDisponiblesParMasseur.size());
		
		listeHistoEvoPro = proxyOutilBusiness.historiqueEvoProMassByIdMasseur(idMasseurRecupere);
		listeEvoPro = proxyOutilBusiness.getAll();
	}
	
	public void onCPChange() { 
		if (selectedCP !=null && !selectedCP.equals("")) {
			villes = proxyOutilBusiness.listeVilleByCp(selectedCP);
		}else {
			villes = new ArrayList<String>();
		}
	}
	
//	public void modifierMdp() {
//		boolean passwordIsOk = false;
//		boolean areTheSame = false;
//		passwordIsOk = proxyMasseurBusiness.identifiantsOk(identifiantFourni, motDePasseActuel);
//		if (nouveauMotDePasse.equals(nouveauMotDePasseConfirm)) {
//			areTheSame=true;
//		}
//		if (passwordIsOk && areTheSame) {
//			Ok = true;
//		}
//		System.out.println(Ok);
//	}
//	
	
	
	public String mettreAJour() {
//		if (Ok == true) {
//			masseur.setPasswordMasseur(nouveauMotDePasse);
//		}
		System.out.println("*********************************" + listeCategoriesAffectees.size());
		for (AffectationCategorieMasseur aff : listeCategoriesAffectees) {
			System.out.println(aff.getCategorieMasseur().getLibelleCategorieMasseur());
		}
//		masseur.setAffectactionsCategorieMasseur(listeCategoriesAffectees);
		proxyMasseurBusiness.ModifierMasseur(masseur);
		return "/MesMasseurs.xhtml?faces-redirect=true";
	}
	
	public void supprimerCategorie() {

		selectedCategorieAffectee.setDateSortieCategorieMasseur(dateDuJour);
		proxyOutilBusiness.updateAffectationCategorieMasseur(selectedCategorieAffectee);
	}
	
//	public List<CategorieMasseur> ObtenirListeCategoDispoByMasseur (){
//		listeCategoriesDisponiblesParMasseur = null;
//		listeCategoriesDisponiblesParMasseur = proxyOutilBusiness.getListCatMassDispoByIdMass(idMasseurRecupere);
//		return listeCategoriesDisponiblesParMasseur;
//	}
	
	public void ajouterCategorie() {
		System.out.println("----------------------------------" + listeCategoriesAffectees.size() + " et le masseur est " + masseur.getPrenomMasseur());

		AffectationCategorieMasseur newAffect = null;
		newAffect = new AffectationCategorieMasseur(null, dateDuJour, null, masseur, proxyOutilBusiness.getCategoMasseurById(idSelectedCategorie));
//		listeCategoriesAffectees.add(newAffect);
//		masseur.getAffectactionsCategorieMasseur().add(newAffect);
//		System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!" + masseur.getAffectactionsCategorieMasseur().size());
		proxyOutilBusiness.PersisterAffectationCategorie(newAffect);
	}
	
	public void supprimerEvolution() {
		selectedHistoEvoPro.setDateFinEvolutionProfessionnelleMasseur(dateDuJour);
		proxyOutilBusiness.updateHistoEvoPro(selectedHistoEvoPro);
	}
	
	public void ajouterEvolution() {
		HistoriqueEvolutionProfessionnelleMasseur histevo = null;
		histevo = new HistoriqueEvolutionProfessionnelleMasseur(null, dateDuJour, null, masseur, proxyOutilBusiness.getEvoProMasseurById(idSelectedEvoPro));
		proxyOutilBusiness.PersisterHistoEvolutionPro(histevo);
	}
	

	/**
	 * @return the proxyMasseurBusiness
	 */
	public MasseurIBusiness getProxyMasseurBusiness() {
		return proxyMasseurBusiness;
	}

	/**
	 * @param proxyMasseurBusiness the proxyMasseurBusiness to set
	 */
	public void setProxyMasseurBusiness(MasseurIBusiness proxyMasseurBusiness) {
		this.proxyMasseurBusiness = proxyMasseurBusiness;
	}

	/**
	 * @return the masseur
	 */
	public Masseur getMasseur() {
		return masseur;
	}

	/**
	 * @param masseur the masseur to set
	 */
	public void setMasseur(Masseur masseur) {
		this.masseur = masseur;
	}

	/**
	 * @return the idMasseurRecupere
	 */
	public int getIdMasseurRecupere() {
		return idMasseurRecupere;
	}

	/**
	 * @param idMasseurRecupere the idMasseurRecupere to set
	 */
	public void setIdMasseurRecupere(int idMasseurRecupere) {
		this.idMasseurRecupere = idMasseurRecupere;
	}

	/**
	 * @return the proxyOutilBusiness
	 */
	public OutilsIBusiness getProxyOutilBusiness() {
		return proxyOutilBusiness;
	}

	/**
	 * @param proxyOutilBusiness the proxyOutilBusiness to set
	 */
	public void setProxyOutilBusiness(OutilsIBusiness proxyOutilBusiness) {
		this.proxyOutilBusiness = proxyOutilBusiness;
	}

	/**
	 * @return the codesPostaux
	 */
	public List<String> getCodesPostaux() {
		return codesPostaux;
	}

	/**
	 * @param codesPostaux the codesPostaux to set
	 */
	public void setCodesPostaux(List<String> codesPostaux) {
		this.codesPostaux = codesPostaux;
	}

	/**
	 * @return the villes
	 */
	public List<String> getVilles() {
		return villes;
	}

	/**
	 * @param villes the villes to set
	 */
	public void setVilles(List<String> villes) {
		this.villes = villes;
	}

	/**
	 * @return the selectedCP
	 */
	public String getSelectedCP() {
		return selectedCP;
	}

	/**
	 * @param selectedCP the selectedCP to set
	 */
	public void setSelectedCP(String selectedCP) {
		this.selectedCP = selectedCP;
	}

	/**
	 * @return the selectedVille
	 */
	public String getSelectedVille() {
		return selectedVille;
	}

	/**
	 * @param selectedVille the selectedVille to set
	 */
	public void setSelectedVille(String selectedVille) {
		this.selectedVille = selectedVille;
	}

	/**
	 * @return the listeSexe
	 */
	public List<Sexe> getListeSexe() {
		return listeSexe;
	}

	/**
	 * @param listeSexe the listeSexe to set
	 */
	public void setListeSexe(List<Sexe> listeSexe) {
		this.listeSexe = listeSexe;
	}

	public int getIdSexeRecup() {
		return idSexeRecup;
	}

	public void setIdSexeRecup(int idSexeRecup) {
		this.idSexeRecup = idSexeRecup;
	}

	public List<AffectationCategorieMasseur> getListeCategoriesAffectees() {
		return listeCategoriesAffectees;
	}

	public void setListeCategoriesAffectees(List<AffectationCategorieMasseur> listeCategoriesAffectees) {
		this.listeCategoriesAffectees = listeCategoriesAffectees;
	}

	public Date getDateDuJour() {
		return dateDuJour;
	}

	public void setDateDuJour(Date dateDuJour) {
		this.dateDuJour = dateDuJour;
	}

	/**
	 * @return the selectedCategorieAffectee
	 */
	public AffectationCategorieMasseur getSelectedCategorieAffectee() {
		return selectedCategorieAffectee;
	}

	/**
	 * @param selectedCategorieAffectee the selectedCategorieAffectee to set
	 */
	public void setSelectedCategorieAffectee(AffectationCategorieMasseur selectedCategorieAffectee) {
		this.selectedCategorieAffectee = selectedCategorieAffectee;
	}

	public List<CategorieMasseur> getListeCategoriesDisponiblesParMasseur() {
		return listeCategoriesDisponiblesParMasseur;
	}

	public void setListeCategoriesDisponiblesParMasseur(List<CategorieMasseur> listeCategoriesDisponiblesParMasseur) {
		this.listeCategoriesDisponiblesParMasseur = listeCategoriesDisponiblesParMasseur;
	}

	public Integer getIdSelectedCategorie() {
		return idSelectedCategorie;
	}

	

	/**
	 * @param idSelectedCategorie the idSelectedCategorie to set
	 */
	public void setIdSelectedCategorie(Integer idSelectedCategorie) {
		this.idSelectedCategorie = idSelectedCategorie;
	}

	public EvolutionProfessionnelle getSelectedEvoPro() {
		return selectedEvoPro;
	}

	public void setSelectedEvoPro(EvolutionProfessionnelle selectedEvoPro) {
		this.selectedEvoPro = selectedEvoPro;
	}

	public HistoriqueEvolutionProfessionnelleMasseur getSelectedHistoEvoPro() {
		return selectedHistoEvoPro;
	}

	public void setSelectedHistoEvoPro(HistoriqueEvolutionProfessionnelleMasseur selectedHistoEvoPro) {
		this.selectedHistoEvoPro = selectedHistoEvoPro;
	}

	public List<HistoriqueEvolutionProfessionnelleMasseur> getListeHistoEvoPro() {
		return listeHistoEvoPro;
	}

	public void setListeHistoEvoPro(List<HistoriqueEvolutionProfessionnelleMasseur> listeHistoEvoPro) {
		this.listeHistoEvoPro = listeHistoEvoPro;
	}

	public Integer getIdSelectedEvoPro() {
		return idSelectedEvoPro;
	}

	public void setIdSelectedEvoPro(Integer idSelectedEvoPro) {
		this.idSelectedEvoPro = idSelectedEvoPro;
	}

	public List<EvolutionProfessionnelle> getListeEvoPro() {
		return listeEvoPro;
	}

	public void setListeEvoPro(List<EvolutionProfessionnelle> listeEvoPro) {
		this.listeEvoPro = listeEvoPro;
	}

	/**
	 * @return the masseurConnected
	 */
	public Masseur getMasseurConnected() {
		return masseurConnected;
	}

	/**
	 * @param masseurConnected the masseurConnected to set
	 */
	public void setMasseurConnected(Masseur masseurConnected) {
		this.masseurConnected = masseurConnected;
	}

//	public String getotDePasseActuel() {
//		return motDePasseActuel;
//	}
//
//	public void setMotDePasseActuel(String motDePasseActuel) {
//		this.motDePasseActuel = motDePasseActuel;
//	}
//
//	/**
//	 * @return the nouveauMotDePasse
//	 */
//	public String getNouveauMotDePasse() {
//		return nouveauMotDePasse;
//	}
//
//	/**
//	 * @param nouveauMotDePasse the nouveauMotDePasse to set
//	 */
//	public void setNouveauMotDePasse(String nouveauMotDePasse) {
//		this.nouveauMotDePasse = nouveauMotDePasse;
//	}
//
//	/**
//	 * @return the nouveauMotDePasseConfirm
//	 */
//	public String getNouveauMotDePasseConfirm() {
//		return nouveauMotDePasseConfirm;
//	}
//
//	/**
//	 * @param nouveauMotDePasseConfirm the nouveauMotDePasseConfirm to set
//	 */
//	public void setNouveauMotDePasseConfirm(String nouveauMotDePasseConfirm) {
//		this.nouveauMotDePasseConfirm = nouveauMotDePasseConfirm;
//	}
//
//	/**
//	 * @return the motDePasseActuel
//	 */
//	public String getMotDePasseActuel() {
//		return motDePasseActuel;
//	}
//
//	public boolean isOk() {
//		return Ok;
//	}
//
//	public void setOk(boolean ok) {
//		Ok = ok;
//	}
//
//	/**
//	 * @return the identifiantFourni
//	 */
//	public String getIdentifiantFourni() {
//		return identifiantFourni;
//	}
//
//	/**
//	 * @param identifiantFourni the identifiantFourni to set
//	 */
//	public void setIdentifiantFourni(String identifiantFourni) {
//		this.identifiantFourni = identifiantFourni;
//	}
	

}
