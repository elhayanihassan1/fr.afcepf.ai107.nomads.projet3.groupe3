package fr.afcepf.ai107.nomads.controller.retours;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import fr.afcepf.ai107.nomads.entities.InscriptionMasseurs;
import fr.afcepf.ai107.nomads.entities.InterventionReelle;
import fr.afcepf.ai107.nomads.entities.LocationMateriel;
import fr.afcepf.ai107.nomads.entities.MotifAnnulationPrestation;
import fr.afcepf.ai107.nomads.entities.Partenaire;
import fr.afcepf.ai107.nomads.entities.Prestation;
import fr.afcepf.ai107.nomads.entities.RetourExperienceMasseur;
import fr.afcepf.ai107.nomads.entities.RetourExperiencePartenaire;
import fr.afcepf.ai107.nomads.entities.TypePaiement;
import fr.afcepf.ai107.nomads.entities.VilleCp;
import fr.afcepf.ai107.nomads.retours.ibusiness.RetoursMasseursIBusiness;

@ManagedBean(name = "mbRetoursMass")
@RequestScoped
public class RetoursMasseursManagedBean implements Serializable{

		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		@EJB
		private RetoursMasseursIBusiness proxyRetoursMasseurs;
		
		private List<RetourExperienceMasseur> retoursMasseurs = new ArrayList<RetourExperienceMasseur>();
		private RetourExperienceMasseur selectedRetour;
		private Integer idRetourExperienceMasseur;
		private String commentaireRetourExperienceMasseur;
		private Date dateRetourExperienceMasseur;
		private double chiffreAffaire;
		private Integer nombreClientsMasses;
		private Integer noteAccueilEvaluationMasseur;
		private Integer noteAmbianceEvaluationMasseur;
		private Integer noteClienteleEvaluationMasseur;
		
		private List<Prestation> prestations = new ArrayList<Prestation>();
		private Integer idPrestation; 
		private String libellePrestation;
		private Double coutDevisPrestation;
		private Date dateEmissionPrestation;
		private Date dateValidationPrestation;
		private String heureDebutPrestation;
		private String heureFinPrestation;
		private Integer nombreMasseursPrestation;
		private Integer nombreChaisesErgonomiquesPrestation;
		private Double montantRemunerationPrestation;
		private Date dateAnnulationPrestation;
		private Date datePaiementPrestation;
		private String adresseInterventionPrestation;
		private String specificitesPrestation;
		private Integer coutMads;
		private Partenaire partenaire; 
		private VilleCp villeCp;
		private MotifAnnulationPrestation motifAnnulationPrestation;
		private TypePaiement typePaiement; 
		private List<LocationMateriel> locationMateriel;
		
		private List<InterventionReelle> interventionReelle = new ArrayList<InterventionReelle>();
		private Integer idInterventionReelle;
		private Date datePrestation;
		private Prestation prestation;
		private List<InscriptionMasseurs> inscriptionMasseur;
		private Set<RetourExperienceMasseur> retoursExperienceMasseurs;
		private List<RetourExperiencePartenaire> retourExperiencePartenaire;
		
		private List<Partenaire> partenaires = new ArrayList<Partenaire>();
		private int idPartenaire;
		private String nomPartenaire;

		
		@PostConstruct
		public void init() {
			retoursMasseurs = proxyRetoursMasseurs.getCurrentMonthRetourMasseur();
		}


		/**
		 * @return the retoursMasseurs
		 */
		public List<RetourExperienceMasseur> getRetoursMasseurs() {
			return retoursMasseurs;
		}


		/**
		 * @param retoursMasseurs the retoursMasseurs to set
		 */
		public void setRetoursMasseurs(List<RetourExperienceMasseur> retoursMasseurs) {
			this.retoursMasseurs = retoursMasseurs;
		}


		/**
		 * @return the selectedRetour
		 */
		public RetourExperienceMasseur getSelectedRetour() {
			return selectedRetour;
		}


		/**
		 * @param selectedRetour the selectedRetour to set
		 */
		public void setSelectedRetour(RetourExperienceMasseur selectedRetour) {
			this.selectedRetour = selectedRetour;
		}


		/**
		 * @return the idRetourExperienceMasseur
		 */
		public Integer getIdRetourExperienceMasseur() {
			return idRetourExperienceMasseur;
		}


		/**
		 * @param idRetourExperienceMasseur the idRetourExperienceMasseur to set
		 */
		public void setIdRetourExperienceMasseur(Integer idRetourExperienceMasseur) {
			this.idRetourExperienceMasseur = idRetourExperienceMasseur;
		}


		/**
		 * @return the commentaireRetourExperienceMasseur
		 */
		public String getCommentaireRetourExperienceMasseur() {
			return commentaireRetourExperienceMasseur;
		}


		/**
		 * @param commentaireRetourExperienceMasseur the commentaireRetourExperienceMasseur to set
		 */
		public void setCommentaireRetourExperienceMasseur(String commentaireRetourExperienceMasseur) {
			this.commentaireRetourExperienceMasseur = commentaireRetourExperienceMasseur;
		}


		/**
		 * @return the dateRetourExperienceMasseur
		 */
		public Date getDateRetourExperienceMasseur() {
			return dateRetourExperienceMasseur;
		}


		/**
		 * @param dateRetourExperienceMasseur the dateRetourExperienceMasseur to set
		 */
		public void setDateRetourExperienceMasseur(Date dateRetourExperienceMasseur) {
			this.dateRetourExperienceMasseur = dateRetourExperienceMasseur;
		}


		/**
		 * @return the chiffreAffaire
		 */
		public double getChiffreAffaire() {
			return chiffreAffaire;
		}


		/**
		 * @param chiffreAffaire the chiffreAffaire to set
		 */
		public void setChiffreAffaire(double chiffreAffaire) {
			this.chiffreAffaire = chiffreAffaire;
		}


		/**
		 * @return the nombreClientsMasses
		 */
		public Integer getNombreClientsMasses() {
			return nombreClientsMasses;
		}


		/**
		 * @param nombreClientsMasses the nombreClientsMasses to set
		 */
		public void setNombreClientsMasses(Integer nombreClientsMasses) {
			this.nombreClientsMasses = nombreClientsMasses;
		}


		/**
		 * @return the noteAccueilEvaluationMasseur
		 */
		public Integer getNoteAccueilEvaluationMasseur() {
			return noteAccueilEvaluationMasseur;
		}


		/**
		 * @param noteAccueilEvaluationMasseur the noteAccueilEvaluationMasseur to set
		 */
		public void setNoteAccueilEvaluationMasseur(Integer noteAccueilEvaluationMasseur) {
			this.noteAccueilEvaluationMasseur = noteAccueilEvaluationMasseur;
		}


		/**
		 * @return the noteAmbianceEvaluationMasseur
		 */
		public Integer getNoteAmbianceEvaluationMasseur() {
			return noteAmbianceEvaluationMasseur;
		}


		/**
		 * @param noteAmbianceEvaluationMasseur the noteAmbianceEvaluationMasseur to set
		 */
		public void setNoteAmbianceEvaluationMasseur(Integer noteAmbianceEvaluationMasseur) {
			this.noteAmbianceEvaluationMasseur = noteAmbianceEvaluationMasseur;
		}


		/**
		 * @return the noteClienteleEvaluationMasseur
		 */
		public Integer getNoteClienteleEvaluationMasseur() {
			return noteClienteleEvaluationMasseur;
		}


		/**
		 * @param noteClienteleEvaluationMasseur the noteClienteleEvaluationMasseur to set
		 */
		public void setNoteClienteleEvaluationMasseur(Integer noteClienteleEvaluationMasseur) {
			this.noteClienteleEvaluationMasseur = noteClienteleEvaluationMasseur;
		}


		/**
		 * @return the prestations
		 */
		public List<Prestation> getPrestations() {
			return prestations;
		}


		/**
		 * @param prestations the prestations to set
		 */
		public void setPrestations(List<Prestation> prestations) {
			this.prestations = prestations;
		}


		/**
		 * @return the idPrestation
		 */
		public Integer getIdPrestation() {
			return idPrestation;
		}


		/**
		 * @param idPrestation the idPrestation to set
		 */
		public void setIdPrestation(Integer idPrestation) {
			this.idPrestation = idPrestation;
		}


		/**
		 * @return the libellePrestation
		 */
		public String getLibellePrestation() {
			return libellePrestation;
		}


		/**
		 * @param libellePrestation the libellePrestation to set
		 */
		public void setLibellePrestation(String libellePrestation) {
			this.libellePrestation = libellePrestation;
		}


		/**
		 * @return the coutDevisPrestation
		 */
		public Double getCoutDevisPrestation() {
			return coutDevisPrestation;
		}


		/**
		 * @param coutDevisPrestation the coutDevisPrestation to set
		 */
		public void setCoutDevisPrestation(Double coutDevisPrestation) {
			this.coutDevisPrestation = coutDevisPrestation;
		}


		/**
		 * @return the dateEmissionPrestation
		 */
		public Date getDateEmissionPrestation() {
			return dateEmissionPrestation;
		}


		/**
		 * @param dateEmissionPrestation the dateEmissionPrestation to set
		 */
		public void setDateEmissionPrestation(Date dateEmissionPrestation) {
			this.dateEmissionPrestation = dateEmissionPrestation;
		}


		/**
		 * @return the dateValidationPrestation
		 */
		public Date getDateValidationPrestation() {
			return dateValidationPrestation;
		}


		/**
		 * @param dateValidationPrestation the dateValidationPrestation to set
		 */
		public void setDateValidationPrestation(Date dateValidationPrestation) {
			this.dateValidationPrestation = dateValidationPrestation;
		}


		/**
		 * @return the heureDebutPrestation
		 */
		public String getHeureDebutPrestation() {
			return heureDebutPrestation;
		}


		/**
		 * @param heureDebutPrestation the heureDebutPrestation to set
		 */
		public void setHeureDebutPrestation(String heureDebutPrestation) {
			this.heureDebutPrestation = heureDebutPrestation;
		}


		/**
		 * @return the heureFinPrestation
		 */
		public String getHeureFinPrestation() {
			return heureFinPrestation;
		}


		/**
		 * @param heureFinPrestation the heureFinPrestation to set
		 */
		public void setHeureFinPrestation(String heureFinPrestation) {
			this.heureFinPrestation = heureFinPrestation;
		}


		/**
		 * @return the nombreMasseursPrestation
		 */
		public Integer getNombreMasseursPrestation() {
			return nombreMasseursPrestation;
		}


		/**
		 * @param nombreMasseursPrestation the nombreMasseursPrestation to set
		 */
		public void setNombreMasseursPrestation(Integer nombreMasseursPrestation) {
			this.nombreMasseursPrestation = nombreMasseursPrestation;
		}


		/**
		 * @return the nombreChaisesErgonomiquesPrestation
		 */
		public Integer getNombreChaisesErgonomiquesPrestation() {
			return nombreChaisesErgonomiquesPrestation;
		}


		/**
		 * @param nombreChaisesErgonomiquesPrestation the nombreChaisesErgonomiquesPrestation to set
		 */
		public void setNombreChaisesErgonomiquesPrestation(Integer nombreChaisesErgonomiquesPrestation) {
			this.nombreChaisesErgonomiquesPrestation = nombreChaisesErgonomiquesPrestation;
		}


		/**
		 * @return the montantRemunerationPrestation
		 */
		public Double getMontantRemunerationPrestation() {
			return montantRemunerationPrestation;
		}


		/**
		 * @param montantRemunerationPrestation the montantRemunerationPrestation to set
		 */
		public void setMontantRemunerationPrestation(Double montantRemunerationPrestation) {
			this.montantRemunerationPrestation = montantRemunerationPrestation;
		}


		/**
		 * @return the dateAnnulationPrestation
		 */
		public Date getDateAnnulationPrestation() {
			return dateAnnulationPrestation;
		}


		/**
		 * @param dateAnnulationPrestation the dateAnnulationPrestation to set
		 */
		public void setDateAnnulationPrestation(Date dateAnnulationPrestation) {
			this.dateAnnulationPrestation = dateAnnulationPrestation;
		}


		/**
		 * @return the datePaiementPrestation
		 */
		public Date getDatePaiementPrestation() {
			return datePaiementPrestation;
		}


		/**
		 * @param datePaiementPrestation the datePaiementPrestation to set
		 */
		public void setDatePaiementPrestation(Date datePaiementPrestation) {
			this.datePaiementPrestation = datePaiementPrestation;
		}


		/**
		 * @return the adresseInterventionPrestation
		 */
		public String getAdresseInterventionPrestation() {
			return adresseInterventionPrestation;
		}


		/**
		 * @param adresseInterventionPrestation the adresseInterventionPrestation to set
		 */
		public void setAdresseInterventionPrestation(String adresseInterventionPrestation) {
			this.adresseInterventionPrestation = adresseInterventionPrestation;
		}


		/**
		 * @return the specificitesPrestation
		 */
		public String getSpecificitesPrestation() {
			return specificitesPrestation;
		}


		/**
		 * @param specificitesPrestation the specificitesPrestation to set
		 */
		public void setSpecificitesPrestation(String specificitesPrestation) {
			this.specificitesPrestation = specificitesPrestation;
		}


		/**
		 * @return the coutMads
		 */
		public Integer getCoutMads() {
			return coutMads;
		}


		/**
		 * @param coutMads the coutMads to set
		 */
		public void setCoutMads(Integer coutMads) {
			this.coutMads = coutMads;
		}


		/**
		 * @return the partenaire
		 */
		public Partenaire getPartenaire() {
			return partenaire;
		}


		/**
		 * @param partenaire the partenaire to set
		 */
		public void setPartenaire(Partenaire partenaire) {
			this.partenaire = partenaire;
		}


		/**
		 * @return the villeCp
		 */
		public VilleCp getVilleCp() {
			return villeCp;
		}


		/**
		 * @param villeCp the villeCp to set
		 */
		public void setVilleCp(VilleCp villeCp) {
			this.villeCp = villeCp;
		}


		/**
		 * @return the motifAnnulationPrestation
		 */
		public MotifAnnulationPrestation getMotifAnnulationPrestation() {
			return motifAnnulationPrestation;
		}


		/**
		 * @param motifAnnulationPrestation the motifAnnulationPrestation to set
		 */
		public void setMotifAnnulationPrestation(MotifAnnulationPrestation motifAnnulationPrestation) {
			this.motifAnnulationPrestation = motifAnnulationPrestation;
		}


		/**
		 * @return the typePaiement
		 */
		public TypePaiement getTypePaiement() {
			return typePaiement;
		}


		/**
		 * @param typePaiement the typePaiement to set
		 */
		public void setTypePaiement(TypePaiement typePaiement) {
			this.typePaiement = typePaiement;
		}


		/**
		 * @return the locationMateriel
		 */
		public List<LocationMateriel> getLocationMateriel() {
			return locationMateriel;
		}


		/**
		 * @param locationMateriel the locationMateriel to set
		 */
		public void setLocationMateriel(List<LocationMateriel> locationMateriel) {
			this.locationMateriel = locationMateriel;
		}


		/**
		 * @return the interventionReelle
		 */
		public List<InterventionReelle> getInterventionReelle() {
			return interventionReelle;
		}


		/**
		 * @param interventionReelle the interventionReelle to set
		 */
		public void setInterventionReelle(List<InterventionReelle> interventionReelle) {
			this.interventionReelle = interventionReelle;
		}


		/**
		 * @return the idInterventionReelle
		 */
		public Integer getIdInterventionReelle() {
			return idInterventionReelle;
		}


		/**
		 * @param idInterventionReelle the idInterventionReelle to set
		 */
		public void setIdInterventionReelle(Integer idInterventionReelle) {
			this.idInterventionReelle = idInterventionReelle;
		}


		/**
		 * @return the datePrestation
		 */
		public Date getDatePrestation() {
			return datePrestation;
		}


		/**
		 * @param datePrestation the datePrestation to set
		 */
		public void setDatePrestation(Date datePrestation) {
			this.datePrestation = datePrestation;
		}


		/**
		 * @return the prestation
		 */
		public Prestation getPrestation() {
			return prestation;
		}


		/**
		 * @param prestation the prestation to set
		 */
		public void setPrestation(Prestation prestation) {
			this.prestation = prestation;
		}


		/**
		 * @return the inscriptionMasseur
		 */
		public List<InscriptionMasseurs> getInscriptionMasseur() {
			return inscriptionMasseur;
		}


		/**
		 * @param inscriptionMasseur the inscriptionMasseur to set
		 */
		public void setInscriptionMasseur(List<InscriptionMasseurs> inscriptionMasseur) {
			this.inscriptionMasseur = inscriptionMasseur;
		}


		/**
		 * @return the retoursExperienceMasseurs
		 */
		public Set<RetourExperienceMasseur> getRetoursExperienceMasseurs() {
			return retoursExperienceMasseurs;
		}


		/**
		 * @param retoursExperienceMasseurs the retoursExperienceMasseurs to set
		 */
		public void setRetoursExperienceMasseurs(Set<RetourExperienceMasseur> retoursExperienceMasseurs) {
			this.retoursExperienceMasseurs = retoursExperienceMasseurs;
		}


		/**
		 * @return the retourExperiencePartenaire
		 */
		public List<RetourExperiencePartenaire> getRetourExperiencePartenaire() {
			return retourExperiencePartenaire;
		}


		/**
		 * @param retourExperiencePartenaire the retourExperiencePartenaire to set
		 */
		public void setRetourExperiencePartenaire(List<RetourExperiencePartenaire> retourExperiencePartenaire) {
			this.retourExperiencePartenaire = retourExperiencePartenaire;
		}


		/**
		 * @return the partenaires
		 */
		public List<Partenaire> getPartenaires() {
			return partenaires;
		}


		/**
		 * @param partenaires the partenaires to set
		 */
		public void setPartenaires(List<Partenaire> partenaires) {
			this.partenaires = partenaires;
		}


		/**
		 * @return the idPartenaire
		 */
		public int getIdPartenaire() {
			return idPartenaire;
		}


		/**
		 * @param idPartenaire the idPartenaire to set
		 */
		public void setIdPartenaire(int idPartenaire) {
			this.idPartenaire = idPartenaire;
		}


		/**
		 * @return the nomPartenaire
		 */
		public String getNomPartenaire() {
			return nomPartenaire;
		}


		/**
		 * @param nomPartenaire the nomPartenaire to set
		 */
		public void setNomPartenaire(String nomPartenaire) {
			this.nomPartenaire = nomPartenaire;
		}


		
}
