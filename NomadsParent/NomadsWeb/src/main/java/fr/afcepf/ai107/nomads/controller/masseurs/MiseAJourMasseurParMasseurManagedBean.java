package fr.afcepf.ai107.nomads.controller.masseurs;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import fr.afcepf.ai107.nomads.entities.AffectationCategorieMasseur;
import fr.afcepf.ai107.nomads.entities.CategorieMasseur;
import fr.afcepf.ai107.nomads.entities.EvolutionProfessionnelle;
import fr.afcepf.ai107.nomads.entities.HistoriqueEvolutionProfessionnelleMasseur;
import fr.afcepf.ai107.nomads.entities.IndisponibiliteMasseur;
import fr.afcepf.ai107.nomads.entities.Masseur;
import fr.afcepf.ai107.nomads.entities.MotifIndisponibiliteMasseur;
import fr.afcepf.ai107.nomads.entities.Sexe;
import fr.afcepf.ai107.nomads.masseurs.ibusiness.MasseurIBusiness;
import fr.afcepf.ai107.nomads.outils.ibusiness.OutilsIBusiness;

//import org.omnifaces.cdi.ViewScoped;
//import org.omnifaces.util.selectitems.SelectItemsBuilder;

@ManagedBean (name = "mbMajMassMass")
@ViewScoped
public class MiseAJourMasseurParMasseurManagedBean implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@EJB
	private MasseurIBusiness proxyMasseurBusiness;
	
	@EJB
	private OutilsIBusiness proxyOutilBusiness;
	
	@ManagedProperty(value = "#{mbAccount.masseur}")
	private Masseur masseurConnected;
	
//	private Masseur masseur = new Masseur ();
//	private int idMasseurRecupere;
	
//			private String nomRecup;
//			private String prenomRecup;
//			private String adresseRecup;
	
	private List<String> codesPostaux = new ArrayList<String>();
	private List<String> villes = new ArrayList<String>();
	private String selectedCP;
	private String selectedVille;
	private List<Sexe> listeSexe = new ArrayList<Sexe>();
	private int idSexeRecup;
	private Date dateDuJour;
	
	private List<AffectationCategorieMasseur> listeCategoriesAffectees;
	private AffectationCategorieMasseur selectedCategorieAffectee;
	private List<CategorieMasseur> listeCategoriesDisponiblesParMasseur;
	private Integer idSelectedCategorie;
	
	private EvolutionProfessionnelle selectedEvoPro;
	private HistoriqueEvolutionProfessionnelleMasseur selectedHistoEvoPro;
	private List<HistoriqueEvolutionProfessionnelleMasseur> listeHistoEvoPro;
	private Integer idSelectedEvoPro;
	private List<EvolutionProfessionnelle> listeEvoPro;
	

// ===================GESTION INDISPONIBILITÉS====================================================	
	private List<IndisponibiliteMasseur> listeIndispoMasseurAVenir;
	private List<IndisponibiliteMasseur> listeIndispoMasseur;
	private List<IndisponibiliteMasseur> listeIndispoPasseesMasseur;

	private IndisponibiliteMasseur selectedIndispoMass;
	private Date dateDebutIndispo;
	private Date dateFinIndispo;
	private List<MotifIndisponibiliteMasseur> motifsIndispoMass;
														private String selectedMotifIndispoMass;
	private MotifIndisponibiliteMasseur selectedMotif;
	private Date currentDate=new Date();
	private LocalDateTime localDateTime = LocalDateTime.ofInstant(currentDate.toInstant(), ZoneId.systemDefault());
	private Date dateFromLocalDT = Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
	private IndisponibiliteMasseur nouvelleIndisponibiliteMasseur;
	private boolean historique = false;
	
	
//	private String motDePasseActuel;
//	private String identifiantFourni;
//	private String nouveauMotDePasse;
//	private String nouveauMotDePasseConfirm;
//	private boolean Ok = false;


	
	@PostConstruct
	public void init() {
//		System.out.println(FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("id"));
//		idMasseurRecupere=Integer.parseInt(FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("id"));
//		masseur = proxyMasseurBusiness.getInfosMasseurById(idMasseurRecupere);
		listeSexe = proxyMasseurBusiness.getSexes();
		codesPostaux = proxyOutilBusiness.listeCp();
		selectedCP = masseurConnected.getVilleCp().getLibelleCp();
		selectedVille = masseurConnected.getVilleCp().getLibelleVille();
		idSexeRecup = masseurConnected.getSexe().getIdSexe();
		
//		listeCategoriesAffectees = proxyOutilBusiness.getListAffectationCatMassByIdMass(masseurConnected.getId_masseur());
		dateDuJour = new Date();
		
		listeIndispoMasseurAVenir = proxyMasseurBusiness.getIndispoMasseurFutByIdMasseur(masseurConnected.getId_masseur());
		listeIndispoMasseur = listeIndispoMasseurAVenir;
		motifsIndispoMass = proxyMasseurBusiness.getAllMotifsIndispoMass();
		listeIndispoPasseesMasseur = proxyMasseurBusiness.getIndispoMasseurPasseesByIdMasseur(masseurConnected.getId_masseur());

//		//listeCategoriesDisponiblesParMasseur = proxyOutilBusiness.getListCatMassDispoByIdMass(idMasseurRecupere);
//		listeCategoriesDisponiblesParMasseur = proxyOutilBusiness.getAllCategories();
//		System.out.println("//////////////////////////////////// Taille de la liste de catégories à affectées: " + listeCategoriesDisponiblesParMasseur.size());
//		
//		listeHistoEvoPro = proxyOutilBusiness.historiqueEvoProMassByIdMasseur(idMasseurRecupere);
//		listeEvoPro = proxyOutilBusiness.getAll();
	}
	
	public void onCPChange() { 
		if (selectedCP !=null && !selectedCP.equals("")) {
			villes = proxyOutilBusiness.listeVilleByCp(selectedCP);
		}else {
			villes = new ArrayList<String>();
		}
	}
	
//	public void modifierMdp() {
//		boolean passwordIsOk = false;
//		boolean areTheSame = false;
//		passwordIsOk = proxyMasseurBusiness.identifiantsOk(identifiantFourni, motDePasseActuel);
//		if (nouveauMotDePasse.equals(nouveauMotDePasseConfirm)) {
//			areTheSame=true;
//		}
//		if (passwordIsOk && areTheSame) {
//			Ok = true;
//		}
//		System.out.println(Ok);
//	}
//	
	
	public String mettreAJour() {
////		if (Ok == true) {
////			masseur.setPasswordMasseur(nouveauMotDePasse);
////		}
//		System.out.println("*********************************" + listeCategoriesAffectees.size());
//		for (AffectationCategorieMasseur aff : listeCategoriesAffectees) {
//			System.out.println(aff.getCategorieMasseur().getLibelleCategorieMasseur());
//		}
////		masseur.setAffectactionsCategorieMasseur(listeCategoriesAffectees);
		proxyMasseurBusiness.ModifierMasseur(masseurConnected);
		return "/FicheMasseurMass.xhtml?faces-redirect=true";
	}
	

	
	public void AjouterIndisponibilite() {
		
		System.out.println("JE RENTRE DANS LA FONCTION D'AJOUT!!!");
		nouvelleIndisponibiliteMasseur = null;
		if (selectedMotif != null) {
			nouvelleIndisponibiliteMasseur.setMotifIndisponibiliteMasseur(selectedMotif);
		}
		System.out.println("LE MOTIF d4INDISPO EST " + nouvelleIndisponibiliteMasseur.getMotifIndisponibiliteMasseur().getLibelleMotifIndisponibiliteMasseur());
		nouvelleIndisponibiliteMasseur.setDateDebutIndisponibiliteMasseur(dateDebutIndispo);
		System.out.println("La date de début est : " + nouvelleIndisponibiliteMasseur.getDateDebutIndisponibiliteMasseur());
		if(dateFinIndispo != null) {
			nouvelleIndisponibiliteMasseur.setDateFinIndisponibiliteMasseur(dateFinIndispo);
		}
		nouvelleIndisponibiliteMasseur.setMasseur(masseurConnected);
		proxyMasseurBusiness.AjouterIndisponibilitéMasseur(nouvelleIndisponibiliteMasseur);
	}
	
	public void voirHistoIndispo() {
		System.out.println(historique);
		historique = true;
		listeIndispoMasseur = new ArrayList<IndisponibiliteMasseur>(listeIndispoPasseesMasseur);
		System.out.println(historique);
		historique = true;
	}
	
	public void voirIndispoEnCours() {
		historique = false;
		listeIndispoMasseur = new ArrayList<IndisponibiliteMasseur>(listeIndispoMasseurAVenir);
	}
//	public List<CategorieMasseur> ObtenirListeCategoDispoByMasseur (){
//		listeCategoriesDisponiblesParMasseur = null;
//		listeCategoriesDisponiblesParMasseur = proxyOutilBusiness.getListCatMassDispoByIdMass(idMasseurRecupere);
//		return listeCategoriesDisponiblesParMasseur;
//	}


	/**
	 * @return the proxyMasseurBusiness
	 */
	public MasseurIBusiness getProxyMasseurBusiness() {
		return proxyMasseurBusiness;
	}

	/**
	 * @param proxyMasseurBusiness the proxyMasseurBusiness to set
	 */
	public void setProxyMasseurBusiness(MasseurIBusiness proxyMasseurBusiness) {
		this.proxyMasseurBusiness = proxyMasseurBusiness;
	}

	/**
	 * @return the masseur
	 */
//	public Masseur getMasseur() {
//		return masseur;
//	}
//
//	/**
//	 * @param masseur the masseur to set
//	 */
//	public void setMasseur(Masseur masseur) {
//		this.masseur = masseur;
//	}
//
//	/**
//	 * @return the idMasseurRecupere
//	 */
//	public int getIdMasseurRecupere() {
//		return idMasseurRecupere;
//	}
//
//	/**
//	 * @param idMasseurRecupere the idMasseurRecupere to set
//	 */
//	public void setIdMasseurRecupere(int idMasseurRecupere) {
//		this.idMasseurRecupere = idMasseurRecupere;
//	}

	/**
	 * @return the proxyOutilBusiness
	 */
	public OutilsIBusiness getProxyOutilBusiness() {
		return proxyOutilBusiness;
	}

	/**
	 * @param proxyOutilBusiness the proxyOutilBusiness to set
	 */
	public void setProxyOutilBusiness(OutilsIBusiness proxyOutilBusiness) {
		this.proxyOutilBusiness = proxyOutilBusiness;
	}

	/**
	 * @return the codesPostaux
	 */
	public List<String> getCodesPostaux() {
		return codesPostaux;
	}

	/**
	 * @param codesPostaux the codesPostaux to set
	 */
	public void setCodesPostaux(List<String> codesPostaux) {
		this.codesPostaux = codesPostaux;
	}

	/**
	 * @return the villes
	 */
	public List<String> getVilles() {
		return villes;
	}

	/**
	 * @param villes the villes to set
	 */
	public void setVilles(List<String> villes) {
		this.villes = villes;
	}

	/**
	 * @return the selectedCP
	 */
	public String getSelectedCP() {
		return selectedCP;
	}

	/**
	 * @param selectedCP the selectedCP to set
	 */
	public void setSelectedCP(String selectedCP) {
		this.selectedCP = selectedCP;
	}

	/**
	 * @return the selectedVille
	 */
	public String getSelectedVille() {
		return selectedVille;
	}

	/**
	 * @param selectedVille the selectedVille to set
	 */
	public void setSelectedVille(String selectedVille) {
		this.selectedVille = selectedVille;
	}

	/**
	 * @return the listeSexe
	 */
	public List<Sexe> getListeSexe() {
		return listeSexe;
	}

	/**
	 * @param listeSexe the listeSexe to set
	 */
	public void setListeSexe(List<Sexe> listeSexe) {
		this.listeSexe = listeSexe;
	}

	public int getIdSexeRecup() {
		return idSexeRecup;
	}

	public void setIdSexeRecup(int idSexeRecup) {
		this.idSexeRecup = idSexeRecup;
	}

	public List<AffectationCategorieMasseur> getListeCategoriesAffectees() {
		return listeCategoriesAffectees;
	}

	public void setListeCategoriesAffectees(List<AffectationCategorieMasseur> listeCategoriesAffectees) {
		this.listeCategoriesAffectees = listeCategoriesAffectees;
	}

	public Date getDateDuJour() {
		return dateDuJour;
	}

	public void setDateDuJour(Date dateDuJour) {
		this.dateDuJour = dateDuJour;
	}

	/**
	 * @return the selectedCategorieAffectee
	 */
	public AffectationCategorieMasseur getSelectedCategorieAffectee() {
		return selectedCategorieAffectee;
	}

	/**
	 * @param selectedCategorieAffectee the selectedCategorieAffectee to set
	 */
	public void setSelectedCategorieAffectee(AffectationCategorieMasseur selectedCategorieAffectee) {
		this.selectedCategorieAffectee = selectedCategorieAffectee;
	}

	public List<CategorieMasseur> getListeCategoriesDisponiblesParMasseur() {
		return listeCategoriesDisponiblesParMasseur;
	}

	public void setListeCategoriesDisponiblesParMasseur(List<CategorieMasseur> listeCategoriesDisponiblesParMasseur) {
		this.listeCategoriesDisponiblesParMasseur = listeCategoriesDisponiblesParMasseur;
	}

	public Integer getIdSelectedCategorie() {
		return idSelectedCategorie;
	}

	

	/**
	 * @param idSelectedCategorie the idSelectedCategorie to set
	 */
	public void setIdSelectedCategorie(Integer idSelectedCategorie) {
		this.idSelectedCategorie = idSelectedCategorie;
	}

	public EvolutionProfessionnelle getSelectedEvoPro() {
		return selectedEvoPro;
	}

	public void setSelectedEvoPro(EvolutionProfessionnelle selectedEvoPro) {
		this.selectedEvoPro = selectedEvoPro;
	}

	public HistoriqueEvolutionProfessionnelleMasseur getSelectedHistoEvoPro() {
		return selectedHistoEvoPro;
	}

	public void setSelectedHistoEvoPro(HistoriqueEvolutionProfessionnelleMasseur selectedHistoEvoPro) {
		this.selectedHistoEvoPro = selectedHistoEvoPro;
	}

	public List<HistoriqueEvolutionProfessionnelleMasseur> getListeHistoEvoPro() {
		return listeHistoEvoPro;
	}

	public void setListeHistoEvoPro(List<HistoriqueEvolutionProfessionnelleMasseur> listeHistoEvoPro) {
		this.listeHistoEvoPro = listeHistoEvoPro;
	}

	public Integer getIdSelectedEvoPro() {
		return idSelectedEvoPro;
	}

	public void setIdSelectedEvoPro(Integer idSelectedEvoPro) {
		this.idSelectedEvoPro = idSelectedEvoPro;
	}

	public List<EvolutionProfessionnelle> getListeEvoPro() {
		return listeEvoPro;
	}

	public void setListeEvoPro(List<EvolutionProfessionnelle> listeEvoPro) {
		this.listeEvoPro = listeEvoPro;
	}

	/**
	 * @return the masseurConnected
	 */
	public Masseur getMasseurConnected() {
		return masseurConnected;
	}

	/**
	 * @param masseurConnected the masseurConnected to set
	 */
	public void setMasseurConnected(Masseur masseurConnected) {
		this.masseurConnected = masseurConnected;
	}

	/**
	 * @return the listeIndispoMasseur
	 */
	public List<IndisponibiliteMasseur> getListeIndispoMasseur() {
		return listeIndispoMasseur;
	}

	/**
	 * @param listeIndispoMasseur the listeIndispoMasseur to set
	 */
	public void setListeIndispoMasseur(List<IndisponibiliteMasseur> listeIndispoMasseur) {
		this.listeIndispoMasseur = listeIndispoMasseur;
	}

	/**
	 * @return the selectedIndispoMass
	 */
	public IndisponibiliteMasseur getSelectedIndispoMass() {
		return selectedIndispoMass;
	}

	/**
	 * @param selectedIndispoMass the selectedIndispoMass to set
	 */
	public void setSelectedIndispoMass(IndisponibiliteMasseur selectedIndispoMass) {
		this.selectedIndispoMass = selectedIndispoMass;
	}

	/**
	 * @return the dateDebutIndispo
	 */
	public Date getDateDebutIndispo() {
		return dateDebutIndispo;
	}

	/**
	 * @param dateDebutIndispo the dateDebutIndispo to set
	 */
	public void setDateDebutIndispo(Date dateDebutIndispo) {
		this.dateDebutIndispo = dateDebutIndispo;
	}

	/**
	 * @return the dateFinIndispo
	 */
	public Date getDateFinIndispo() {
		return dateFinIndispo;
	}

	/**
	 * @param dateFinIndispo the dateFinIndispo to set
	 */
	public void setDateFinIndispo(Date dateFinIndispo) {
		this.dateFinIndispo = dateFinIndispo;
	}

	/**
	 * @return the motifsIndispoMass
	 */
	public List<MotifIndisponibiliteMasseur> getMotifsIndispoMass() {
		return motifsIndispoMass;
	}

	/**
	 * @param motifsIndispoMass the motifsIndispoMass to set
	 */
	public void setMotifsIndispoMass(List<MotifIndisponibiliteMasseur> motifsIndispoMass) {
		this.motifsIndispoMass = motifsIndispoMass;
	}

	/**
	 * @return the selectedMotifIndispoMass
	 */
	public String getSelectedMotifIndispoMass() {
		return selectedMotifIndispoMass;
	}

	/**
	 * @param selectedMotifIndispoMass the selectedMotifIndispoMass to set
	 */
	public void setSelectedMotifIndispoMass(String selectedMotifIndispoMass) {
		this.selectedMotifIndispoMass = selectedMotifIndispoMass;
	}

	/**
	 * @return the currentDate
	 */
	public Date getCurrentDate() {
		return currentDate;
	}

	/**
	 * @param currentDate the currentDate to set
	 */
	public void setCurrentDate(Date currentDate) {
		this.currentDate = currentDate;
	}

	/**
	 * @return the localDateTime
	 */
	public LocalDateTime getLocalDateTime() {
		return localDateTime;
	}

	/**
	 * @param localDateTime the localDateTime to set
	 */
	public void setLocalDateTime(LocalDateTime localDateTime) {
		this.localDateTime = localDateTime;
	}

	public Date getDateFromLocalDT() {
		return dateFromLocalDT;
	}

	public void setDateFromLocalDT(Date dateFromLocalDT) {
		this.dateFromLocalDT = dateFromLocalDT;
	}

	public MotifIndisponibiliteMasseur getSelectedMotif() {
		return selectedMotif;
	}

	public void setSelectedMotif(MotifIndisponibiliteMasseur selectedMotif) {
		this.selectedMotif = selectedMotif;
	}

	public List<IndisponibiliteMasseur> getListeIndispoPasseesMasseur() {
		return listeIndispoPasseesMasseur;
	}

	public void setListeIndispoPasseesMasseur(List<IndisponibiliteMasseur> listeIndispoPasseesMasseur) {
		this.listeIndispoPasseesMasseur = listeIndispoPasseesMasseur;
	}

	public IndisponibiliteMasseur getNouvelleIndisponibiliteMasseur() {
		return nouvelleIndisponibiliteMasseur;
	}

	public void setNouvelleIndisponibiliteMasseur(IndisponibiliteMasseur nouvelleIndisponibiliteMasseur) {
		this.nouvelleIndisponibiliteMasseur = nouvelleIndisponibiliteMasseur;
	}

	public boolean isHistorique() {
		return historique;
	}

	public void setHistorique(boolean historique) {
		this.historique = historique;
	}

	public List<IndisponibiliteMasseur> getListeIndispoMasseurAVenir() {
		return listeIndispoMasseurAVenir;
	}

	public void setListeIndispoMasseurAVenir(List<IndisponibiliteMasseur> listeIndispoMasseurAVenir) {
		this.listeIndispoMasseurAVenir = listeIndispoMasseurAVenir;
	}

//	public String getotDePasseActuel() {
//		return motDePasseActuel;
//	}
//
//	public void setMotDePasseActuel(String motDePasseActuel) {
//		this.motDePasseActuel = motDePasseActuel;
//	}
//
//	/**
//	 * @return the nouveauMotDePasse
//	 */
//	public String getNouveauMotDePasse() {
//		return nouveauMotDePasse;
//	}
//
//	/**
//	 * @param nouveauMotDePasse the nouveauMotDePasse to set
//	 */
//	public void setNouveauMotDePasse(String nouveauMotDePasse) {
//		this.nouveauMotDePasse = nouveauMotDePasse;
//	}
//
//	/**
//	 * @return the nouveauMotDePasseConfirm
//	 */
//	public String getNouveauMotDePasseConfirm() {
//		return nouveauMotDePasseConfirm;
//	}
//
//	/**
//	 * @param nouveauMotDePasseConfirm the nouveauMotDePasseConfirm to set
//	 */
//	public void setNouveauMotDePasseConfirm(String nouveauMotDePasseConfirm) {
//		this.nouveauMotDePasseConfirm = nouveauMotDePasseConfirm;
//	}
//
//	/**
//	 * @return the motDePasseActuel
//	 */
//	public String getMotDePasseActuel() {
//		return motDePasseActuel;
//	}
//
//	public boolean isOk() {
//		return Ok;
//	}
//
//	public void setOk(boolean ok) {
//		Ok = ok;
//	}
//
//	/**
//	 * @return the identifiantFourni
//	 */
//	public String getIdentifiantFourni() {
//		return identifiantFourni;
//	}
//
//	/**
//	 * @param identifiantFourni the identifiantFourni to set
//	 */
//	public void setIdentifiantFourni(String identifiantFourni) {
//		this.identifiantFourni = identifiantFourni;
//	}
	

}
