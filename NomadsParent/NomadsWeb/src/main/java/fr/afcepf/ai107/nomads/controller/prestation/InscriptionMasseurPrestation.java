package fr.afcepf.ai107.nomads.controller.prestation;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import fr.afcepf.ai107.nomads.entities.InterventionReelle;
import fr.afcepf.ai107.nomads.entities.Masseur;
import fr.afcepf.ai107.nomads.prestation.ibusiness.PrestationIBusiness;

@ManagedBean(name = "mbInscriPrestaMass")
@ViewScoped

public class InscriptionMasseurPrestation implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@EJB
	private PrestationIBusiness proxyPrestationBusiness;
	
	@ManagedProperty(value = "#{mbAccount.masseur}")
	private Masseur masseurConnected;
	
	private List<InterventionReelle> listeInterventionReellesDispo ;
	private Integer totMasseurs = 0;
//	private Integer nbPlacesDispo;
	
	
	@PostConstruct
	public void init() {
		listeInterventionReellesDispo = proxyPrestationBusiness.getListeInterventionReelleDispoForMasseur(masseurConnected.getId_masseur());
		System.out.println(listeInterventionReellesDispo.size() + "-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-");
	}
	
	
		
	public String getDetailsPresta(Integer idPresta) {
		return "/FichePrestation?faces-redirect=true&id="+idPresta;
	}
	
	public Integer CalculMasseursPresents (Integer IdPrestation) {
		totMasseurs = proxyPrestationBusiness.getAllInscriMasseurByIdIntervention(IdPrestation).size();
		
		return totMasseurs;
	}
	
//	public Integer CalculPlacesDispos (Integer IdPrestation) {
//		
//		
//		
//		return nbPlacesDispo;
//	}
	
	
	
	
	public List<InterventionReelle> getListeInterventionReellesDispo() {
		return listeInterventionReellesDispo;
	}


	public void setListeInterventionReellesDispo(List<InterventionReelle> listeInterventionReellesDispo) {
		this.listeInterventionReellesDispo = listeInterventionReellesDispo;
	}

	/**
	 * @return the masseurConnected
	 */
	public Masseur getMasseurConnected() {
		return masseurConnected;
	}

	/**
	 * @param masseurConnected the masseurConnected to set
	 */
	public void setMasseurConnected(Masseur masseurConnected) {
		this.masseurConnected = masseurConnected;
	}

	/**
	 * @return the totMasseurs
	 */
	public Integer getTotMasseurs() {
		return totMasseurs;
	}

	/**
	 * @param totMasseurs the totMasseurs to set
	 */
	public void setTotMasseurs(Integer totMasseurs) {
		this.totMasseurs = totMasseurs;
	}

//	public Integer getNbPlacesDispo() {
//		return nbPlacesDispo;
//	}
//
//	public void setNbPlacesDispo(Integer nbPlacesDispo) {
//		this.nbPlacesDispo = nbPlacesDispo;
//	}
	
}
