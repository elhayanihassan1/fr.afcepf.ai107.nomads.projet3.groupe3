package fr.afcepf.ai107.nomads.controller.partenaire;

import java.io.Serializable;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.mail.Part;

import fr.afcepf.ai107.nomads.entities.Administrateur;
import fr.afcepf.ai107.nomads.entities.IndisponibilitePartenaire;
import fr.afcepf.ai107.nomads.entities.Partenaire;
import fr.afcepf.ai107.nomads.entities.TypePartenaire;
import fr.afcepf.ai107.nomads.outils.ibusiness.OutilsIBusiness;
import fr.afcepf.ai107.nomads.partenaire.ibusiness.PartenaireIBusiness;


@ManagedBean(name = "mbModifierPartenaire")
@ViewScoped
public class ModifierPartenaireManagedBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@EJB
	private PartenaireIBusiness  proxyPartenaireBusiness;

	@EJB
	private OutilsIBusiness proxyVilleCpBusiness;

	@ManagedProperty (value = "#{mbAccount.partenaire}" )
	private Partenaire partenaireConnected;


	@ManagedProperty (value = "#{mbAccount.admin}" )
	private Administrateur administrateur;
	
	private Partenaire partenaireRecuperer;	

	private Partenaire partenaire;
	private String success;
	private String nomPartenaire;
	private String adresseMailPartenaire;
	private String telephonePartenaire;
	private String adressePostalePartenaire;
	private Date datePremierEnregistrementPartenaire = new Date();
	private String nomContactPartenaire;
	private String numeroContactPartenaire;
	private String mailContactPartenaire;
	private String loginPartenaire,passwordPartenaire;
	private int TypePartenaireId;
	private List<TypePartenaire> listtypePartenaire ;
	private List<String> codesPostaux = new ArrayList<String>();
	private List<String> villes = new ArrayList<String>();
	private String selectedCP;
	private String selectedVille;
	private List<Partenaire> listPartenaire;
	 
	

	private String message;
	private LocalDateTime localDate = LocalDateTime.now();
	// --------------- Indisponibilités partenaires ---------------
	private IndisponibilitePartenaire indisponibiliteEnCours;
	private List<IndisponibilitePartenaire> historiqueIndispoPart = null;
	private boolean aEteModifie;
	private boolean aEteAjoute;
	private Date dateDebutIndispoModif;
	private Date dateFinIndispoModif;
	private Date dateDebutIndispoAjout;
	private Date dateFinIndispoAjout;
	private IndisponibilitePartenaire nouvelleIndispo= new IndisponibilitePartenaire();
	private IndisponibilitePartenaire indispoPart;
	private Date currentDate=new Date();
	private LocalDateTime localDateTime = LocalDateTime.ofInstant(currentDate.toInstant(), ZoneId.systemDefault());
	private Date dateFromLocalDT = Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
	private List<String> motifsIdispo=null;
	private String motifIndispoPart;
	private Partenaire selectedPartenaire = null;
	private boolean part=false;
	private boolean admin=false;
	
	
	public void onCPChange() { 

		if (selectedCP !=null && !selectedCP.equals("")) {
			villes = proxyVilleCpBusiness.listeVilleByCp(selectedCP);
		}else {
			villes = new ArrayList<String>();
		}
	}

	@PostConstruct
	public void init(){
		
		
		if(administrateur!=null) {
			int partenaireId = Integer.parseInt(FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("idPartSelected"));
			partenaireRecuperer = proxyPartenaireBusiness.getInfosPartenaireById(partenaireId);
			partenaire=partenaireRecuperer;
			admin=true;
		}
		if(partenaireConnected!=null) {
			partenaire=partenaireConnected;
			part=true;
		}
		listtypePartenaire=proxyPartenaireBusiness.getTypePartenaire();
		codesPostaux = proxyVilleCpBusiness.listeCp();
		motifsIdispo = proxyPartenaireBusiness.getAllMotifsIndispoPartenaire();
		nouvelleIndispo.setPartenaire(partenaire);
		try {
			historiqueIndispoPart=proxyPartenaireBusiness.getHistoriqueIndispoByPart(partenaire);
			indisponibiliteEnCours=proxyPartenaireBusiness.getIndispoEnCoursPart(partenaire);
		} catch (Exception e) {
			message=e.getMessage();
		}
		message="";

	}


	public void modifierIndisponibilitePartenaire() {
		message="";
		dateDebutIndispoModif=indisponibiliteEnCours.getDateDebutIndisponibilitePartenaire();

			indisponibiliteEnCours.setDateFinIndisponibilitePartenaire(dateFinIndispoModif);
			indisponibiliteEnCours.setPartenaire(partenaire);
		
		success="La modification a été effectuée";
		proxyPartenaireBusiness.updateIndispoPart(indisponibiliteEnCours);
	}

	public void ajouterIndisponibilitePartenaire() {
		
		if (motifIndispoPart!=null) {
			nouvelleIndispo.setMotifIndisponibilitePartenaire(proxyPartenaireBusiness.getMotifIndispoByLibelleMotifIndispo(motifIndispoPart));
		}
		if(dateDebutIndispoAjout != null) {
			nouvelleIndispo.setDateDebutIndisponibilitePartenaire(dateDebutIndispoAjout);
		}
		if(dateFinIndispoAjout != null) {
		nouvelleIndispo.setDateFinIndisponibilitePartenaire(dateFinIndispoAjout);
		}
				
		proxyPartenaireBusiness.ajouterIndispoPart(nouvelleIndispo);
		proxyPartenaireBusiness.modifierPartenaire(partenaire);
		
	}

	public String modifierPartenaire () {
		if(selectedVille != null) {
			partenaire.setVilleCp(proxyVilleCpBusiness.VilleCpParNomVille(selectedVille)); 
		}
		proxyPartenaireBusiness.modifierPartenaire(partenaire);
		success="La modification a bien été enregistrée !";
//		return "ModifierPartenaire.xhtml?faces-redirect=true&idPartSelected="+partenaire.getIdPartenaire();
		String retour = "#";
		if (partenaireConnected != null) {
			retour = "FichePartenaire.xhtml";
		}
		if (administrateur != null) {
			retour = "MesPartenaires.xhtml";
		}
		
		return retour;
	}
	public String retourListePartenaire() {
		return "MesPartenaires.xhtml?faces-redirect=true";
	}


	public String getLoginPartenaire() {
		return loginPartenaire;
	}
	public void setLoginPartenaire(String loginPartenaire) {
		this.loginPartenaire = loginPartenaire;
	}
	public String getPasswordPartenaire() {
		return passwordPartenaire;
	}
	public void setPasswordPartenaire(String passwordPartenaire) {
		this.passwordPartenaire = passwordPartenaire;
	}
	public String getSuccess() {
		return success;
	}
	public void setSuccess(String success) {
		this.success = success;
	}
	public int getTypePartenaireId() {
		return TypePartenaireId;
	}

	public void setTypePartenaireId(int typePartenaireId) {
		TypePartenaireId = typePartenaireId;
	}

	public Partenaire getPartenaire() {
		return partenaire;
	}

	public void setPartenaire(Partenaire partenaire) {
		this.partenaire = partenaire;
	}

	public List<TypePartenaire> getListtypePartenaire() {
		return listtypePartenaire;
	}

	public void setListtypePartenaire(List<TypePartenaire> listtypePartenaire) {
		this.listtypePartenaire = listtypePartenaire;
	}

	public List<Partenaire> getListPartenaire() {
		return listPartenaire;
	}
	public void setListPartenaire(List<Partenaire> listPartenaire) {
		this.listPartenaire = listPartenaire;
	}
	public List<String> getCodesPostaux() {
		return codesPostaux;
	}

	public void setCodesPostaux(List<String> codesPostaux) {
		this.codesPostaux = codesPostaux;
	}

	public List<String> getVilles() {
		return villes;
	}

	public void setVilles(List<String> villes) {
		this.villes = villes;
	}

	public String getSelectedCP() {
		return selectedCP;
	}

	public void setSelectedCP(String selectedCP) {
		this.selectedCP = selectedCP;
	}

	public String getSelectedVille() {
		return selectedVille;
	}

	public void setSelectedVille(String selectedVille) {
		this.selectedVille = selectedVille;
	}

	public String getNomPartenaire() {
		return nomPartenaire;
	}

	public void setNomPartenaire(String nomPartenaire) {
		this.nomPartenaire = nomPartenaire;
	}

	public String getAdresseMailPartenaire() {
		return adresseMailPartenaire;
	}

	public void setAdresseMailPartenaire(String adresseMailPartenaire) {
		this.adresseMailPartenaire = adresseMailPartenaire;
	}

	public String getTelephonePartenaire() {
		return telephonePartenaire;
	}

	public void setTelephonePartenaire(String telephonePartenaire) {
		this.telephonePartenaire = telephonePartenaire;
	}

	public String getAdressePostalePartenaire() {
		return adressePostalePartenaire;
	}

	public void setAdressePostalePartenaire(String adressePostalePartenaire) {
		this.adressePostalePartenaire = adressePostalePartenaire;
	}

	public Date getDatePremierEnregistrementPartenaire() {
		return datePremierEnregistrementPartenaire;
	}

	public void setDatePremierEnregistrementPartenaire(Date datePremierEnregistrementPartenaire) {
		this.datePremierEnregistrementPartenaire = datePremierEnregistrementPartenaire;
	}

	public String getNomContactPartenaire() {
		return nomContactPartenaire;
	}

	public void setNomContactPartenaire(String nomContactPartenaire) {
		this.nomContactPartenaire = nomContactPartenaire;
	}

	public String getNumeroContactPartenaire() {
		return numeroContactPartenaire;
	}

	public void setNumeroContactPartenaire(String numeroContactPartenaire) {
		this.numeroContactPartenaire = numeroContactPartenaire;
	}

	public String getMailContactPartenaire() {
		return mailContactPartenaire;
	}

	public void setMailContactPartenaire(String mailContactPartenaire) {
		this.mailContactPartenaire = mailContactPartenaire;
	}

	public List<IndisponibilitePartenaire> getHistoriqueIndispoPart() {
		return historiqueIndispoPart;
	}

	public void setHistoriqueIndispoPart(List<IndisponibilitePartenaire> historiqueIndispoPart) {
		this.historiqueIndispoPart = historiqueIndispoPart;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public boolean isaEteModifie() {
		return aEteModifie;
	}

	public void setaEteModifie(boolean aEteModifie) {
		this.aEteModifie = aEteModifie;
	}

	public boolean isaEteAjoute() {
		return aEteAjoute;
	}

	public void setaEteAjoute(boolean aEteAjoute) {
		this.aEteAjoute = aEteAjoute;
	}

	public Object getDateDebutIndispoModif() {
		return dateDebutIndispoModif;
	}

	public void setDateDebutIndispoModif(Date dateDebutIndispoModif) {
		this.dateDebutIndispoModif = dateDebutIndispoModif;
	}

	public Object getDateFinIndispoModif() {
		return dateFinIndispoModif;
	}

	public void setDateFinIndispoModif(Date dateFinIndispoModif) {
		this.dateFinIndispoModif = dateFinIndispoModif;
	}

	public IndisponibilitePartenaire getIndispoPart() {
		return indispoPart;
	}

	public void setIndispoPart(IndisponibilitePartenaire indispoPart) {
		this.indispoPart = indispoPart;
	}

	public Date getDateDebutIndispoAjout() {
		return dateDebutIndispoAjout;
	}

	public void setDateDebutIndispoAjout(Date dateDebutIndispoAjout) {
		this.dateDebutIndispoAjout = dateDebutIndispoAjout;
	}

	public IndisponibilitePartenaire getNouvelleIndispo() {
		return nouvelleIndispo;
	}

	public void setNouvelleIndispo(IndisponibilitePartenaire nouvelleIndispo) {
		this.nouvelleIndispo = nouvelleIndispo;
	}

	public Date getDateFinIndispoAjout() {
		return dateFinIndispoAjout;
	}

	public void setDateFinIndispoAjout(Date dateFinIndispoAjout) {
		this.dateFinIndispoAjout = dateFinIndispoAjout;
	}

	public LocalDateTime getLocalDate() {
		return localDate;
	}

	public void setLocalDate(LocalDateTime localDate) {
		this.localDate = localDate;
	}

	public Date getDateFromLocalDT() {
		return dateFromLocalDT;
	}

	public void setDateFromLocalDT(Date dateFromLocalDT) {
		this.dateFromLocalDT = dateFromLocalDT;
	}

	public IndisponibilitePartenaire getIndisponibiliteEnCours() {
		return indisponibiliteEnCours;
	}

	public void setIndisponibiliteEnCours(IndisponibilitePartenaire indisponibiliteEnCours) {
		this.indisponibiliteEnCours = indisponibiliteEnCours;
	}

	public Date getCurrentDate() {
		return currentDate;
	}

	public void setCurrentDate(Date currentDate) {
		this.currentDate = currentDate;
	}

	public LocalDateTime getLocalDateTime() {
		return localDateTime;
	}

	public void setLocalDateTime(LocalDateTime localDateTime) {
		this.localDateTime = localDateTime;
	}

	public List<String> getMotifsIdispo() {
		return motifsIdispo;
	}

	public void setMotifsIdispo(List<String> motifsIdispo) {
		this.motifsIdispo = motifsIdispo;
	}

	public String getMotifIndispoPart() {
		return motifIndispoPart;
	}

	public void setMotifIndispoPart(String motifIndispoPart) {
		this.motifIndispoPart = motifIndispoPart;
	}

	public Partenaire getSelectedPartenaire() {
		return selectedPartenaire;
	}

	public void setSelectedPartenaire(Partenaire selectedPartenaire) {
		this.selectedPartenaire = selectedPartenaire;
	}

	public Partenaire getPartenaireRecuperer() {
		return partenaireRecuperer;
	}

	public void setPartenaireRecuperer(Partenaire partenaireRecuperer) {
		this.partenaireRecuperer = partenaireRecuperer;
	}


	public Partenaire getPartenaireConnected() {
		return partenaireConnected;
	}

	public void setPartenaireConnected(Partenaire partenaireConnected) {
		this.partenaireConnected = partenaireConnected;
	}

	public Administrateur getAdministrateur() {
		return administrateur;
	}

	public void setAdministrateur(Administrateur administrateur) {
		this.administrateur = administrateur;
	}

	public boolean isPart() {
		return part;
	}

	public void setPart(boolean part) {
		this.part = part;
	}

	public boolean isAdmin() {
		return admin;
	}

	public void setAdmin(boolean admin) {
		this.admin = admin;
	}
	
}
