package fr.afcepf.ai107.nomads.controller.prestation;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import fr.afcepf.ai107.nomads.entities.Administrateur;
import fr.afcepf.ai107.nomads.entities.InscriptionMasseurs;
import fr.afcepf.ai107.nomads.entities.InterventionReelle;
import fr.afcepf.ai107.nomads.entities.Masseur;
import fr.afcepf.ai107.nomads.entities.Transaction;
import fr.afcepf.ai107.nomads.entities.TypePartenaire;
import fr.afcepf.ai107.nomads.masseurs.ibusiness.InscriptionMasseursIBusiness;
import fr.afcepf.ai107.nomads.masseurs.ibusiness.MasseurIBusiness;
import fr.afcepf.ai107.nomads.outils.ibusiness.OutilsIBusiness;
import fr.afcepf.ai107.nomads.partenaire.ibusiness.PartenaireIBusiness;
import fr.afcepf.ai107.nomads.prestation.ibusiness.PrestationIBusiness;

@ManagedBean(name = "mbFichePresta")
@ViewScoped
public class FichePrestaManagedBean implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@EJB
	private PrestationIBusiness proxyPrestaBusiness;
	
	@EJB
	private OutilsIBusiness proxyOutilBusiness;
	
	@EJB 
	private MasseurIBusiness proxyMasseurBusiness;
	
	@EJB
	private InscriptionMasseursIBusiness proxyInscriptionMass;
	
	@EJB
	private PartenaireIBusiness proxyPartenaire;
	
	@ManagedProperty (value="#{mbAccount.masseur}")
	private Masseur masseurConnected;
	
	@ManagedProperty (value="#{mbAccount.admin}")
	private Administrateur administrateurConnected;

	private InterventionReelle intervReelle = new InterventionReelle();
	
	private int idIntervention = Integer.parseInt(FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("id"));
	
	private int nombreDePlacesDispo;
	private int nombreTotalDePlaces;
	
	private List<Masseur> mesMasseurs = new ArrayList<Masseur>();
	private List<Object> obj;
//	private List<Masseur> masseurs = (List<Masseur>) (Object) obj;
	private Double soldeMads;
	private List<InterventionReelle>listeInterventionsReellesMasseur;
	private List<Transaction> listeTransactionsMasseur;
	private TypePartenaire idTypePartenairePresta;
	

	@PostConstruct
	public void init() {
		
		intervReelle = proxyPrestaBusiness.getInterventionById(idIntervention);
		System.out.println(intervReelle.getPrestation().getNombreMasseursPrestation());
		
		
		mesMasseurs = proxyInscriptionMass.getMasseursInscritsPourUneIntervention(idIntervention);
		nombreDePlacesDispo = intervReelle.getPrestation().getNombreMasseursPrestation() - mesMasseurs.size();
	
		if (masseurConnected != null) {
		listeInterventionsReellesMasseur = proxyPrestaBusiness.getAllInfosInterventionReelleByIdMasseur(masseurConnected.getId_masseur());
		listeTransactionsMasseur = proxyOutilBusiness.getListTransactionByIdMasseur(masseurConnected.getId_masseur()); 
		}
	//	idTypePartenairePresta = proxyPartenaire.getTypePartenaireById(idIntervention);
	}
	
	public double calculSoldeMads() {
		Double totalMads = 0.0;
		Double totalDepenses = 0.0;
		
		if(listeTransactionsMasseur != null) {
			for (Transaction transaction : listeTransactionsMasseur) {
				totalMads += transaction.getNombreDePack() * transaction.getTypePackMads().getValeurTypePackMads();
			}
		}else {
			totalMads = 0.0;
		}
		if(listeInterventionsReellesMasseur !=null) {
			for (InterventionReelle interventionReelle : listeInterventionsReellesMasseur) {
				Integer truc =interventionReelle.getPrestation().getCoutMads();
				if( truc != null) {
					totalDepenses += interventionReelle.getPrestation().getCoutMads();
				}
			}
		}else {
			totalDepenses = 0.0;
		}
		soldeMads = totalMads - totalDepenses;
		if (soldeMads == null) {
			soldeMads = 0.0;
		}
		return soldeMads;
	}
	
	
	public String inscription () {
		System.out.println("&&&&&&&&&&&&&&&&&&&&&&J'arrive dans la fonction");
		//Revérification du nombre de places disponibles
//		mesMasseurs = proxyInscriptionMass.getMasseursInscritsPourUneIntervention(intervReelle.getIdInterventionReelle());
//		nombreDePlacesDispo = intervReelle.getPrestation().getNombreMasseursPrestation() - mesMasseurs.size();
		System.out.println("Le masseur est " + masseurConnected.getPrenomMasseur());
		System.out.println("La *ùù%*ù*$$^*ù*^ù*$ de prestation est : " + intervReelle.getIdInterventionReelle());
		
		
		//Inscription
		if (nombreDePlacesDispo>0) {
			System.out.println("''''''''''''''''''''''''''''''''''''''''''''''' JE RENTRE DANS LE IF");
			InscriptionMasseurs nouvelleInscription = new InscriptionMasseurs(null, new Date(), null, masseurConnected, intervReelle, null);
			System.out.println("{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{J'ai créé la nouvelle Inscription");
			List<InscriptionMasseurs> inscriS = new ArrayList<InscriptionMasseurs>();
			System.out.println("##############JE CREE UNE LISTE A VIDE");
			System.out.println("Le masseur est " + masseurConnected.getPrenomMasseur());

			inscriS = masseurConnected.getInscriptionsMasseurs();
			System.out.println("-------------------J'AI MIS LA LISTE DES INSCRI DANS LA LISTE VIDE");
			System.out.println(nouvelleInscription.getDateInscriptionMasseur());
			System.out.println(nouvelleInscription.getMasseur().getNomMasseur());
			inscriS.add(nouvelleInscription);
			System.out.println("''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''");
			for (InscriptionMasseurs inscriptionMasseurs : inscriS) {
				System.out.println(inscriptionMasseurs.getDateInscriptionMasseur());
			}
			masseurConnected.setInscriptionsMasseurs(inscriS);
			System.out.println("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++J'ai mis la liste au masseur ET HJE VEUX PERSISTER");
			proxyMasseurBusiness.ModifierMasseur(masseurConnected);
		}
		return "MesPrestationsMasseur.xhtml?faces-redirect=true";
	}
	
	
	
	/**
	 * @return the idTypePartenairePresta
	 */
	public TypePartenaire getIdTypePartenairePresta() {
		return idTypePartenairePresta;
	}

	/**
	 * @param idTypePartenairePresta the idTypePartenairePresta to set
	 */
	public void setIdTypePartenairePresta(TypePartenaire idTypePartenairePresta) {
		this.idTypePartenairePresta = idTypePartenairePresta;
	}

	/**
	 * @return the mesMasseurs
	 */
	public List<Masseur> getMesMasseurs() {
		return mesMasseurs;
	}




	/**
	 * @param mesMasseurs the mesMasseurs to set
	 */
	public void setMesMasseurs(List<Masseur> mesMasseurs) {
		this.mesMasseurs = mesMasseurs;
	}




	/**
	 * @param nombreDePlacesDispo the nombreDePlacesDispo to set
	 */
	public void setNombreDePlacesDispo(int nombreDePlacesDispo) {
		this.nombreDePlacesDispo = nombreDePlacesDispo;
	}




	/**
	 * @return the nombreDePlacesDispo
	 */
	public int getNombreDePlacesDispo() {
		return nombreDePlacesDispo;
	}




	/**
	 * @return the nombreTotalDePlaces
	 */
	public int getNombreTotalDePlaces() {
		return nombreTotalDePlaces;
	}




	/**
	 * @param nombreTotalDePlaces the nombreTotalDePlaces to set
	 */
	public void setNombreTotalDePlaces(int nombreTotalDePlaces) {
		this.nombreTotalDePlaces = nombreTotalDePlaces;
	}




	/**
	 * @return the intervReelle
	 */
	public InterventionReelle getIntervReelle() {
		return intervReelle;
	}

	/**
	 * @param intervReelle the intervReelle to set
	 */
	public void setIntervReelle(InterventionReelle intervReelle) {
		this.intervReelle = intervReelle;
	}

	/**
	 * @return the idIntervention
	 */
	public int getIdIntervention() {
		return idIntervention;
	}

	/**
	 * @param idIntervention the idIntervention to set
	 */
	public void setIdIntervention(int idIntervention) {
		this.idIntervention = idIntervention;
	}



	/**
	 * @return the masseurConnected
	 */
	public Masseur getMasseurConnected() {
		return masseurConnected;
	}



	/**
	 * @param masseurConnected the masseurConnected to set
	 */
	public void setMasseurConnected(Masseur masseurConnected) {
		this.masseurConnected = masseurConnected;
	}

	/**
	 * @return the soldeMads
	 */
	public Double getSoldeMads() {
		return soldeMads;
	}

	/**
	 * @param soldeMads the soldeMads to set
	 */
	public void setSoldeMads(Double soldeMads) {
		this.soldeMads = soldeMads;
	}

	/**
	 * @return the listeInterventionsReellesMasseur
	 */
	public List<InterventionReelle> getListeInterventionsReellesMasseur() {
		return listeInterventionsReellesMasseur;
	}

	/**
	 * @param listeInterventionsReellesMasseur the listeInterventionsReellesMasseur to set
	 */
	public void setListeInterventionsReellesMasseur(List<InterventionReelle> listeInterventionsReellesMasseur) {
		this.listeInterventionsReellesMasseur = listeInterventionsReellesMasseur;
	}

	/**
	 * @return the listeTransactionsMasseur
	 */
	public List<Transaction> getListeTransactionsMasseur() {
		return listeTransactionsMasseur;
	}

	/**
	 * @param listeTransactionsMasseur the listeTransactionsMasseur to set
	 */
	public void setListeTransactionsMasseur(List<Transaction> listeTransactionsMasseur) {
		this.listeTransactionsMasseur = listeTransactionsMasseur;
	}

	/**
	 * @return the administrateurConnected
	 */
	public Administrateur getAdministrateurConnected() {
		return administrateurConnected;
	}

	/**
	 * @param administrateurConnected the administrateurConnected to set
	 */
	public void setAdministrateurConnected(Administrateur administrateurConnected) {
		this.administrateurConnected = administrateurConnected;
	}
	
	
	
	
}
