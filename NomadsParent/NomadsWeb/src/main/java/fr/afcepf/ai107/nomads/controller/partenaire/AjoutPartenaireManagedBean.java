package fr.afcepf.ai107.nomads.controller.partenaire;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import fr.afcepf.ai107.nomads.entities.ContratPartenaire;
import fr.afcepf.ai107.nomads.entities.Partenaire;
import fr.afcepf.ai107.nomads.entities.TypePartenaire;
import fr.afcepf.ai107.nomads.outils.ibusiness.OutilsIBusiness;
import fr.afcepf.ai107.nomads.partenaire.ibusiness.PartenaireIBusiness;


@ManagedBean(name = "mbAjoutPartenaire")
@ViewScoped
public class AjoutPartenaireManagedBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@EJB
	private PartenaireIBusiness  proxyPartenaireBusiness;
		
	@EJB
	private OutilsIBusiness proxyVilleCpBusiness;
	
	private Partenaire partenaire = new Partenaire();
	private String success;
	private String nomPartenaire;
	private String adresseMailPartenaire;
	private String telephonePartenaire;
	private String adressePostalePartenaire;
	private Date datePremierEnregistrementPartenaire = new Date();
	private String nomContactPartenaire;
	private String numeroContactPartenaire;
	private String mailContactPartenaire;
	private String loginPartenaire,passwordPartenaire;
	private int TypePartenaireId;
	private List<TypePartenaire> listtypePartenaire ;
	private List<String> codesPostaux = new ArrayList<String>();
	private List<String> villes = new ArrayList<String>();
	private String selectedCP;
	private String selectedVille;
	private List<Partenaire> listPartenaire;
	private Date dateDebutContrat = new Date();
	private int partenaireId ;
	private ContratPartenaire nouveauContrat = new ContratPartenaire();
	private Partenaire nouveauPartenaire = new Partenaire();
	
	
	public void onCPChange() { 
		if (selectedCP !=null && !selectedCP.equals("")) {
			villes = proxyVilleCpBusiness.listeVilleByCp(selectedCP);
		}else {
			villes = new ArrayList<String>();
		}
	}
	@PostConstruct
	public void init() {
        if( FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("id") != null ) {
        	
        	partenaireId = Integer.parseInt(FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("id"));
        	
		partenaire = proxyPartenaireBusiness.getInfosPartenaireById(partenaireId);}
		listtypePartenaire=proxyPartenaireBusiness.getTypePartenaire();
		codesPostaux = proxyVilleCpBusiness.listeCp();
		
     }

	public String ajouterPartenaire () {
		success="";
		partenaire.setNomPartenaire(nomPartenaire);
		partenaire.setAdresseMailPartenaire(adresseMailPartenaire);
		partenaire.setTelephonePartenaire(telephonePartenaire);
		partenaire.setAdressePostalePartenaire(adressePostalePartenaire);
		partenaire.setNomContactPartenaire(nomContactPartenaire);
		partenaire.setNumeroContactPartenaire(numeroContactPartenaire);
		partenaire.setMailContactPartenaire(mailContactPartenaire);
		partenaire.setDatePremierEnregistrementPartenaire(datePremierEnregistrementPartenaire);
	    partenaire.setTypePartenaire(proxyPartenaireBusiness.getTypePartenaireById(TypePartenaireId));
		if(selectedVille != null) {
			partenaire.setVilleCp(proxyVilleCpBusiness.VilleCpParNomVille(selectedVille)); 
			}
		partenaire.setLoginPartenaire(loginPartenaire);
		partenaire.setPasswordPartenaire(passwordPartenaire);
				
		nouveauPartenaire = proxyPartenaireBusiness.ajouterPartenaire(partenaire);
		nouveauContrat.setDateDebutContratPartenaire(dateDebutContrat);
		nouveauContrat.setPartenaire(nouveauPartenaire);
		proxyPartenaireBusiness.ajouterContratPartenaire(nouveauContrat);
		
		nomPartenaire="";
		adresseMailPartenaire="";
		telephonePartenaire="";
		adressePostalePartenaire="";
		nomContactPartenaire="";
		numeroContactPartenaire="";
		mailContactPartenaire="";
		selectedVille="";
		selectedCP="";
		TypePartenaireId=0;
		loginPartenaire="";
		passwordPartenaire="";
		return "/MesPartenaires.xhtml?faces-redirect=true";
	}

	 public void saveMessage() {
	        FacesContext context = FacesContext.getCurrentInstance();
	         
	        context.addMessage(null, new FacesMessage("Successful : ",  "Ajout effectué avec succés") );

	    }

	 public void demo () {
		 nomPartenaire="Au lutin des bords de Seine";
			adresseMailPartenaire="aulutindesbordsdeseine@gmail.com";
			telephonePartenaire="01 40 11 22 33";
			adressePostalePartenaire="18 quai Anatole France";
			nomContactPartenaire="Henricot";
			numeroContactPartenaire="06 11 22 33 44";
			mailContactPartenaire="vanessa.henricot@hotmail.fr";
			selectedCP="75007";
			selectedVille="";
			TypePartenaireId=1;
			loginPartenaire="lutin";
			passwordPartenaire="bds757";
	 }
    
	  
	public int getPartenaireId() {
		return partenaireId;
	}
	public void setPartenaireId(int partenaireId) {
		this.partenaireId = partenaireId;
   }
	public String getLoginPartenaire() {
		return loginPartenaire;
	}
	public void setLoginPartenaire(String loginPartenaire) {
		this.loginPartenaire = loginPartenaire;
	}
	public String getPasswordPartenaire() {
		return passwordPartenaire;
	}
	public void setPasswordPartenaire(String passwordPartenaire) {
		this.passwordPartenaire = passwordPartenaire;
	}
	public String getSuccess() {
		return success;
	}
	public void setSuccess(String success) {
		this.success = success;
	}
	public int getTypePartenaireId() {
		return TypePartenaireId;
	}

	public void setTypePartenaireId(int typePartenaireId) {
		TypePartenaireId = typePartenaireId;
	}

	public Partenaire getPartenaire() {
		return partenaire;
	}

	public void setPartenaire(Partenaire partenaire) {
		this.partenaire = partenaire;
	}

	public List<TypePartenaire> getListtypePartenaire() {
		return listtypePartenaire;
	}

	public void setListtypePartenaire(List<TypePartenaire> listtypePartenaire) {
		this.listtypePartenaire = listtypePartenaire;
	}

	public List<Partenaire> getListPartenaire() {
		return listPartenaire;
	}
	public void setListPartenaire(List<Partenaire> listPartenaire) {
		this.listPartenaire = listPartenaire;
	}
	public List<String> getCodesPostaux() {
		return codesPostaux;
	}



	public void setCodesPostaux(List<String> codesPostaux) {
		this.codesPostaux = codesPostaux;
	}



	public List<String> getVilles() {
		return villes;
	}



	public void setVilles(List<String> villes) {
		this.villes = villes;
	}



	public String getSelectedCP() {
		return selectedCP;
	}



	public void setSelectedCP(String selectedCP) {
		this.selectedCP = selectedCP;
	}



	public String getSelectedVille() {
		return selectedVille;
	}



	public void setSelectedVille(String selectedVille) {
		this.selectedVille = selectedVille;
	}



	public String getNomPartenaire() {
		return nomPartenaire;
	}


	public void setNomPartenaire(String nomPartenaire) {
		this.nomPartenaire = nomPartenaire;
	}


	public String getAdresseMailPartenaire() {
		return adresseMailPartenaire;
	}


	public void setAdresseMailPartenaire(String adresseMailPartenaire) {
		this.adresseMailPartenaire = adresseMailPartenaire;
	}


	public String getTelephonePartenaire() {
		return telephonePartenaire;
	}


	public void setTelephonePartenaire(String telephonePartenaire) {
		this.telephonePartenaire = telephonePartenaire;
	}


	public String getAdressePostalePartenaire() {
		return adressePostalePartenaire;
	}


	public void setAdressePostalePartenaire(String adressePostalePartenaire) {
		this.adressePostalePartenaire = adressePostalePartenaire;
	}


	public Date getDatePremierEnregistrementPartenaire() {
		return datePremierEnregistrementPartenaire;
	}


	public void setDatePremierEnregistrementPartenaire(Date datePremierEnregistrementPartenaire) {
		this.datePremierEnregistrementPartenaire = datePremierEnregistrementPartenaire;
	}


	public String getNomContactPartenaire() {
		return nomContactPartenaire;
	}


	public void setNomContactPartenaire(String nomContactPartenaire) {
		this.nomContactPartenaire = nomContactPartenaire;
	}


	public String getNumeroContactPartenaire() {
		return numeroContactPartenaire;
	}

	public void setNumeroContactPartenaire(String numeroContactPartenaire) {
		this.numeroContactPartenaire = numeroContactPartenaire;
	}


	public String getMailContactPartenaire() {
		return mailContactPartenaire;
	}

	public void setMailContactPartenaire(String mailContactPartenaire) {
		this.mailContactPartenaire = mailContactPartenaire;
	}
	public Date getDateDebutContrat() {
		return dateDebutContrat;
	}
	public void setDateDebutContrat(Date dateDebutContrat) {
		this.dateDebutContrat = dateDebutContrat;
	}
	public ContratPartenaire getNouveauContrat() {
		return nouveauContrat;
	}
	public void setNouveauContrat(ContratPartenaire nouveauContrat) {
		this.nouveauContrat = nouveauContrat;
	}
	public Partenaire getNouveauPartenaire() {
		return nouveauPartenaire;
	}
	public void setNouveauPartenaire(Partenaire nouveauPartenaire) {
		this.nouveauPartenaire = nouveauPartenaire;
	}
}
