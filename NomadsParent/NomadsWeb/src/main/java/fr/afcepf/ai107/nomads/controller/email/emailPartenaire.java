package fr.afcepf.ai107.nomads.controller.email;

import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class emailPartenaire {

	  public static void sendMail(String recepient) throws Exception {
		  System.out.println("Preparing to send email");
		  Properties properties = new Properties();
		  
		  
		  properties.put("mail.smtp.auth", "true");
		  properties.put("mail.smtp.starttls.enable", "true");
		  properties.put("mail.smtp.host", "smtp.gmail.com");
		  properties.put("mail.smtp.port", "587");
		 
		 
		  final String myAccountEmail = "nomadsafcepf@gmail.com";
		  final String password = "Adminnomads";
		  
		  @SuppressWarnings("unused")
		  Session session = Session.getInstance(properties, new Authenticator() {
			  @Override
			  protected PasswordAuthentication getPasswordAuthentication() {
				  
				  return new PasswordAuthentication(myAccountEmail, password);
			  }
		  
		});
		  Message message = prepareMessage(session , myAccountEmail , recepient);
	      Transport.send(message);
	      System.out.println("Message sent successfully");
	  }
	  

	private static Message prepareMessage(Session session , String myAccountEmail , String recepient ) {
		
		
		try {
			Message message = new  MimeMessage(session);
			message.setFrom(new InternetAddress(myAccountEmail));
			message.setRecipient(Message.RecipientType.TO,  new InternetAddress(recepient ));
			message.setSubject("Demande de devis");
			message.setText(" Bonjour TestPartenaire , \n Votre prestation à bien été créer");
			  
			return message;
		} catch (AddressException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
}
