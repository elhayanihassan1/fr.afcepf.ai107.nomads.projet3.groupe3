package fr.afcepf.ai107.nomads.controller.mads;


import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;

import fr.afcepf.ai107.nomads.entities.Masseur;
import fr.afcepf.ai107.nomads.entities.TypePackMads;
import fr.afcepf.ai107.nomads.masseurs.ibusiness.MasseurIBusiness;
import fr.afcepf.ai107.nomads.outils.ibusiness.OutilsIBusiness;
import fr.afcepf.ai107.nomads.outils.ibusiness.TypePackMadsIBusiness;

@ManagedBean(name = "mbAcheterMads")
public class AcheterMadsManagedBean {
	
	@EJB
	private MasseurIBusiness proxyMasseurBusiness;
	
	@EJB
	private TypePackMadsIBusiness proxyTypePackMadsBusiness;
	
	@ManagedProperty(value = "#{mbAccount.masseur}")
	private Masseur masseurConnected;
	
	private List<TypePackMads> typePackMads;
	private TypePackMads packMads;
	
	
	private TypePackMads packMadsChoisi;
	
	@PostConstruct
	public void init() {
			typePackMads=proxyTypePackMadsBusiness.allNonRemovedPackMads();
	}
	public String detailCommande() {
		
		return "/DetailCommande.xhtml?faces-redirect=true&idPack="+packMadsChoisi.getIdTypePack();
//		return "AccueilMasseur.xhtml?faces-redirect=true";
	}
	public List<TypePackMads> getTypePackMads() {
		return typePackMads;
	}

	public void setTypePackMads(List<TypePackMads> typePackMads) {
		this.typePackMads = typePackMads;
	}

	public TypePackMads getPackMadsChoisi() {
		return packMadsChoisi;
	}

	public void setPackMadsChoisi(TypePackMads packMadsChoisi) {
		this.packMadsChoisi = packMadsChoisi;
	}

	public Masseur getMasseurConnected() {
		return masseurConnected;
	}

	public void setMasseurConnected(Masseur masseurConnected) {
		this.masseurConnected = masseurConnected;
	}

	public TypePackMads getPackMads() {
		return packMads;
	}

	public void setPackMads(TypePackMads packMads) {
		this.packMads = packMads;
	}
	
}
