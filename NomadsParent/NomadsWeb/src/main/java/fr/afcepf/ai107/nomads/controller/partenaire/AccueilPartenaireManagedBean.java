package fr.afcepf.ai107.nomads.controller.partenaire;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import fr.afcepf.ai107.nomads.entities.InterventionReelle;
import fr.afcepf.ai107.nomads.entities.Partenaire;
import fr.afcepf.ai107.nomads.prestation.ibusiness.PrestationIBusiness;

@ManagedBean (name = "mbAccueilPart")
@ViewScoped
public class AccueilPartenaireManagedBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@EJB
	private PrestationIBusiness proxyPrestaBusiness;
	
	@ManagedProperty(value = "#{mbAccount.partenaire}")
	private Partenaire connectedPartenaire;
	
	private List<InterventionReelle> Interv3J ;
	private List<InterventionReelle> IntervAjD;
	private int dureePartenariat;
	private Date ajd = new Date();
	
	
	@PostConstruct
	public void init() {
		Interv3J = proxyPrestaBusiness.getAllInterventionFoNext3DAysByIdPart(connectedPartenaire.getIdPartenaire());
		IntervAjD =proxyPrestaBusiness.getAllInterventionFoTodayByIdPart(connectedPartenaire.getIdPartenaire());
		long diff = ajd.getTime() - connectedPartenaire.getDatePremierEnregistrementPartenaire().getTime();
	    float res = (diff / (1000*60*60*24));
		dureePartenariat = (int) res;
		
	}


	/**
	 * @return the interv3J
	 */
	public List<InterventionReelle> getInterv3J() {
		return Interv3J;
	}


	/**
	 * @param interv3j the interv3J to set
	 */
	public void setInterv3J(List<InterventionReelle> interv3j) {
		Interv3J = interv3j;
	}


	/**
	 * @return the intervAjD
	 */
	public List<InterventionReelle> getIntervAjD() {
		return IntervAjD;
	}


	/**
	 * @param intervAjD the intervAjD to set
	 */
	public void setIntervAjD(List<InterventionReelle> intervAjD) {
		IntervAjD = intervAjD;
	}


	/**
	 * @return the connectedPartenaire
	 */
	public Partenaire getConnectedPartenaire() {
		return connectedPartenaire;
	}


	/**
	 * @param connectedPartenaire the connectedPartenaire to set
	 */
	public void setConnectedPartenaire(Partenaire connectedPartenaire) {
		this.connectedPartenaire = connectedPartenaire;
	}


	/**
	 * @return the dureePartenariat
	 */
	public int getDureePartenariat() {
		return dureePartenariat;
	}


	/**
	 * @param dureePartenariat the dureePartenariat to set
	 */
	public void setDureePartenariat(int dureePartenariat) {
		this.dureePartenariat = dureePartenariat;
	}


	/**
	 * @return the ajd
	 */
	public Date getAjd() {
		return ajd;
	}


	/**
	 * @param ajd the ajd to set
	 */
	public void setAjd(Date ajd) {
		this.ajd = ajd;
	}
	
}
