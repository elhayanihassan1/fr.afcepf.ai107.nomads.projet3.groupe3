package fr.afcepf.ai107.nomads.controller.masseurs;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import fr.afcepf.ai107.nomads.entities.InterventionReelle;
import fr.afcepf.ai107.nomads.entities.Masseur;
import fr.afcepf.ai107.nomads.prestation.ibusiness.PrestationIBusiness;

@ManagedBean (name = "mbAccueilMasseur")
@ViewScoped
public class AccueilMasseurManagedBean {

	@EJB
	private PrestationIBusiness proxyPrestaBusiness;
	
	@ManagedProperty(value = "#{mbAccount.masseur}")
	private Masseur masseurConnected;
	
	private List<InterventionReelle> interventionsDuJour;
	
	@PostConstruct
	public void init() {
		interventionsDuJour= proxyPrestaBusiness.getAllInterventionsByIdMasseurToday(masseurConnected.getId_masseur());
	}

	/**
	 * @return the masseurConnected
	 */
	public Masseur getMasseurConnected() {
		return masseurConnected;
	}

	/**
	 * @param masseurConnected the masseurConnected to set
	 */
	public void setMasseurConnected(Masseur masseurConnected) {
		this.masseurConnected = masseurConnected;
	}

	/**
	 * @return the interventionsDuJour
	 */
	public List<InterventionReelle> getInterventionsDuJour() {
		return interventionsDuJour;
	}

	/**
	 * @param interventionsDuJour the interventionsDuJour to set
	 */
	public void setInterventionsDuJour(List<InterventionReelle> interventionsDuJour) {
		this.interventionsDuJour = interventionsDuJour;
	}
	
}
