package fr.afcepf.ai107.nomads.controller.masseurs;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import fr.afcepf.ai107.nomads.entities.Administrateur;
import fr.afcepf.ai107.nomads.entities.AffectationCategorieMasseur;
import fr.afcepf.ai107.nomads.entities.EvolutionProfessionnelle;
import fr.afcepf.ai107.nomads.entities.HistoriqueEvolutionProfessionnelleMasseur;
import fr.afcepf.ai107.nomads.entities.InterventionReelle;
import fr.afcepf.ai107.nomads.entities.Masseur;
import fr.afcepf.ai107.nomads.entities.Transaction;
import fr.afcepf.ai107.nomads.masseurs.ibusiness.MasseurIBusiness;
import fr.afcepf.ai107.nomads.outils.ibusiness.OutilsIBusiness;
import fr.afcepf.ai107.nomads.prestation.ibusiness.PrestationIBusiness;

@ManagedBean (name = "mbFicheMasseur")
@ViewScoped  //Session? Request? entre les deux?... REQUEST: tout fonctionne sauf le bouton de redirection==> Test passage en View
public class FicheMasseurManagedBean {
	
	@EJB
	private MasseurIBusiness proxyMasseurBusiness;
	
	@EJB
	private OutilsIBusiness proxyOutilBusiness;
	
	@EJB
	private PrestationIBusiness proxyPrestationBusiness;
	
	@ManagedProperty (value="#{mbAccount.admin}")
	private Administrateur adminConnected;
	
	@ManagedProperty (value="#{mbAccount.masseur}")
	private Masseur masseurConnected;
	
	private Masseur masseur;
	private Integer id;
	private int idMasseur;
	private EvolutionProfessionnelle derniereEvoPro;
	private List<HistoriqueEvolutionProfessionnelleMasseur> listeHistoEvoProMass;
	private HistoriqueEvolutionProfessionnelleMasseur dernierHistoEvoPro;
	private List<AffectationCategorieMasseur> listeAffectationCatMass;
	private List<Transaction> listeTransactionsMasseur;
	private List<InterventionReelle> listeInterventionsReellesMasseur;
	private Double soldus;
	private List<InterventionReelle> interventionsAVenir;
	private List<InterventionReelle> interventionsPassees; // #####################A IMPLEMENTER!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	private Integer idInterventionSelectionnee;
	
	@PostConstruct
	public void init() { 
		idMasseur= Integer.parseInt(FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("id"));
		masseur = proxyMasseurBusiness.getInfosMasseurById(idMasseur);
		setListeHistoEvoProMass(proxyOutilBusiness.historiqueEvoProMassByIdMasseur(idMasseur));
		dernierHistoEvoPro = listeHistoEvoProMass.get(listeHistoEvoProMass.size()-1);
		listeAffectationCatMass = proxyOutilBusiness.getListAffectationCatMassByIdMass(idMasseur);
		listeTransactionsMasseur = proxyOutilBusiness.getListTransactionByIdMasseur(idMasseur); 
		listeInterventionsReellesMasseur = proxyPrestationBusiness.getAllInfosInterventionReelleByIdMasseur(idMasseur);
		
		Double totalMads = 0.0;
		Double totalDepenses = 0.0;
		
		if(listeTransactionsMasseur != null) {
		for (Transaction transaction : listeTransactionsMasseur) {
			totalMads += transaction.getNombreDePack() * transaction.getTypePackMads().getValeurTypePackMads();
		}
		}else {
			totalMads = 0.0;
		}
		if(listeInterventionsReellesMasseur !=null) {
			for (InterventionReelle interventionReelle : listeInterventionsReellesMasseur) {
				Integer truc =interventionReelle.getPrestation().getCoutMads();
				if( truc != null) {
					totalDepenses += interventionReelle.getPrestation().getCoutMads();
				}
			}
		}else {
			totalDepenses = 0.0;
		}
		soldus = totalMads - totalDepenses;
		if (soldus == null) {
			soldus = 0.0;
		}
		

		interventionsAVenir = proxyPrestationBusiness.getAllInfosInterventionReelleAVenirByIdMasseur(idMasseur);
		
	}
	
	public String detailFichePresta () {
		return "FichePrestation.xhtml?faces-redirect=true&id="+idInterventionSelectionnee;
//		return "/FichePrestation.xhtml?faces-redirect=true&id=43";
//	OK	return "/MiseAJourMasseur.xhtml?faces-redirect=true&id=" + idMasseur;
	//	return"#";
}
	
	public String mettreAjour() { 
		
	return "/MiseAJourMasseur.xhtml?faces-redirect=true&id=" + idMasseur;
	}
	

	
	/**
	 * @return the masseur
	 */
	public Masseur getMasseur() {
		return masseur;
	}

	/**
	 * @param masseur the masseur to set
	 */
	public void setMasseur(Masseur masseur) {
		this.masseur = masseur;
	}

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the partenaireid
	 */
	public int getidMasseur() {
		return idMasseur;
	}

	/**
	 * @param partenaireid the partenaireid to set
	 */
	public void setidMasseur(int idMasseur) {
		this.idMasseur = idMasseur;
	}

	public List<HistoriqueEvolutionProfessionnelleMasseur> getListeHistoEvoProMass() {
		return listeHistoEvoProMass;
	}

	public void setListeHistoEvoProMass(List<HistoriqueEvolutionProfessionnelleMasseur> listeHistoEvoProMass) {
		this.listeHistoEvoProMass = listeHistoEvoProMass;
	}


	public EvolutionProfessionnelle getDerniereEvoPro() {
		return derniereEvoPro;
	}



	public void setDerniereEvoPro(EvolutionProfessionnelle derniereEvoPro) {
		this.derniereEvoPro = derniereEvoPro;
	}

	public HistoriqueEvolutionProfessionnelleMasseur getDernierHistoEvoPro() {
		return dernierHistoEvoPro;
	}

	public void setDernierHistoEvoPro(HistoriqueEvolutionProfessionnelleMasseur dernierHistoEvoPro) {
		this.dernierHistoEvoPro = dernierHistoEvoPro;
	}



	public List<AffectationCategorieMasseur> getListeAffectationCatMass() {
		return listeAffectationCatMass;
	}



	public void setListeAffectationCatMass(List<AffectationCategorieMasseur> listeAffectationCatMass) {
		this.listeAffectationCatMass = listeAffectationCatMass;
	}



	public List<Transaction> getListeTransactionsMasseur() {
		return listeTransactionsMasseur;
	}



	public void setListeTransactionsMasseur(List<Transaction> listeTransactionsMasseur) {
		this.listeTransactionsMasseur = listeTransactionsMasseur;
	}

	public List<InterventionReelle> getListeInterventionsReellesMasseur() {
		return listeInterventionsReellesMasseur;
	}

	public void setListeInterventionsReellesMasseur(List<InterventionReelle> listeInterventionsReellesMasseur) {
		this.listeInterventionsReellesMasseur = listeInterventionsReellesMasseur;
	}

	/**
	 * @return the soldus
	 */
	public double getSoldus() {
		return soldus;
	}

	/**
	 * @param soldus the soldus to set
	 */
	public void setSoldus(double soldus) {
		this.soldus = soldus;
	}

	public List<InterventionReelle> getInterventionsAVenir() {
		return interventionsAVenir;
	}

	public void setPrestationsAVenir(List<InterventionReelle> interventionsAVenir) {
		this.interventionsAVenir = interventionsAVenir;
	}

	public List<InterventionReelle> getInterventionsPassees() {
		return interventionsPassees;
	}

	public void setInterventionsPassees(List<InterventionReelle> interventionsPassees) {
		this.interventionsPassees = interventionsPassees;
	}

	/**
	 * @return the adminConnected
	 */
	public Administrateur getAdminConnected() {
		return adminConnected;
	}

	/**
	 * @param adminConnected the adminConnected to set
	 */
	public void setAdminConnected(Administrateur adminConnected) {
		this.adminConnected = adminConnected;
	}

	/**
	 * @return the idMasseur
	 */
	public int getIdMasseur() {
		return idMasseur;
	}

	/**
	 * @param idMasseur the idMasseur to set
	 */
	public void setIdMasseur(int idMasseur) {
		this.idMasseur = idMasseur;
	}

	/**
	 * @param soldus the soldus to set
	 */
	public void setSoldus(Double soldus) {
		this.soldus = soldus;
	}

	/**
	 * @param interventionsAVenir the interventionsAVenir to set
	 */
	public void setInterventionsAVenir(List<InterventionReelle> interventionsAVenir) {
		this.interventionsAVenir = interventionsAVenir;
	}

	/**
	 * @return the masseurConnected
	 */
	public Masseur getMasseurConnected() {
		return masseurConnected;
	}

	/**
	 * @param masseurConnected the masseurConnected to set
	 */
	public void setMasseurConnected(Masseur masseurConnected) {
		this.masseurConnected = masseurConnected;
	}

	public Integer getIdInterventionSelectionnee() {
		return idInterventionSelectionnee;
	}

	public void setIdInterventionSelectionnee(Integer idInterventionSelectionnee) {
		this.idInterventionSelectionnee = idInterventionSelectionnee;
	}

	

}
