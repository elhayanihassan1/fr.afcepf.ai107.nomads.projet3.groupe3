package fr.afcepf.ai107.nomads.entities;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import javax.persistence.Table;

@Entity
@Table(name = "motif_indisponibilite_masseur")
public class MotifIndisponibiliteMasseur implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_motif_indisponibilite_masseur")
	private int idMotifIndisponibiliteMasseur;

	@Column(name = "libelle_motif_indisponibilite_masseur")
	private String libelleMotifIndisponibiliteMasseur;
	
	@OneToMany (mappedBy = "motifIndisponibiliteMasseur", cascade = CascadeType.PERSIST,fetch = FetchType.LAZY)
	private Set<IndisponibiliteMasseur> indisponibilitesMasseur;

	public MotifIndisponibiliteMasseur() {
		super();
	}

	public MotifIndisponibiliteMasseur(int idMotifIndisponibiliteMasseur, String libelleMotifIndisponibiliteMasseur,
			Set<IndisponibiliteMasseur> indisponibilitesMasseur) {
		super();
		this.idMotifIndisponibiliteMasseur = idMotifIndisponibiliteMasseur;
		this.libelleMotifIndisponibiliteMasseur = libelleMotifIndisponibiliteMasseur;
		this.indisponibilitesMasseur = indisponibilitesMasseur;
	}

	public int getIdMotifIndisponibiliteMasseur() {
		return idMotifIndisponibiliteMasseur;
	}

	public void setIdMotifIndisponibiliteMasseur(int idMotifIndisponibiliteMasseur) {
		this.idMotifIndisponibiliteMasseur = idMotifIndisponibiliteMasseur;
	}

	public String getLibelleMotifIndisponibiliteMasseur() {
		return libelleMotifIndisponibiliteMasseur;
	}

	public void setLibelleMotifIndisponibiliteMasseur(String libelleMotifIndisponibiliteMasseur) {
		this.libelleMotifIndisponibiliteMasseur = libelleMotifIndisponibiliteMasseur;
	}

	public Set<IndisponibiliteMasseur> getIndisponibilitesMasseur() {
		return indisponibilitesMasseur;
	}

	public void setIndisponibilitesMasseur(Set<IndisponibiliteMasseur> indisponibilitesMasseur) {
		this.indisponibilitesMasseur = indisponibilitesMasseur;
	}
	
}