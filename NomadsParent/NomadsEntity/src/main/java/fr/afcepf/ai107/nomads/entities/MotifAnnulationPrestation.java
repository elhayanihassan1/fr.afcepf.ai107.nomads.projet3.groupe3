package fr.afcepf.ai107.nomads.entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import javax.persistence.Table;

@Entity
@Table(name = "motif_annulation_prestation")
public class MotifAnnulationPrestation implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_motif_annulation_prestation")
		private int idMotifAnnulationPrestation;

	@Column(name = "libelle_motif_annulation_prestation")
	private String libelleMotifAnnulationPrestation;

	@OneToMany(mappedBy = "motifAnnulationPrestation", cascade = CascadeType.PERSIST)
	private List<Prestation> prestation;
	
	@OneToMany(mappedBy = "motifAnnulationPrestation", cascade = CascadeType.ALL)
	private List<InterventionReelle> interventionReelle;
	
	public MotifAnnulationPrestation() {
		super();
		
	}

	public MotifAnnulationPrestation(int idMotifAnnulationPrestation, String libelleMotifAnnulationPrestation,
			List<Prestation> prestation, List<InterventionReelle> interventionReelle) {
		super();
		this.idMotifAnnulationPrestation = idMotifAnnulationPrestation;
		this.libelleMotifAnnulationPrestation = libelleMotifAnnulationPrestation;
		this.prestation = prestation;
		this.interventionReelle = interventionReelle;
	}

	/**
	 * @return the idMotifAnnulationPrestation
	 */
	public int getIdMotifAnnulationPrestation() {
		return idMotifAnnulationPrestation;
	}

	/**
	 * @param idMotifAnnulationPrestation the idMotifAnnulationPrestation to set
	 */
	public void setIdMotifAnnulationPrestation(int idMotifAnnulationPrestation) {
		this.idMotifAnnulationPrestation = idMotifAnnulationPrestation;
	}

	/**
	 * @return the libelleMotifAnnulationPrestation
	 */
	public String getLibelleMotifAnnulationPrestation() {
		return libelleMotifAnnulationPrestation;
	}

	/**
	 * @param libelleMotifAnnulationPrestation the libelleMotifAnnulationPrestation to set
	 */
	public void setLibelleMotifAnnulationPrestation(String libelleMotifAnnulationPrestation) {
		this.libelleMotifAnnulationPrestation = libelleMotifAnnulationPrestation;
	}

	/**
	 * @return the prestation
	 */
	public List<Prestation> getPrestation() {
		return prestation;
	}

	/**
	 * @param prestation the prestation to set
	 */
	public void setPrestation(List<Prestation> prestation) {
		this.prestation = prestation;
	}

	/**
	 * @return the interventionReelle
	 */
	public List<InterventionReelle> getInterventionReelle() {
		return interventionReelle;
	}

	/**
	 * @param interventionReelle the interventionReelle to set
	 */
	public void setInterventionReelle(List<InterventionReelle> interventionReelle) {
		this.interventionReelle = interventionReelle;
	}


	
	
}