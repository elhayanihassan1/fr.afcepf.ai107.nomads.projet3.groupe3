package fr.afcepf.ai107.nomads.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import javax.persistence.Table;

@Entity
@Table(name = "inscription_masseur")
public class InscriptionMasseurs implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_inscription_masseur")
	private Integer idInscriptionMasseur;

	@Column(name = "date_inscription_masseur")
	private Date dateInscriptionMasseur;

	@Column(name = "date_desinscription_masseur")
	private Date dateDesinscriptionMasseur;
	
	@ManyToOne
	@JoinColumn (name= "id_masseur", referencedColumnName = "id_masseur")
	private Masseur masseur;
	
	@ManyToOne
	@JoinColumn (name= "id_intervention_reelle", referencedColumnName = "id_intervention_reelle")
	private InterventionReelle interventionReelle;
	
	@ManyToOne
	@JoinColumn (name= "id_motif_desinscription_prestation", referencedColumnName = "id_motif_desinscription_prestation")
	private MotifDesinscriptionPrestation motifDesinscriptionPrestation;

	public InscriptionMasseurs() {
		super();
	}

	public InscriptionMasseurs(Integer idInscriptionMasseur, Date dateInscriptionMasseur, Date dateDesinscriptionMasseur,
			Masseur masseur, InterventionReelle interventionReelle,
			MotifDesinscriptionPrestation motifDesinscriptionPrestation) {
		super();
		this.idInscriptionMasseur = idInscriptionMasseur;
		this.dateInscriptionMasseur = dateInscriptionMasseur;
		this.dateDesinscriptionMasseur = dateDesinscriptionMasseur;
		this.masseur = masseur;
		this.interventionReelle = interventionReelle;
		this.motifDesinscriptionPrestation = motifDesinscriptionPrestation;
	}

	/**
	 * @return the idInscriptionMasseur
	 */
	public Integer getIdInscriptionMasseur() {
		return idInscriptionMasseur;
	}

	/**
	 * @param idInscriptionMasseur the idInscriptionMasseur to set
	 */
	public void setIdInscriptionMasseur(Integer idInscriptionMasseur) {
		this.idInscriptionMasseur = idInscriptionMasseur;
	}

	/**
	 * @return the dateInscriptionMasseur
	 */
	public Date getDateInscriptionMasseur() {
		return dateInscriptionMasseur;
	}

	/**
	 * @param dateInscriptionMasseur the dateInscriptionMasseur to set
	 */
	public void setDateInscriptionMasseur(Date dateInscriptionMasseur) {
		this.dateInscriptionMasseur = dateInscriptionMasseur;
	}

	/**
	 * @return the dateDesinscriptionMasseur
	 */
	public Date getDateDesinscriptionMasseur() {
		return dateDesinscriptionMasseur;
	}

	/**
	 * @param dateDesinscriptionMasseur the dateDesinscriptionMasseur to set
	 */
	public void setDateDesinscriptionMasseur(Date dateDesinscriptionMasseur) {
		this.dateDesinscriptionMasseur = dateDesinscriptionMasseur;
	}

	/**
	 * @return the masseur
	 */
	public Masseur getMasseur() {
		return masseur;
	}

	/**
	 * @param masseur the masseur to set
	 */
	public void setMasseur(Masseur masseur) {
		this.masseur = masseur;
	}

	/**
	 * @return the interventionReelle
	 */
	public InterventionReelle getInterventionReelle() {
		return interventionReelle;
	}

	/**
	 * @param interventionReelle the interventionReelle to set
	 */
	public void setInterventionReelle(InterventionReelle interventionReelle) {
		this.interventionReelle = interventionReelle;
	}

	/**
	 * @return the motifDesinscriptionPrestation
	 */
	public MotifDesinscriptionPrestation getMotifDesinscriptionPrestation() {
		return motifDesinscriptionPrestation;
	}

	/**
	 * @param motifDesinscriptionPrestation the motifDesinscriptionPrestation to set
	 */
	public void setMotifDesinscriptionPrestation(MotifDesinscriptionPrestation motifDesinscriptionPrestation) {
		this.motifDesinscriptionPrestation = motifDesinscriptionPrestation;
	}

}