package fr.afcepf.ai107.nomads.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import javax.persistence.Table;

@Entity
@Table(name = "indisponibilite_masseur")
public class IndisponibiliteMasseur implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_indisponibilite_masseur")
	private int id_indisponibilite_masseur;

	@Column(name = "date_debut_indisponibilite_masseur")
	private Date dateDebutIndisponibiliteMasseur;

	@Column(name = "date_fin_indisponibilite_masseur")
	private Date dateFinIndisponibiliteMasseur;

	@ManyToOne
	@JoinColumn(name = "id_masseur", referencedColumnName = "id_masseur")
	private Masseur masseur;

	@ManyToOne
	@JoinColumn(name = "id_motif_indisponibilite_masseur", referencedColumnName = "id_motif_indisponibilite_masseur")
	private MotifIndisponibiliteMasseur motifIndisponibiliteMasseur;

	/**
	 * 
	 */
	public IndisponibiliteMasseur() {
		super();
	}

	/**
	 * @param id_indisponibilite_masseur
	 * @param dateDebutIndisponibiliteMasseur
	 * @param dateFinIndisponibiliteMasseur
	 * @param masseur
	 * @param motifIndisponibiliteMasseur
	 */
	public IndisponibiliteMasseur(int id_indisponibilite_masseur, Date dateDebutIndisponibiliteMasseur,
			Date dateFinIndisponibiliteMasseur, Masseur masseur,
			MotifIndisponibiliteMasseur motifIndisponibiliteMasseur) {
		super();
		this.id_indisponibilite_masseur = id_indisponibilite_masseur;
		this.dateDebutIndisponibiliteMasseur = dateDebutIndisponibiliteMasseur;
		this.dateFinIndisponibiliteMasseur = dateFinIndisponibiliteMasseur;
		this.masseur = masseur;
		this.motifIndisponibiliteMasseur = motifIndisponibiliteMasseur;
	}

	/**
	 * @return the id_indisponibilite_masseur
	 */
	public int getId_indisponibilite_masseur() {
		return id_indisponibilite_masseur;
	}

	/**
	 * @param id_indisponibilite_masseur the id_indisponibilite_masseur to set
	 */
	public void setId_indisponibilite_masseur(int id_indisponibilite_masseur) {
		this.id_indisponibilite_masseur = id_indisponibilite_masseur;
	}

	/**
	 * @return the dateDebutIndisponibiliteMasseur
	 */
	public Date getDateDebutIndisponibiliteMasseur() {
		return dateDebutIndisponibiliteMasseur;
	}

	/**
	 * @param dateDebutIndisponibiliteMasseur the dateDebutIndisponibiliteMasseur to
	 *                                        set
	 */
	public void setDateDebutIndisponibiliteMasseur(Date dateDebutIndisponibiliteMasseur) {
		this.dateDebutIndisponibiliteMasseur = dateDebutIndisponibiliteMasseur;
	}

	/**
	 * @return the dateFinIndisponibiliteMasseur
	 */
	public Date getDateFinIndisponibiliteMasseur() {
		return dateFinIndisponibiliteMasseur;
	}

	/**
	 * @param dateFinIndisponibiliteMasseur the dateFinIndisponibiliteMasseur to set
	 */
	public void setDateFinIndisponibiliteMasseur(Date dateFinIndisponibiliteMasseur) {
		this.dateFinIndisponibiliteMasseur = dateFinIndisponibiliteMasseur;
	}

	/**
	 * @return the masseur
	 */
	public Masseur getMasseur() {
		return masseur;
	}

	/**
	 * @param masseur the masseur to set
	 */
	public void setMasseur(Masseur masseur) {
		this.masseur = masseur;
	}

	/**
	 * @return the motifIndisponibiliteMasseur
	 */
	public MotifIndisponibiliteMasseur getMotifIndisponibiliteMasseur() {
		return motifIndisponibiliteMasseur;
	}

	/**
	 * @param motifIndisponibiliteMasseur the motifIndisponibiliteMasseur to set
	 */
	public void setMotifIndisponibiliteMasseur(MotifIndisponibiliteMasseur motifIndisponibiliteMasseur) {
		this.motifIndisponibiliteMasseur = motifIndisponibiliteMasseur;
	}

}