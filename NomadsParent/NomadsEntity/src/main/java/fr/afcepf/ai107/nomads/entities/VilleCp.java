package fr.afcepf.ai107.nomads.entities;

import java.io.Serializable;

import java.util.List;

import java.util.Set;


import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import javax.persistence.Table;

@Entity
@Table(name = "ville_cp")
public class VilleCp implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_ville_cp")
	private int idVilleCp;

	@Column(name = "libelle_ville")
	private String libelleVille;

	@Column(name = "libelle_cp")
	private String libelleCp;
	
	@OneToMany (mappedBy = "villeCp", cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
	private Set<Masseur> masseurs;

	@OneToMany(mappedBy = "villeCp", cascade = CascadeType.PERSIST)
	private List<Partenaire> partenaire;
	
	@OneToMany(mappedBy = "villeCp", cascade = CascadeType.PERSIST) 
	private List<Masseur> masseur;
	
	@OneToMany(mappedBy = "villeCp", cascade = CascadeType.PERSIST)
	private List <Prestation> prestation;
	
	@OneToMany(mappedBy = "villeCp", cascade = CascadeType.PERSIST)
	private List<Administrateur> administrateur;

	public VilleCp() {
		super();
		
	}

	
	public VilleCp(int idVilleCp, String libelleVille, String libelleCp, Set<Masseur> masseurs,
			List<Partenaire> partenaire, List<Masseur> masseur, List<Prestation> prestation,
			List<Administrateur> administrateur) {
		super();
		this.idVilleCp = idVilleCp;
		this.libelleVille = libelleVille;
		this.libelleCp = libelleCp;
		this.masseurs = masseurs;
		this.partenaire = partenaire;
		this.masseur = masseur;
		this.prestation = prestation;
		this.administrateur = administrateur;
	}

	

	/**
	 * @return the masseurs
	 */
	public Set<Masseur> getMasseurs() {
		return masseurs;
	}


	/**
	 * @param masseurs the masseurs to set
	 */
	public void setMasseurs(Set<Masseur> masseurs) {
		this.masseurs = masseurs;
	}


	/**
	 * @return the administrateur
	 */
	public List<Administrateur> getAdministrateur() {
		return administrateur;
	}


	/**
	 * @param administrateur the administrateur to set
	 */
	public void setAdministrateur(List<Administrateur> administrateur) {
		this.administrateur = administrateur;
	}


	/**
	 * @return the idVilleCp
	 */
	public int getIdVilleCp() {
		return idVilleCp;
	}

	/**
	 * @param idVilleCp the idVilleCp to set
	 */
	public void setIdVilleCp(int idVilleCp) {
		this.idVilleCp = idVilleCp;
	}

	/**
	 * @return the libelleVille
	 */
	public String getLibelleVille() {
		return libelleVille;
	}

	/**
	 * @param libelleVille the libelleVille to set
	 */
	public void setLibelleVille(String libelleVille) {
		this.libelleVille = libelleVille;
	}

	/**
	 * @return the libelleCp
	 */
	public String getLibelleCp() {
		return libelleCp;
	}

	/**
	 * @param libelleCp the libelleCp to set
	 */
	public void setLibelleCp(String libelleCp) {
		this.libelleCp = libelleCp;
	}

	/**
	 * @return the partenaire
	 */
	public List<Partenaire> getPartenaire() {
		return partenaire;
	}

	/**
	 * @param partenaire the partenaire to set
	 */
	public void setPartenaire(List<Partenaire> partenaire) {
		this.partenaire = partenaire;
	}

	/**
	 * @return the masseur
	 */
	public List<Masseur> getMasseur() {
		return masseur;
	}

	/**
	 * @param masseur the masseur to set
	 */
	public void setMasseur(List<Masseur> masseur) {
		this.masseur = masseur;
	}

	/**
	 * @return the prestation
	 */
	public List<Prestation> getPrestation() {
		return prestation;
	}

	/**
	 * @param prestation the prestation to set
	 */
	public void setPrestation(List<Prestation> prestation) {
		this.prestation = prestation;
	}


	@Override
	public String toString() {
		return "VilleCp [idVilleCp=" + idVilleCp + ", libelleVille=" + libelleVille + ", libelleCp=" + libelleCp
				+ ", masseurs=" + masseurs + ", partenaire=" + partenaire + ", masseur=" + masseur + ", prestation="
				+ prestation + ", administrateur=" + administrateur + "]";
	}


	
}