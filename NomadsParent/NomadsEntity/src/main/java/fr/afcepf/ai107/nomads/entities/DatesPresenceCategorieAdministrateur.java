package fr.afcepf.ai107.nomads.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import javax.persistence.Table;

@Entity
@Table(name = "dates_presence_categorie_administrateur")
public class DatesPresenceCategorieAdministrateur implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_dates_presence_categorie_administrateur")
	private int id_datePresenceCategorie;

	@Column(name = "date_entree_categorie_administrateur")
	private Date dateEntreeCategorie;

	@Column(name = "date_sortie_categorie_administrateur")
	private Date dateSortieCategorie;

	@ManyToOne
	@JoinColumn(name = "id_administrateur", referencedColumnName = "id_administrateur")
	private Administrateur administrateur; 
	
	@ManyToOne
	@JoinColumn(name="id_categorie_administrateur", referencedColumnName = "id_categorie_administrateur ")
	private CategorieAdministrateur categorieAdministrateur;

	public DatesPresenceCategorieAdministrateur() {
		super();
		
	}

	public DatesPresenceCategorieAdministrateur(int id_datePresenceCategorie, Date dateEntreeCategorie, Date dateSortieCategorie,
			Administrateur administrateur, CategorieAdministrateur categorieAdministrateur) {
		super();
		this.id_datePresenceCategorie = id_datePresenceCategorie;
		this.dateEntreeCategorie = dateEntreeCategorie;
		this.dateSortieCategorie = dateSortieCategorie;
		this.administrateur = administrateur;
		this.categorieAdministrateur = categorieAdministrateur;
	}

	/**
	 * @return the id_datePresenceCategorie
	 */
	public int getId_datePresenceCategorie() {
		return id_datePresenceCategorie;
	}

	/**
	 * @param id_datePresenceCategorie the id_datePresenceCategorie to set
	 */
	public void setId_datePresenceCategorie(int id_datePresenceCategorie) {
		this.id_datePresenceCategorie = id_datePresenceCategorie;
	}

	/**
	 * @return the dateEntreeCategorie
	 */
	public Date getDateEntreeCategorie() {
		return dateEntreeCategorie;
	}

	/**
	 * @param dateEntreeCategorie the dateEntreeCategorie to set
	 */
	public void setDateEntreeCategorie(Date dateEntreeCategorie) {
		this.dateEntreeCategorie = dateEntreeCategorie;
	}

	/**
	 * @return the dateSortieCategorie
	 */
	public Date getDateSortieCategorie() {
		return dateSortieCategorie;
	}

	/**
	 * @param dateSortieCategorie the dateSortieCategorie to set
	 */
	public void setDateSortieCategorie(Date dateSortieCategorie) {
		this.dateSortieCategorie = dateSortieCategorie;
	}

	/**
	 * @return the administrateur
	 */
	public Administrateur getAdministrateur() {
		return administrateur;
	}

	/**
	 * @param administrateur the administrateur to set
	 */
	public void setAdministrateur(Administrateur administrateur) {
		this.administrateur = administrateur;
	}

	/**
	 * @return the categorieAdministrateur
	 */
	public CategorieAdministrateur getCategorieAdministrateur() {
		return categorieAdministrateur;
	}

	/**
	 * @param categorieAdministrateur the categorieAdministrateur to set
	 */
	public void setCategorieAdministrateur(CategorieAdministrateur categorieAdministrateur) {
		this.categorieAdministrateur = categorieAdministrateur;
	}
	
	
}