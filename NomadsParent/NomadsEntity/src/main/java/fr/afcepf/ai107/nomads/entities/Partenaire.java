package fr.afcepf.ai107.nomads.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "partenaire")
public class Partenaire implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_partenaire") 
	private Integer idPartenaire;

	@Column(name = "nom_partenaire")
	private String nomPartenaire;

	@Column(name = "adresse_mail_partenaire")
	private String adresseMailPartenaire;

	@Column(name = "telephone_partenaire")
	private String telephonePartenaire;

	@Column(name = "adresse_postale_partenaire")
	private String adressePostalePartenaire;

	@Temporal(value = TemporalType.DATE)
	@Column(name = "date_premier_enregistrement_partenaire")
	private Date datePremierEnregistrementPartenaire;

	@Column(name = "nom_contact_partenaire")
	private String nomContactPartenaire;

	@Column(name = "numero_contact_partenaire")
	private String numeroContactPartenaire;

	@Column(name = "mail_contact_partenaire")
	private String mailContactPartenaire;

	@Column(name = "login_partenaire")
	private String loginPartenaire;

	@Column(name = "password_partenaire")
	private String passwordPartenaire;
	
	@Column(name = "logo_partenaire")
	private String logoPartenaire;

	@OneToMany(mappedBy = "partenaire", cascade = CascadeType.ALL)
	private List<ContratPartenaire> contratPartenaire;
	
	@ManyToOne
	@JoinColumn(name = "id_type_partenaire", referencedColumnName = "id_type_partenaire")
	private TypePartenaire typePartenaire;
	
	@OneToMany(mappedBy = "partenaire", cascade = CascadeType.MERGE)
	private List<IndisponibilitePartenaire> indisponibilitePartenaire;
	
	@OneToMany(mappedBy = "partenaire", cascade = CascadeType.PERSIST)
	private List<RetourExperiencePartenaire> retourExperiencePartenaire;
	
	@ManyToOne
	@JoinColumn(name = "id_ville_cp", referencedColumnName = "id_ville_cp")
	private VilleCp villeCp;
	
	@OneToMany(mappedBy = "partenaire", cascade = CascadeType.ALL)
	private List<Prestation> prestation;

	/**
	 * 
	 */
	public Partenaire() {
		super();
	}

	/**
	 * @param idPartenaire
	 * @param nomPartenaire
	 * @param adresseMailPartenaire
	 * @param telephonePartenaire
	 * @param adressePostalePartenaire
	 * @param datePremierEnregistrementPartenaire
	 * @param nomContactPartenaire
	 * @param numeroContactPartenaire
	 * @param mailContactPartenaire
	 * @param loginPartenaire
	 * @param passwordPartenaire
	 * @param logoPartenaire
	 * @param contratPartenaire
	 * @param typePartenaire
	 * @param indisponibilitePartenaire
	 * @param retourExperiencePartenaire
	 * @param villeCp
	 * @param prestation
	 */
	public Partenaire(Integer idPartenaire, String nomPartenaire, String adresseMailPartenaire, String telephonePartenaire,
			String adressePostalePartenaire, Date datePremierEnregistrementPartenaire, String nomContactPartenaire,
			String numeroContactPartenaire, String mailContactPartenaire, String loginPartenaire,
			String passwordPartenaire, String logoPartenaire, List<ContratPartenaire> contratPartenaire,
			TypePartenaire typePartenaire, List<IndisponibilitePartenaire> indisponibilitePartenaire,
			List<RetourExperiencePartenaire> retourExperiencePartenaire, VilleCp villeCp, List<Prestation> prestation) {
		super();
		this.idPartenaire = idPartenaire;
		this.nomPartenaire = nomPartenaire;
		this.adresseMailPartenaire = adresseMailPartenaire;
		this.telephonePartenaire = telephonePartenaire;
		this.adressePostalePartenaire = adressePostalePartenaire;
		this.datePremierEnregistrementPartenaire = datePremierEnregistrementPartenaire;
		this.nomContactPartenaire = nomContactPartenaire;
		this.numeroContactPartenaire = numeroContactPartenaire;
		this.mailContactPartenaire = mailContactPartenaire;
		this.loginPartenaire = loginPartenaire;
		this.passwordPartenaire = passwordPartenaire;
		this.logoPartenaire = logoPartenaire;
		this.contratPartenaire = contratPartenaire;
		this.typePartenaire = typePartenaire;
		this.indisponibilitePartenaire = indisponibilitePartenaire;
		this.retourExperiencePartenaire = retourExperiencePartenaire;
		this.villeCp = villeCp;
		this.prestation = prestation;
	}

	/**
	 * @return the idPartenaire
	 */
	public Integer getIdPartenaire() {
		return idPartenaire;
	}

	/**
	 * @param idPartenaire the idPartenaire to set
	 */
	public void setIdPartenaire(Integer idPartenaire) {
		this.idPartenaire = idPartenaire;
	}

	/**
	 * @return the nomPartenaire
	 */
	public String getNomPartenaire() {
		return nomPartenaire;
	}

	/**
	 * @param nomPartenaire the nomPartenaire to set
	 */
	public void setNomPartenaire(String nomPartenaire) {
		this.nomPartenaire = nomPartenaire;
	}

	/**
	 * @return the adresseMailPartenaire
	 */
	public String getAdresseMailPartenaire() {
		return adresseMailPartenaire;
	}

	/**
	 * @param adresseMailPartenaire the adresseMailPartenaire to set
	 */
	public void setAdresseMailPartenaire(String adresseMailPartenaire) {
		this.adresseMailPartenaire = adresseMailPartenaire;
	}

	/**
	 * @return the telephonePartenaire
	 */
	public String getTelephonePartenaire() {
		return telephonePartenaire;
	}

	/**
	 * @param telephonePartenaire the telephonePartenaire to set
	 */
	public void setTelephonePartenaire(String telephonePartenaire) {
		this.telephonePartenaire = telephonePartenaire;
	}

	/**
	 * @return the adressePostalePartenaire
	 */
	public String getAdressePostalePartenaire() {
		return adressePostalePartenaire;
	}

	/**
	 * @param adressePostalePartenaire the adressePostalePartenaire to set
	 */
	public void setAdressePostalePartenaire(String adressePostalePartenaire) {
		this.adressePostalePartenaire = adressePostalePartenaire;
	}

	/**
	 * @return the datePremierEnregistrementPartenaire
	 */
	public Date getDatePremierEnregistrementPartenaire() {
		return datePremierEnregistrementPartenaire;
	}

	/**
	 * @param datePremierEnregistrementPartenaire the datePremierEnregistrementPartenaire to set
	 */
	public void setDatePremierEnregistrementPartenaire(Date datePremierEnregistrementPartenaire) {
		this.datePremierEnregistrementPartenaire = datePremierEnregistrementPartenaire;
	}

	/**
	 * @return the nomContactPartenaire
	 */
	public String getNomContactPartenaire() {
		return nomContactPartenaire;
	}

	/**
	 * @param nomContactPartenaire the nomContactPartenaire to set
	 */
	public void setNomContactPartenaire(String nomContactPartenaire) {
		this.nomContactPartenaire = nomContactPartenaire;
	}

	/**
	 * @return the numeroContactPartenaire
	 */
	public String getNumeroContactPartenaire() {
		return numeroContactPartenaire;
	}

	/**
	 * @param numeroContactPartenaire the numeroContactPartenaire to set
	 */
	public void setNumeroContactPartenaire(String numeroContactPartenaire) {
		this.numeroContactPartenaire = numeroContactPartenaire;
	}

	/**
	 * @return the mailContactPartenaire
	 */
	public String getMailContactPartenaire() {
		return mailContactPartenaire;
	}

	/**
	 * @param mailContactPartenaire the mailContactPartenaire to set
	 */
	public void setMailContactPartenaire(String mailContactPartenaire) {
		this.mailContactPartenaire = mailContactPartenaire;
	}

	/**
	 * @return the loginPartenaire
	 */
	public String getLoginPartenaire() {
		return loginPartenaire;
	}

	/**
	 * @param loginPartenaire the loginPartenaire to set
	 */
	public void setLoginPartenaire(String loginPartenaire) {
		this.loginPartenaire = loginPartenaire;
	}

	/**
	 * @return the passwordPartenaire
	 */
	public String getPasswordPartenaire() {
		return passwordPartenaire;
	}

	/**
	 * @param passwordPartenaire the passwordPartenaire to set
	 */
	public void setPasswordPartenaire(String passwordPartenaire) {
		this.passwordPartenaire = passwordPartenaire;
	}

	/**
	 * @return the logoPartenaire
	 */
	public String getLogoPartenaire() {
		return logoPartenaire;
	}

	/**
	 * @param logoPartenaire the logoPartenaire to set
	 */
	public void setLogoPartenaire(String logoPartenaire) {
		this.logoPartenaire = logoPartenaire;
	}

	/**
	 * @return the contratPartenaire
	 */
	public List<ContratPartenaire> getContratPartenaire() {
		return contratPartenaire;
	}

	/**
	 * @param contratPartenaire the contratPartenaire to set
	 */
	public void setContratPartenaire(List<ContratPartenaire> contratPartenaire) {
		this.contratPartenaire = contratPartenaire;
	}

	/**
	 * @return the typePartenaire
	 */
	public TypePartenaire getTypePartenaire() {
		return typePartenaire;
	}

	/**
	 * @param typePartenaire the typePartenaire to set
	 */
	public void setTypePartenaire(TypePartenaire typePartenaire) {
		this.typePartenaire = typePartenaire;
	}

	/**
	 * @return the indisponibilitePartenaire
	 */
	public List<IndisponibilitePartenaire> getIndisponibilitePartenaire() {
		return indisponibilitePartenaire;
	}

	/**
	 * @param indisponibilitePartenaire the indisponibilitePartenaire to set
	 */
	public void setIndisponibilitePartenaire(List<IndisponibilitePartenaire> indisponibilitePartenaire) {
		this.indisponibilitePartenaire = indisponibilitePartenaire;
	}

	/**
	 * @return the retourExperiencePartenaire
	 */
	public List<RetourExperiencePartenaire> getRetourExperiencePartenaire() {
		return retourExperiencePartenaire;
	}

	/**
	 * @param retourExperiencePartenaire the retourExperiencePartenaire to set
	 */
	public void setRetourExperiencePartenaire(List<RetourExperiencePartenaire> retourExperiencePartenaire) {
		this.retourExperiencePartenaire = retourExperiencePartenaire;
	}

	/**
	 * @return the villeCp
	 */
	public VilleCp getVilleCp() {
		return villeCp;
	}

	/**
	 * @param villeCp the villeCp to set
	 */
	public void setVilleCp(VilleCp villeCp) {
		this.villeCp = villeCp;
	}

	/**
	 * @return the prestation
	 */
	public List<Prestation> getPrestation() {
		return prestation;
	}

	/**
	 * @param prestation the prestation to set
	 */
	public void setPrestation(List<Prestation> prestation) {
		this.prestation = prestation;
	}

	
	
}