package fr.afcepf.ai107.nomads.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import javax.persistence.Table;

@Entity
@Table(name = "session_formation_masseur")
public class SessionFormationMasseur implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_session_formation_masseur")
	private int idSessionFormationMasseur;

	@Column(name = "libelle_session_formation_masseur")
	private String libelleSessionFormationMasseur;

	@Column(name = "date_debut_session_formation_masseur")
	private Date dateDebutSessionFormationMasseur;

	@Column(name = "date_fin_session_formation_masseur")
	private Date dateFinSessionFormationMasseur;

	@Column(name = "nombre_masseurs_session_formation_masseur")
	private int nombreMasseursSessionFormationMasseur;

	@Column(name = "specificites_session_formation_masseur")
	private String specificitesSessionFormationMasseur;

	@ManyToOne
	@JoinColumn(name = "id_administrateur", referencedColumnName = "id_administrateur")
	private Administrateur administrateur; 
	
	@OneToMany(mappedBy = "sessionFormationMasseur", cascade = CascadeType.PERSIST)
	private List<InscriptionSessionFormation> inscriptionSessionFormation;


	/**
	 * 
	 */
	public SessionFormationMasseur() {
		super();
	}

	/**
	 * @param idSessionFormationMasseur
	 * @param libelleSessionFormationMasseur
	 * @param dateDebutSessionFormationMasseur
	 * @param dateFinSessionFormationMasseur
	 * @param nombreMasseursSessionFormationMasseur
	 * @param specificitesSessionFormationMasseur
	 * @param administrateur
	 * @param inscriptionSessionFormation
	 */
	public SessionFormationMasseur(int idSessionFormationMasseur, String libelleSessionFormationMasseur,
			Date dateDebutSessionFormationMasseur, Date dateFinSessionFormationMasseur,
			int nombreMasseursSessionFormationMasseur, String specificitesSessionFormationMasseur,
			Administrateur administrateur, List<InscriptionSessionFormation> inscriptionSessionFormation) {
		super();
		this.idSessionFormationMasseur = idSessionFormationMasseur;
		this.libelleSessionFormationMasseur = libelleSessionFormationMasseur;
		this.dateDebutSessionFormationMasseur = dateDebutSessionFormationMasseur;
		this.dateFinSessionFormationMasseur = dateFinSessionFormationMasseur;
		this.nombreMasseursSessionFormationMasseur = nombreMasseursSessionFormationMasseur;
		this.specificitesSessionFormationMasseur = specificitesSessionFormationMasseur;
		this.administrateur = administrateur;
		this.inscriptionSessionFormation = inscriptionSessionFormation;
	}

	/**
	 * @return the idSessionFormationMasseur
	 */
	public int getIdSessionFormationMasseur() {
		return idSessionFormationMasseur;
	}


	/**
	 * @param idSessionFormationMasseur the idSessionFormationMasseur to set
	 */
	public void setIdSessionFormationMasseur(int idSessionFormationMasseur) {
		this.idSessionFormationMasseur = idSessionFormationMasseur;
	}


	/**
	 * @return the libelleSessionFormationMasseur
	 */
	public String getLibelleSessionFormationMasseur() {
		return libelleSessionFormationMasseur;
	}


	/**
	 * @param libelleSessionFormationMasseur the libelleSessionFormationMasseur to set
	 */
	public void setLibelleSessionFormationMasseur(String libelleSessionFormationMasseur) {
		this.libelleSessionFormationMasseur = libelleSessionFormationMasseur;
	}


	/**
	 * @return the dateDebutSessionFormationMasseur
	 */
	public Date getDateDebutSessionFormationMasseur() {
		return dateDebutSessionFormationMasseur;
	}


	/**
	 * @param dateDebutSessionFormationMasseur the dateDebutSessionFormationMasseur to set
	 */
	public void setDateDebutSessionFormationMasseur(Date dateDebutSessionFormationMasseur) {
		this.dateDebutSessionFormationMasseur = dateDebutSessionFormationMasseur;
	}


	/**
	 * @return the dateFinSessionFormationMasseur
	 */
	public Date getDateFinSessionFormationMasseur() {
		return dateFinSessionFormationMasseur;
	}


	/**
	 * @param dateFinSessionFormationMasseur the dateFinSessionFormationMasseur to set
	 */
	public void setDateFinSessionFormationMasseur(Date dateFinSessionFormationMasseur) {
		this.dateFinSessionFormationMasseur = dateFinSessionFormationMasseur;
	}


	/**
	 * @return the nombreMasseursSessionFormationMasseur
	 */
	public int getNombreMasseursSessionFormationMasseur() {
		return nombreMasseursSessionFormationMasseur;
	}


	/**
	 * @param nombreMasseursSessionFormationMasseur the nombreMasseursSessionFormationMasseur to set
	 */
	public void setNombreMasseursSessionFormationMasseur(int nombreMasseursSessionFormationMasseur) {
		this.nombreMasseursSessionFormationMasseur = nombreMasseursSessionFormationMasseur;
	}


	/**
	 * @return the specificitesSessionFormationMasseur
	 */
	public String getSpecificitesSessionFormationMasseur() {
		return specificitesSessionFormationMasseur;
	}


	/**
	 * @param specificitesSessionFormationMasseur the specificitesSessionFormationMasseur to set
	 */
	public void setSpecificitesSessionFormationMasseur(String specificitesSessionFormationMasseur) {
		this.specificitesSessionFormationMasseur = specificitesSessionFormationMasseur;
	}


	/**
	 * @return the administrateur
	 */
	public Administrateur getAdministrateur() {
		return administrateur;
	}

	/**
	 * @param administrateur the administrateur to set
	 */
	public void setAdministrateur(Administrateur administrateur) {
		this.administrateur = administrateur;
	}

	/**
	 * @return the inscriptionSessionFormation
	 */
	public List<InscriptionSessionFormation> getInscriptionSessionFormation() {
		return inscriptionSessionFormation;
	}

	/**
	 * @param inscriptionSessionFormation the inscriptionSessionFormation to set
	 */
	public void setInscriptionSessionFormation(List<InscriptionSessionFormation> inscriptionSessionFormation) {
		this.inscriptionSessionFormation = inscriptionSessionFormation;
	}
	
	
}