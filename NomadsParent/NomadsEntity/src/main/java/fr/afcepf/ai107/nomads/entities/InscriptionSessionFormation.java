package fr.afcepf.ai107.nomads.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import javax.persistence.Table;

@Entity
@Table(name = "inscription_session_formation")
public class InscriptionSessionFormation implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_inscription_session_formation")
	private int idInscriptionSessionFormation;

	@Column(name = "date_debut_inscription_session_formation")
	private Date dateDebutInscriptionSessionFormation;

	@Column(name = "date_desinscription_session_formation")
	private Date dateDesinscriptionSessionFormation;

	@ManyToOne
	@JoinColumn(name = "id_session_formation_masseur", referencedColumnName = "id_session_formation_masseur")
	private SessionFormationMasseur sessionFormationMasseur;

	@ManyToOne
	@JoinColumn(name = "id_masseur", referencedColumnName = "id_masseur")
	private Masseur masseur;

	@ManyToOne
	@JoinColumn(name = "id_motif_desinscription_session_formation", referencedColumnName = "id_motif_desinscription_session_formation")
	private MotifDesinscriptionSessionFormation motifDesinscriptionSessionFormation;

	/**
	 * 
	 */
	public InscriptionSessionFormation() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param idInscriptionSessionFormation
	 * @param dateDebutInscriptionSessionFormation
	 * @param dateDesinscriptionSessionFormation
	 * @param sessionFormationMasseur
	 * @param masseur
	 * @param motifDesinscriptionSessionFormation
	 */
	public InscriptionSessionFormation(int idInscriptionSessionFormation, Date dateDebutInscriptionSessionFormation,
			Date dateDesinscriptionSessionFormation, SessionFormationMasseur sessionFormationMasseur, Masseur masseur,
			MotifDesinscriptionSessionFormation motifDesinscriptionSessionFormation) {
		super();
		this.idInscriptionSessionFormation = idInscriptionSessionFormation;
		this.dateDebutInscriptionSessionFormation = dateDebutInscriptionSessionFormation;
		this.dateDesinscriptionSessionFormation = dateDesinscriptionSessionFormation;
		this.sessionFormationMasseur = sessionFormationMasseur;
		this.masseur = masseur;
		this.motifDesinscriptionSessionFormation = motifDesinscriptionSessionFormation;
	}

	/**
	 * @return the idInscriptionSessionFormation
	 */
	public int getIdInscriptionSessionFormation() {
		return idInscriptionSessionFormation;
	}

	/**
	 * @param idInscriptionSessionFormation the idInscriptionSessionFormation to set
	 */
	public void setIdInscriptionSessionFormation(int idInscriptionSessionFormation) {
		this.idInscriptionSessionFormation = idInscriptionSessionFormation;
	}

	/**
	 * @return the dateDebutInscriptionSessionFormation
	 */
	public Date getDateDebutInscriptionSessionFormation() {
		return dateDebutInscriptionSessionFormation;
	}

	/**
	 * @param dateDebutInscriptionSessionFormation the
	 *                                             dateDebutInscriptionSessionFormation
	 *                                             to set
	 */
	public void setDateDebutInscriptionSessionFormation(Date dateDebutInscriptionSessionFormation) {
		this.dateDebutInscriptionSessionFormation = dateDebutInscriptionSessionFormation;
	}

	/**
	 * @return the dateDesinscriptionSessionFormation
	 */
	public Date getDateDesinscriptionSessionFormation() {
		return dateDesinscriptionSessionFormation;
	}

	/**
	 * @param dateDesinscriptionSessionFormation the
	 *                                           dateDesinscriptionSessionFormation
	 *                                           to set
	 */
	public void setDateDesinscriptionSessionFormation(Date dateDesinscriptionSessionFormation) {
		this.dateDesinscriptionSessionFormation = dateDesinscriptionSessionFormation;
	}

	/**
	 * @return the sessionFormationMasseur
	 */
	public SessionFormationMasseur getSessionFormationMasseur() {
		return sessionFormationMasseur;
	}

	/**
	 * @param sessionFormationMasseur the sessionFormationMasseur to set
	 */
	public void setSessionFormationMasseur(SessionFormationMasseur sessionFormationMasseur) {
		this.sessionFormationMasseur = sessionFormationMasseur;
	}

	/**
	 * @return the masseur
	 */
	public Masseur getMasseur() {
		return masseur;
	}

	/**
	 * @param masseur the masseur to set
	 */
	public void setMasseur(Masseur masseur) {
		this.masseur = masseur;
	}

	/**
	 * @return the motifDesinscriptionSessionFormation
	 */
	public MotifDesinscriptionSessionFormation getMotifDesinscriptionSessionFormation() {
		return motifDesinscriptionSessionFormation;
	}

	/**
	 * @param motifDesinscriptionSessionFormation the
	 *                                            motifDesinscriptionSessionFormation
	 *                                            to set
	 */
	public void setMotifDesinscriptionSessionFormation(
			MotifDesinscriptionSessionFormation motifDesinscriptionSessionFormation) {
		this.motifDesinscriptionSessionFormation = motifDesinscriptionSessionFormation;
	}

}