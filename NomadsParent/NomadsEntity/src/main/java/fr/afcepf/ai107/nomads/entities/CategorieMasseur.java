package fr.afcepf.ai107.nomads.entities;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "categorie_masseur")
public class CategorieMasseur implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_categorie_masseur")
	private Integer idCategorieMasseur;

	@Column(name = "libelle_categorie_masseur")
	private String libelleCategorieMasseur;
	
	@OneToMany(mappedBy = "categorieMasseur", cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
	private List<AffectationCategorieMasseur> affectationsCategorieMasseur;

	public CategorieMasseur() {
		super();
		
	}


	public CategorieMasseur(Integer idCategorieMasseur, String libelleCategorieMasseur,
			List<AffectationCategorieMasseur> affectationsCategorieMasseur) {
		super();
		this.idCategorieMasseur = idCategorieMasseur;
		this.libelleCategorieMasseur = libelleCategorieMasseur;
		this.affectationsCategorieMasseur = affectationsCategorieMasseur;
	}

	public Integer getIdCategorieMasseur() {
		return idCategorieMasseur;
	}

	public void setIdCategorieMasseur(Integer idCategorieMasseur) {
		this.idCategorieMasseur = idCategorieMasseur;
	}

	public String getLibelleCategorieMasseur() {
		return libelleCategorieMasseur;
	}

	public void setLibelleCategorieMasseur(String libelleCategorieMasseur) {
		this.libelleCategorieMasseur = libelleCategorieMasseur;
	}

	public List<AffectationCategorieMasseur> getAffectationsCategorieMasseur() {
		return affectationsCategorieMasseur;
	}

	public void setAffectationsCategorieMasseur(List<AffectationCategorieMasseur> affectationsCategorieMasseur) {
		this.affectationsCategorieMasseur = affectationsCategorieMasseur;
	}
	
}