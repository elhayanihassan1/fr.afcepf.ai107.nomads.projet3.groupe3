package fr.afcepf.ai107.nomads.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import javax.persistence.Table;

@Entity
@Table(name = "affectation_categorie_masseur")
public class AffectationCategorieMasseur implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_affectation_categorie_masseur")
	private Integer idAffectationCategorieMasseur;

	@Column(name = "date_entree_categorie_masseur")
	private Date dateEntreeCategorieMasseur;

	@Column(name = "date_sortie_categorie_masseur")
	private Date dateSortieCategorieMasseur;
	
	@ManyToOne
	@JoinColumn(name = "id_masseur", referencedColumnName = "id_masseur")
	private Masseur masseur;
	
	@ManyToOne
	@JoinColumn(name = "id_categorie_masseur", referencedColumnName = "id_categorie_masseur")
	private CategorieMasseur categorieMasseur;

	public AffectationCategorieMasseur() {
		super();
		
	}

	public AffectationCategorieMasseur(Integer idAffectationCategorieMasseur, Date dateEntreeCategorieMasseur,
			Date dateSortieCategorieMasseur, Masseur masseur, CategorieMasseur categorieMasseur) {
		super();
		this.idAffectationCategorieMasseur = idAffectationCategorieMasseur;
		this.dateEntreeCategorieMasseur = dateEntreeCategorieMasseur;
		this.dateSortieCategorieMasseur = dateSortieCategorieMasseur;
		this.masseur = masseur;
		this.categorieMasseur = categorieMasseur;
	}

	public Integer getIdAffectationCategorieMasseur() {
		return idAffectationCategorieMasseur;
	}

	public void setIdAffectationCategorieMasseur(Integer idAffectationCategorieMasseur) {
		this.idAffectationCategorieMasseur = idAffectationCategorieMasseur;
	}

	public Date getDateEntreeCategorieMasseur() {
		return dateEntreeCategorieMasseur;
	}

	public void setDateEntreeCategorieMasseur(Date dateEntreeCategorieMasseur) {
		this.dateEntreeCategorieMasseur = dateEntreeCategorieMasseur;
	}

	public Date getDateSortieCategorieMasseur() {
		return dateSortieCategorieMasseur;
	}

	public void setDateSortieCategorieMasseur(Date dateSortieCategorieMasseur) {
		this.dateSortieCategorieMasseur = dateSortieCategorieMasseur;
	}

	public Masseur getMasseur() {
		return masseur;
	}

	public void setMasseur(Masseur masseur) {
		this.masseur = masseur;
	}

	public CategorieMasseur getCategorieMasseur() {
		return categorieMasseur;
	}

	public void setCategorieMasseur(CategorieMasseur categorieMasseur) {
		this.categorieMasseur = categorieMasseur;
	}
	
}