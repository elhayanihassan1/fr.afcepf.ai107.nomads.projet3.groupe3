package fr.afcepf.ai107.nomads.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import javax.persistence.Table;

@Entity
@Table(name = "administrateur")
public class Administrateur implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_administrateur")
	private int id_administrateur;

	@Column(name = "nom_administrateur")
	private String nomAdministrateur;

	@Column(name = "prenom_administrateur")
	private String prenomAdministrateur;

	@Column(name = "date_de_naissance_administrateur")
	private Date dateDeNaissanceAdministrateur;

	@Column(name = "adresse_postale_administrateur")
	private String adressePostaleAdministrateur;

	@Column(name = "telephone_administrateur")
	private String telephoneAdministrateur;

	@Column(name = "login_administrateur")
	private String loginAdministrateur;

	@Column(name = "password_administrateur")
	private String passwordAdministrateur;
	
	@Column(name = "photo_administrateur")
	private String photoAdministrateur;

	@OneToMany(mappedBy = "administrateur", cascade = CascadeType.PERSIST)
	private List<IndisponibiliteAdministrateur> indisponibiliteAdministrateur;
	
	@OneToMany(mappedBy = "administrateur", cascade = CascadeType.PERSIST)
	private List<DatesPresenceCategorieAdministrateur> datesPresenceCategorieAdministrateur;
	
	@OneToMany(mappedBy = "administrateur", cascade = CascadeType.PERSIST)
	private List<SessionFormationMasseur> sessionFormationMasseur;
	
	@ManyToOne
	@JoinColumn(name = "id_ville_cp", referencedColumnName = "id_ville_cp")
	private VilleCp villeCp;

	/**
	 * 
	 */
	public Administrateur() {
		super();
	}

	/**
	 * @param id_administrateur
	 * @param nomAdministrateur
	 * @param prenomAdministrateur
	 * @param dateDeNaissanceAdministrateur
	 * @param adressePostaleAdministrateur
	 * @param telephoneAdministrateur
	 * @param loginAdministrateur
	 * @param passwordAdministrateur
	 * @param photoAdministrateur
	 * @param indisponibiliteAdministrateur
	 * @param datesPresenceCategorieAdministrateur
	 * @param sessionFormationMasseur
	 * @param villeCp
	 */
	public Administrateur(int id_administrateur, String nomAdministrateur, String prenomAdministrateur,
			Date dateDeNaissanceAdministrateur, String adressePostaleAdministrateur, String telephoneAdministrateur,
			String loginAdministrateur, String passwordAdministrateur, String photoAdministrateur,
			List<IndisponibiliteAdministrateur> indisponibiliteAdministrateur,
			List<DatesPresenceCategorieAdministrateur> datesPresenceCategorieAdministrateur,
			List<SessionFormationMasseur> sessionFormationMasseur, VilleCp villeCp) {
		super();
		this.id_administrateur = id_administrateur;
		this.nomAdministrateur = nomAdministrateur;
		this.prenomAdministrateur = prenomAdministrateur;
		this.dateDeNaissanceAdministrateur = dateDeNaissanceAdministrateur;
		this.adressePostaleAdministrateur = adressePostaleAdministrateur;
		this.telephoneAdministrateur = telephoneAdministrateur;
		this.loginAdministrateur = loginAdministrateur;
		this.passwordAdministrateur = passwordAdministrateur;
		this.photoAdministrateur = photoAdministrateur;
		this.indisponibiliteAdministrateur = indisponibiliteAdministrateur;
		this.datesPresenceCategorieAdministrateur = datesPresenceCategorieAdministrateur;
		this.sessionFormationMasseur = sessionFormationMasseur;
		this.villeCp = villeCp;
	}

	/**
	 * @return the id_administrateur
	 */
	public int getId_administrateur() {
		return id_administrateur;
	}

	/**
	 * @param id_administrateur the id_administrateur to set
	 */
	public void setId_administrateur(int id_administrateur) {
		this.id_administrateur = id_administrateur;
	}

	/**
	 * @return the nomAdministrateur
	 */
	public String getNomAdministrateur() {
		return nomAdministrateur;
	}

	/**
	 * @param nomAdministrateur the nomAdministrateur to set
	 */
	public void setNomAdministrateur(String nomAdministrateur) {
		this.nomAdministrateur = nomAdministrateur;
	}

	/**
	 * @return the prenomAdministrateur
	 */
	public String getPrenomAdministrateur() {
		return prenomAdministrateur;
	}

	/**
	 * @param prenomAdministrateur the prenomAdministrateur to set
	 */
	public void setPrenomAdministrateur(String prenomAdministrateur) {
		this.prenomAdministrateur = prenomAdministrateur;
	}

	/**
	 * @return the dateDeNaissanceAdministrateur
	 */
	public Date getDateDeNaissanceAdministrateur() {
		return dateDeNaissanceAdministrateur;
	}

	/**
	 * @param dateDeNaissanceAdministrateur the dateDeNaissanceAdministrateur to set
	 */
	public void setDateDeNaissanceAdministrateur(Date dateDeNaissanceAdministrateur) {
		this.dateDeNaissanceAdministrateur = dateDeNaissanceAdministrateur;
	}

	/**
	 * @return the adressePostaleAdministrateur
	 */
	public String getAdressePostaleAdministrateur() {
		return adressePostaleAdministrateur;
	}

	/**
	 * @param adressePostaleAdministrateur the adressePostaleAdministrateur to set
	 */
	public void setAdressePostaleAdministrateur(String adressePostaleAdministrateur) {
		this.adressePostaleAdministrateur = adressePostaleAdministrateur;
	}

	/**
	 * @return the telephoneAdministrateur
	 */
	public String getTelephoneAdministrateur() {
		return telephoneAdministrateur;
	}

	/**
	 * @param telephoneAdministrateur the telephoneAdministrateur to set
	 */
	public void setTelephoneAdministrateur(String telephoneAdministrateur) {
		this.telephoneAdministrateur = telephoneAdministrateur;
	}

	/**
	 * @return the loginAdministrateur
	 */
	public String getLoginAdministrateur() {
		return loginAdministrateur;
	}

	/**
	 * @param loginAdministrateur the loginAdministrateur to set
	 */
	public void setLoginAdministrateur(String loginAdministrateur) {
		this.loginAdministrateur = loginAdministrateur;
	}

	/**
	 * @return the passwordAdministrateur
	 */
	public String getPasswordAdministrateur() {
		return passwordAdministrateur;
	}

	/**
	 * @param passwordAdministrateur the passwordAdministrateur to set
	 */
	public void setPasswordAdministrateur(String passwordAdministrateur) {
		this.passwordAdministrateur = passwordAdministrateur;
	}

	/**
	 * @return the photoAdministrateur
	 */
	public String getPhotoAdministrateur() {
		return photoAdministrateur;
	}

	/**
	 * @param photoAdministrateur the photoAdministrateur to set
	 */
	public void setPhotoAdministrateur(String photoAdministrateur) {
		this.photoAdministrateur = photoAdministrateur;
	}

	/**
	 * @return the indisponibiliteAdministrateur
	 */
	public List<IndisponibiliteAdministrateur> getIndisponibiliteAdministrateur() {
		return indisponibiliteAdministrateur;
	}

	/**
	 * @param indisponibiliteAdministrateur the indisponibiliteAdministrateur to set
	 */
	public void setIndisponibiliteAdministrateur(List<IndisponibiliteAdministrateur> indisponibiliteAdministrateur) {
		this.indisponibiliteAdministrateur = indisponibiliteAdministrateur;
	}

	/**
	 * @return the datesPresenceCategorieAdministrateur
	 */
	public List<DatesPresenceCategorieAdministrateur> getDatesPresenceCategorieAdministrateur() {
		return datesPresenceCategorieAdministrateur;
	}

	/**
	 * @param datesPresenceCategorieAdministrateur the datesPresenceCategorieAdministrateur to set
	 */
	public void setDatesPresenceCategorieAdministrateur(
			List<DatesPresenceCategorieAdministrateur> datesPresenceCategorieAdministrateur) {
		this.datesPresenceCategorieAdministrateur = datesPresenceCategorieAdministrateur;
	}

	/**
	 * @return the sessionFormationMasseur
	 */
	public List<SessionFormationMasseur> getSessionFormationMasseur() {
		return sessionFormationMasseur;
	}

	/**
	 * @param sessionFormationMasseur the sessionFormationMasseur to set
	 */
	public void setSessionFormationMasseur(List<SessionFormationMasseur> sessionFormationMasseur) {
		this.sessionFormationMasseur = sessionFormationMasseur;
	}

	/**
	 * @return the villeCp
	 */
	public VilleCp getVilleCp() {
		return villeCp;
	}

	/**
	 * @param villeCp the villeCp to set
	 */
	public void setVilleCp(VilleCp villeCp) {
		this.villeCp = villeCp;
	}
	
}