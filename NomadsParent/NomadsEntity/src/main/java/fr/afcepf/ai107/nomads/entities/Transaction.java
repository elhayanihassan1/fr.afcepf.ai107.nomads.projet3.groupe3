package fr.afcepf.ai107.nomads.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "transaction")
public class Transaction implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_transaction")
	private int idTransaction;

	@Column(name = "nombre_de_pack")
	private int nombreDePack;

	@Column(name = "date_transaction")
	private Date dateTransaction;
	
	@ManyToOne 
	@JoinColumn (name = "id_masseur", referencedColumnName = "id_masseur")
	private Masseur masseur;
	
	@ManyToOne 
	@JoinColumn (name = "id_type_pack_mads", referencedColumnName = "id_type_pack_mads")
	private TypePackMads typePackMads;

	public Transaction() {
		super();
		
	}

	public Transaction(int idTransaction, int nombreDePack, Date dateTransaction, Masseur masseur,
			TypePackMads typePackMads) {
		super();
		this.idTransaction = idTransaction;
		this.nombreDePack = nombreDePack;
		this.dateTransaction = dateTransaction;
		this.masseur = masseur;
		this.typePackMads = typePackMads;
	}

	public int getIdTransaction() {
		return idTransaction;
	}

	public void setIdTransaction(int idTransaction) {
		this.idTransaction = idTransaction;
	}

	public int getNombreDePack() {
		return nombreDePack;
	}

	public void setNombreDePack(int nombreDePack) {
		this.nombreDePack = nombreDePack;
	}

	public Date getDateTransaction() {
		return dateTransaction;
	}

	public void setDateTransaction(Date dateTransaction) {
		this.dateTransaction = dateTransaction;
	}

	public Masseur getMasseur() {
		return masseur;
	}

	public void setMasseur(Masseur masseur) {
		this.masseur = masseur;
	}

	public TypePackMads getTypePackMads() {
		return typePackMads;
	}

	public void setTypePackMads(TypePackMads typePackMads) {
		this.typePackMads = typePackMads;
	}
	
}