
package fr.afcepf.ai107.nomads.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "prestation")
public class Prestation implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_prestation")
	private int idPrestation;

	@Column(name = "cout_devis_prestation")
	private Double coutDevisPrestation;
	
	@Temporal(value = TemporalType.DATE)
	@Column(name = "date_emission_prestation")
	private Date dateEmissionPrestation;
	
	@Temporal(value = TemporalType.DATE)
	@Column(name = "date_validation")
	private Date dateValidationPrestation;

	@Column(name = "libelle_prestation")
	private String libellePrestation;
	@Temporal(value = TemporalType.TIME)
	@Column(name = "heure_debut_prestation")
	private Date heureDebutPrestation;
	@Temporal(value = TemporalType.TIME)
	@Column(name = "heure_fin_prestation")
	private Date heureFinPrestation;

	@Column(name = "nombre_masseurs_prestation")
	private Integer nombreMasseursPrestation;

	@Column(name = "nombre_chaises_ergonomiques_prestation")
	private Integer nombreChaisesErgonomiquesPrestation;

	@Column(name = "montant_remuneration_prestation")
	private Double montantRemunerationPrestation;

	@Column(name = "date_annulation_prestation")
	private Date dateAnnulationPrestation;
    
	@Temporal(value = TemporalType.DATE)
	@Column(name = "date_paiement_prestation")
	private Date datePaiementPrestation;

	@Column(name = "adresse_intervention_prestation")
	private String adresseInterventionPrestation;

	@Column(name = "specificites_prestation")
	private String specificitesPrestation;

	@Column(name = "cout_mads_prestation")
	private Integer coutMads;

	@ManyToOne
	@JoinColumn(name = "id_partenaire", referencedColumnName = "id_partenaire")
	private Partenaire partenaire; 
	
	@ManyToOne
	@JoinColumn(name = "id_ville_cp", referencedColumnName = "id_ville_cp")
	private VilleCp villeCp;
	
	@ManyToOne
	@JoinColumn(name = "id_motif_annulation_prestation", referencedColumnName = "id_motif_annulation_prestation")
	private MotifAnnulationPrestation motifAnnulationPrestation;
	
	@ManyToOne
	@JoinColumn(name = "id_type_paiement", referencedColumnName = "id_type_paiement")
	private TypePaiement typePaiement; 
	
	@OneToMany(mappedBy = "prestation", cascade = CascadeType.ALL)
	private List<InterventionReelle> interventionReelle; 
	
	@OneToMany(mappedBy = "prestation", cascade = CascadeType.PERSIST)
	private List<LocationMateriel> locationMateriel;

	/**
	 * 
	 */
	public Prestation() {
		super();
		// TODO Auto-generated constructor stub
	}

 
	public Prestation(int idPrestation, Double coutDevisPrestation, Date dateEmissionPrestation,
			Date dateValidationPrestation, String libellePrestation, Date heureDebutPrestation, Date heureFinPrestation,
			Integer nombreMasseursPrestation, Integer nombreChaisesErgonomiquesPrestation,
			Double montantRemunerationPrestation, Date dateAnnulationPrestation, Date datePaiementPrestation,
			String adresseInterventionPrestation, String specificitesPrestation, Integer coutMads,
			Partenaire partenaire, VilleCp villeCp, MotifAnnulationPrestation motifAnnulationPrestation,
			TypePaiement typePaiement, List<InterventionReelle> interventionReelle,
			List<LocationMateriel> locationMateriel) {
		super();
		this.idPrestation = idPrestation;
		this.coutDevisPrestation = coutDevisPrestation;
		this.dateEmissionPrestation = dateEmissionPrestation;
		this.dateValidationPrestation = dateValidationPrestation;
		this.libellePrestation = libellePrestation;
		this.heureDebutPrestation = heureDebutPrestation;
		this.heureFinPrestation = heureFinPrestation;
		this.nombreMasseursPrestation = nombreMasseursPrestation;
		this.nombreChaisesErgonomiquesPrestation = nombreChaisesErgonomiquesPrestation;
		this.montantRemunerationPrestation = montantRemunerationPrestation;
		this.dateAnnulationPrestation = dateAnnulationPrestation;
		this.datePaiementPrestation = datePaiementPrestation;
		this.adresseInterventionPrestation = adresseInterventionPrestation;
		this.specificitesPrestation = specificitesPrestation;
		this.coutMads = coutMads;
		this.partenaire = partenaire;
		this.villeCp = villeCp;
		this.motifAnnulationPrestation = motifAnnulationPrestation;
		this.typePaiement = typePaiement;
		this.interventionReelle = interventionReelle;
		this.locationMateriel = locationMateriel;
	}


	public void addIntervention(InterventionReelle interv) {
		interventionReelle.add(interv);
		interv.setPrestation(this);
	}


 

	public Date getHeureDebutPrestation() {
		return heureDebutPrestation;
	}


	public void setHeureDebutPrestation(Date heureDebutPrestation) {
		this.heureDebutPrestation = heureDebutPrestation;
	}


	public Date getHeureFinPrestation() {
		return heureFinPrestation;
	}


	public void setHeureFinPrestation(Date heureFinPrestation) {
		this.heureFinPrestation = heureFinPrestation;
	}


	public Double getCoutDevisPrestation() {
		return coutDevisPrestation;
	}
	
	public void setCoutDevisPrestation(Double coutDevisPrestation) {
		this.coutDevisPrestation = coutDevisPrestation;
	}

	/**
	 * @return the dateEmissionPrestation
	 */
	public Date getDateEmissionPrestation() {
		return dateEmissionPrestation;
	}

	/**
	 * @param dateEmissionPrestation the dateEmissionPrestation to set
	 */
	public void setDateEmissionPrestation(Date dateEmissionPrestation) {
		this.dateEmissionPrestation = dateEmissionPrestation;
	}

	/**
	 * @return the dateValidationPrestation
	 */
	public Date getDateValidationPrestation() {
		return dateValidationPrestation;
	}

	/**
	 * @param dateValidationPrestation the dateValidationPrestation to set
	 */
	public void setDateValidationPrestation(Date dateValidationPrestation) {
		this.dateValidationPrestation = dateValidationPrestation;
	}

	/**
	 * @return the libellePrestation
	 */
	public String getLibellePrestation() {
		return libellePrestation;
	}

	/**
	 * @param libellePrestation the libellePrestation to set
	 */
	public void setLibellePrestation(String libellePrestation) {
		this.libellePrestation = libellePrestation;
	}



	/**
	 * @return the nombreMasseursPrestation
	 */


	/**
	 * @param nombreMasseursPrestation the nombreMasseursPrestation to set
	 */
	public void setNombreMasseursPrestation(int nombreMasseursPrestation) {
		this.nombreMasseursPrestation = nombreMasseursPrestation;
	}


	
	public int getIdPrestation() {
		return idPrestation;
	}

	public void setIdPrestation(int idPrestation) {
		this.idPrestation = idPrestation;
	}

	public Integer getNombreMasseursPrestation() {
		return nombreMasseursPrestation;
	}

	public void setNombreMasseursPrestation(Integer nombreMasseursPrestation) {
		this.nombreMasseursPrestation = nombreMasseursPrestation;
	}

	public Integer getNombreChaisesErgonomiquesPrestation() {
		return nombreChaisesErgonomiquesPrestation;
	}

	public void setNombreChaisesErgonomiquesPrestation(Integer nombreChaisesErgonomiquesPrestation) {
		this.nombreChaisesErgonomiquesPrestation = nombreChaisesErgonomiquesPrestation;
	}



	public void setMontantRemunerationPrestation(Double montantRemunerationPrestation) {
		this.montantRemunerationPrestation = montantRemunerationPrestation;
	}

	/**
	 * @param nombreChaisesErgonomiquesPrestation the nombreChaisesErgonomiquesPrestation to set
	 */
	public void setNombreChaisesErgonomiquesPrestation(int nombreChaisesErgonomiquesPrestation) {
		this.nombreChaisesErgonomiquesPrestation = nombreChaisesErgonomiquesPrestation;
	}

	/**
	 * @return the montantRemunerationPrestation
	 */
	public double getMontantRemunerationPrestation() {
		return montantRemunerationPrestation;
	}

	/**
	 * @param montantRemunerationPrestation the montantRemunerationPrestation to set
	 */
	public void setMontantRemunerationPrestation(double montantRemunerationPrestation) {
		this.montantRemunerationPrestation = montantRemunerationPrestation;
	}

	/**
	 * @return the dateAnnulationPrestation
	 */
	public Date getDateAnnulationPrestation() {
		return dateAnnulationPrestation;
	}

	/**
	 * @param dateAnnulationPrestation the dateAnnulationPrestation to set
	 */
	public void setDateAnnulationPrestation(Date dateAnnulationPrestation) {
		this.dateAnnulationPrestation = dateAnnulationPrestation;
	}

	/**
	 * @return the datePaiementPrestation
	 */
	public Date getDatePaiementPrestation() {
		return datePaiementPrestation;
	}

	/**
	 * @param datePaiementPrestation the datePaiementPrestation to set
	 */
	public void setDatePaiementPrestation(Date datePaiementPrestation) {
		this.datePaiementPrestation = datePaiementPrestation;
	}

	/**
	 * @return the adresseInterventionPrestation
	 */
	public String getAdresseInterventionPrestation() {
		return adresseInterventionPrestation;
	}

	/**
	 * @param adresseInterventionPrestation the adresseInterventionPrestation to set
	 */
	public void setAdresseInterventionPrestation(String adresseInterventionPrestation) {
		this.adresseInterventionPrestation = adresseInterventionPrestation;
	}

	/**
	 * @return the specificitesPrestation
	 */
	public String getSpecificitesPrestation() {
		return specificitesPrestation;
	}

	/**
	 * @param specificitesPrestation the specificitesPrestation to set
	 */
	public void setSpecificitesPrestation(String specificitesPrestation) {
		this.specificitesPrestation = specificitesPrestation;
	}

	/**
	 * @return the coutMads
	 */
	public Integer getCoutMads() {
		return coutMads;
	}

	/**
	 * @param coutMads the coutMads to set
	 */
	public void setCoutMads(Integer coutMads) {
		this.coutMads = coutMads;
	}

	/**
	 * @return the partenaire
	 */
	public Partenaire getPartenaire() {
		return partenaire;
	}

	/**
	 * @param partenaire the partenaire to set
	 */
	public void setPartenaire(Partenaire partenaire) {
		this.partenaire = partenaire;
	}

	/**
	 * @return the villeCp
	 */
	public VilleCp getVilleCp() {
		return villeCp;
	}

	/**
	 * @param villeCp the villeCp to set
	 */
	public void setVilleCp(VilleCp villeCp) {
		this.villeCp = villeCp;
	}

	/**
	 * @return the motifAnnulationPrestation
	 */
	public MotifAnnulationPrestation getMotifAnnulationPrestation() {
		return motifAnnulationPrestation;
	}

	/**
	 * @param motifAnnulationPrestation the motifAnnulationPrestation to set
	 */
	public void setMotifAnnulationPrestation(MotifAnnulationPrestation motifAnnulationPrestation) {
		this.motifAnnulationPrestation = motifAnnulationPrestation;
	}

	/**
	 * @return the typePaiement
	 */
	public TypePaiement getTypePaiement() {
		return typePaiement;
	}

	/**
	 * @param typePaiement the typePaiement to set
	 */
	public void setTypePaiement(TypePaiement typePaiement) {
		this.typePaiement = typePaiement;
	}

	/**
	 * @return the interventionReelle
	 */
	public List<InterventionReelle> getInterventionReelle() {
		return interventionReelle;
	}

	/**
	 * @param interventionReelle the interventionReelle to set
	 */
	public void setInterventionReelle(List<InterventionReelle> interventionReelle) {
		this.interventionReelle = interventionReelle;
	}

	/**
	 * @return the locationMateriel
	 */
	public List<LocationMateriel> getLocationMateriel() {
		return locationMateriel;
	}

	/**
	 * @param locationMateriel the locationMateriel to set
	 */
	public void setLocationMateriel(List<LocationMateriel> locationMateriel) {
		this.locationMateriel = locationMateriel;
	}



	}