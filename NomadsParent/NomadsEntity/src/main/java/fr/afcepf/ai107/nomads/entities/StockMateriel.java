package fr.afcepf.ai107.nomads.entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import javax.persistence.Table;

@Entity
@Table(name = "stock_materiel")
public class StockMateriel implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_stock_materiel")
	private int idStockMateriel;

	@Column(name = "libelle_article_stock_materiel")
	private String libelleArticleStockMateriel;

	@Column(name = "quantite_article_stock_materiel")
	private int quantiteArticleStockMateriel;

	@OneToMany(mappedBy = "stockMateriel", cascade = CascadeType.PERSIST)
	private List<LocationMateriel> locationMateriel;

	/**
	 * 
	 */
	public StockMateriel() {
		super();
	}

	/**
	 * @param idStockMateriel
	 * @param libelleArticleStockMateriel
	 * @param quantiteArticleStockMateriel
	 * @param locationMateriel
	 */
	public StockMateriel(int idStockMateriel, String libelleArticleStockMateriel, int quantiteArticleStockMateriel,
			List<LocationMateriel> locationMateriel) {
		super();
		this.idStockMateriel = idStockMateriel;
		this.libelleArticleStockMateriel = libelleArticleStockMateriel;
		this.quantiteArticleStockMateriel = quantiteArticleStockMateriel;
		this.locationMateriel = locationMateriel;
	}

	/**
	 * @return the idStockMateriel
	 */
	public int getIdStockMateriel() {
		return idStockMateriel;
	}

	/**
	 * @param idStockMateriel the idStockMateriel to set
	 */
	public void setIdStockMateriel(int idStockMateriel) {
		this.idStockMateriel = idStockMateriel;
	}

	/**
	 * @return the libelleArticleStockMateriel
	 */
	public String getLibelleArticleStockMateriel() {
		return libelleArticleStockMateriel;
	}

	/**
	 * @param libelleArticleStockMateriel the libelleArticleStockMateriel to set
	 */
	public void setLibelleArticleStockMateriel(String libelleArticleStockMateriel) {
		this.libelleArticleStockMateriel = libelleArticleStockMateriel;
	}

	/**
	 * @return the quantiteArticleStockMateriel
	 */
	public int getQuantiteArticleStockMateriel() {
		return quantiteArticleStockMateriel;
	}

	/**
	 * @param quantiteArticleStockMateriel the quantiteArticleStockMateriel to set
	 */
	public void setQuantiteArticleStockMateriel(int quantiteArticleStockMateriel) {
		this.quantiteArticleStockMateriel = quantiteArticleStockMateriel;
	}

	/**
	 * @return the locationMateriel
	 */
	public List<LocationMateriel> getLocationMateriel() {
		return locationMateriel;
	}

	/**
	 * @param locationMateriel the locationMateriel to set
	 */
	public void setLocationMateriel(List<LocationMateriel> locationMateriel) {
		this.locationMateriel = locationMateriel;
	}
	
	
}