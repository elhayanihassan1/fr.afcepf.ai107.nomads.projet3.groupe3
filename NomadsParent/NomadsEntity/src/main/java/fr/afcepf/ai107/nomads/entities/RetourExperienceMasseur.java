package fr.afcepf.ai107.nomads.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import javax.persistence.Table;

@Entity
@Table(name = "retour_experience_masseur")
public class RetourExperienceMasseur implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_retour_experience_masseur")
	private Integer idRetourExperienceMasseur;

	@Column(name = "commentaire_retour_experience_masseur")
	private String commentaireRetourExperienceMasseur;

	@Column(name = "date_retour_experience_masseur")
	private Date dateRetourExperienceMasseur;

	@Column(name = "chiffre_affaire")
	private Double chiffreAffaire;

	@Column(name = "nombre_clients_masses")
	private Integer nombreClientsMasses;
	
	@Column(name = "note_accueil_evaluation_masseur")
	private Integer noteAccueilEvaluationMasseur;

	@Column(name = "note_ambiance_evaluation_masseur")
	private Integer noteAmbianceEvaluationMasseur;

	@Column(name = "note_clientele_evaluation_masseur")
	private Integer noteClienteleEvaluationMasseur;
	
	@ManyToOne
	@JoinColumn (name= "id_masseur", referencedColumnName = "id_masseur")
	private Masseur masseur;

	@ManyToOne
	@JoinColumn(name = "id_intervention_reelle", referencedColumnName = "id_intervention_reelle")
	private InterventionReelle interventionReelle;

	/**
	 * 
	 */
	public RetourExperienceMasseur() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param idRetourExperienceMasseur
	 * @param commentaireRetourExperienceMasseur
	 * @param dateRetourExperienceMasseur
	 * @param chiffreAffaire
	 * @param nombreClientsMasses
	 * @param noteAccueilEvaluationMasseur
	 * @param noteAmbianceEvaluationMasseur
	 * @param noteClienteleEvaluationMasseur
	 * @param masseur
	 * @param interventionReelle
	 */
	public RetourExperienceMasseur(Integer idRetourExperienceMasseur, String commentaireRetourExperienceMasseur,
			Date dateRetourExperienceMasseur, Double chiffreAffaire, Integer nombreClientsMasses,
			Integer noteAccueilEvaluationMasseur, Integer noteAmbianceEvaluationMasseur,
			Integer noteClienteleEvaluationMasseur, Masseur masseur, InterventionReelle interventionReelle) {
		super();
		this.idRetourExperienceMasseur = idRetourExperienceMasseur;
		this.commentaireRetourExperienceMasseur = commentaireRetourExperienceMasseur;
		this.dateRetourExperienceMasseur = dateRetourExperienceMasseur;
		this.chiffreAffaire = chiffreAffaire;
		this.nombreClientsMasses = nombreClientsMasses;
		this.noteAccueilEvaluationMasseur = noteAccueilEvaluationMasseur;
		this.noteAmbianceEvaluationMasseur = noteAmbianceEvaluationMasseur;
		this.noteClienteleEvaluationMasseur = noteClienteleEvaluationMasseur;
		this.masseur = masseur;
		this.interventionReelle = interventionReelle;
	}

	/**
	 * @return the idRetourExperienceMasseur
	 */
	public Integer getIdRetourExperienceMasseur() {
		return idRetourExperienceMasseur;
	}

	/**
	 * @param idRetourExperienceMasseur the idRetourExperienceMasseur to set
	 */
	public void setIdRetourExperienceMasseur(Integer idRetourExperienceMasseur) {
		this.idRetourExperienceMasseur = idRetourExperienceMasseur;
	}

	/**
	 * @return the commentaireRetourExperienceMasseur
	 */
	public String getCommentaireRetourExperienceMasseur() {
		return commentaireRetourExperienceMasseur;
	}

	/**
	 * @param commentaireRetourExperienceMasseur the commentaireRetourExperienceMasseur to set
	 */
	public void setCommentaireRetourExperienceMasseur(String commentaireRetourExperienceMasseur) {
		this.commentaireRetourExperienceMasseur = commentaireRetourExperienceMasseur;
	}

	/**
	 * @return the dateRetourExperienceMasseur
	 */
	public Date getDateRetourExperienceMasseur() {
		return dateRetourExperienceMasseur;
	}

	/**
	 * @param dateRetourExperienceMasseur the dateRetourExperienceMasseur to set
	 */
	public void setDateRetourExperienceMasseur(Date dateRetourExperienceMasseur) {
		this.dateRetourExperienceMasseur = dateRetourExperienceMasseur;
	}

	/**
	 * @return the chiffreAffaire
	 */
	public Double getChiffreAffaire() {
		return chiffreAffaire;
	}

	/**
	 * @param chiffreAffaire the chiffreAffaire to set
	 */
	public void setChiffreAffaire(Double chiffreAffaire) {
		this.chiffreAffaire = chiffreAffaire;
	}

	/**
	 * @return the nombreClientsMasses
	 */
	public Integer getNombreClientsMasses() {
		return nombreClientsMasses;
	}

	/**
	 * @param nombreClientsMasses the nombreClientsMasses to set
	 */
	public void setNombreClientsMasses(Integer nombreClientsMasses) {
		this.nombreClientsMasses = nombreClientsMasses;
	}

	/**
	 * @return the noteAccueilEvaluationMasseur
	 */
	public Integer getNoteAccueilEvaluationMasseur() {
		return noteAccueilEvaluationMasseur;
	}

	/**
	 * @param noteAccueilEvaluationMasseur the noteAccueilEvaluationMasseur to set
	 */
	public void setNoteAccueilEvaluationMasseur(Integer noteAccueilEvaluationMasseur) {
		this.noteAccueilEvaluationMasseur = noteAccueilEvaluationMasseur;
	}

	/**
	 * @return the noteAmbianceEvaluationMasseur
	 */
	public Integer getNoteAmbianceEvaluationMasseur() {
		return noteAmbianceEvaluationMasseur;
	}

	/**
	 * @param noteAmbianceEvaluationMasseur the noteAmbianceEvaluationMasseur to set
	 */
	public void setNoteAmbianceEvaluationMasseur(Integer noteAmbianceEvaluationMasseur) {
		this.noteAmbianceEvaluationMasseur = noteAmbianceEvaluationMasseur;
	}

	/**
	 * @return the noteClienteleEvaluationMasseur
	 */
	public Integer getNoteClienteleEvaluationMasseur() {
		return noteClienteleEvaluationMasseur;
	}

	/**
	 * @param noteClienteleEvaluationMasseur the noteClienteleEvaluationMasseur to set
	 */
	public void setNoteClienteleEvaluationMasseur(Integer noteClienteleEvaluationMasseur) {
		this.noteClienteleEvaluationMasseur = noteClienteleEvaluationMasseur;
	}

	/**
	 * @return the masseur
	 */
	public Masseur getMasseur() {
		return masseur;
	}

	/**
	 * @param masseur the masseur to set
	 */
	public void setMasseur(Masseur masseur) {
		this.masseur = masseur;
	}

	/**
	 * @return the interventionReelle
	 */
	public InterventionReelle getInterventionReelle() {
		return interventionReelle;
	}

	/**
	 * @param interventionReelle the interventionReelle to set
	 */
	public void setInterventionReelle(InterventionReelle interventionReelle) {
		this.interventionReelle = interventionReelle;
	}

}