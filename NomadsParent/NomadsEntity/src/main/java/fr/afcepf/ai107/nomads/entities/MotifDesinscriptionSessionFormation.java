package fr.afcepf.ai107.nomads.entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import javax.persistence.Table;

@Entity
@Table (name = "motif_desinscription_session_formation")
public class MotifDesinscriptionSessionFormation implements Serializable {  	
	
	private static final long serialVersionUID = 1L;  	
	
	@Id 	
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_motif_desinscription_session_formation")
	private int idMotifDesinscriptionSessionFormation;

    @Column(name = "libelle_motif_desinscription_session_formation")
	private String  libelleMotifDesinscriptionSessionFormation;

    @OneToMany(mappedBy = "motifDesinscriptionSessionFormation", cascade = CascadeType.PERSIST)
    private List<InscriptionSessionFormation> inscriptionSessionFormation;

	public MotifDesinscriptionSessionFormation() {
		super();
		
	}

	public MotifDesinscriptionSessionFormation(int idMotifDesinscriptionSessionFormation,
			String libelleMotifDesinscriptionSessionFormation,
			List<InscriptionSessionFormation> inscriptionSessionFormation) {
		super();
		this.idMotifDesinscriptionSessionFormation = idMotifDesinscriptionSessionFormation;
		this.libelleMotifDesinscriptionSessionFormation = libelleMotifDesinscriptionSessionFormation;
		this.inscriptionSessionFormation = inscriptionSessionFormation;
	}

	/**
	 * @return the idMotifDesinscriptionSessionFormation
	 */
	public int getIdMotifDesinscriptionSessionFormation() {
		return idMotifDesinscriptionSessionFormation;
	}

	/**
	 * @param idMotifDesinscriptionSessionFormation the idMotifDesinscriptionSessionFormation to set
	 */
	public void setIdMotifDesinscriptionSessionFormation(int idMotifDesinscriptionSessionFormation) {
		this.idMotifDesinscriptionSessionFormation = idMotifDesinscriptionSessionFormation;
	}

	/**
	 * @return the libelleMotifDesinscriptionSessionFormation
	 */
	public String getLibelleMotifDesinscriptionSessionFormation() {
		return libelleMotifDesinscriptionSessionFormation;
	}

	/**
	 * @param libelleMotifDesinscriptionSessionFormation the libelleMotifDesinscriptionSessionFormation to set
	 */
	public void setLibelleMotifDesinscriptionSessionFormation(String libelleMotifDesinscriptionSessionFormation) {
		this.libelleMotifDesinscriptionSessionFormation = libelleMotifDesinscriptionSessionFormation;
	}

	/**
	 * @return the inscriptionSessionFormation
	 */
	public List<InscriptionSessionFormation> getInscriptionSessionFormation() {
		return inscriptionSessionFormation;
	}

	/**
	 * @param inscriptionSessionFormation the inscriptionSessionFormation to set
	 */
	public void setInscriptionSessionFormation(List<InscriptionSessionFormation> inscriptionSessionFormation) {
		this.inscriptionSessionFormation = inscriptionSessionFormation;
	}
    
    
}