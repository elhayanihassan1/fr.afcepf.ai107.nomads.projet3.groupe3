package fr.afcepf.ai107.nomads.entities;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import javax.persistence.Table;

@Entity
@Table(name = "motif_fin_contrat_masseur")
public class MotifFinContratMasseur implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_motif_fin_contrat_masseur")
	private int idMotifFinContratMasseur;

	@Column(name = "libelle_motif_fin_contrat_masseur")
	private String libelleMotifFinContratMasseur;
	
	@OneToMany (mappedBy = "motifFinContratMasseur", cascade = CascadeType.PERSIST,fetch = FetchType.LAZY)
	private Set<ContratMasseur> contratsMasseur;

	public MotifFinContratMasseur() {
		super();
	}

	public MotifFinContratMasseur(int idMotifFinContratMasseur, String libelleMotifFinContratMasseur,
			Set<ContratMasseur> contratsMasseur) {
		super();
		this.idMotifFinContratMasseur = idMotifFinContratMasseur;
		this.libelleMotifFinContratMasseur = libelleMotifFinContratMasseur;
		this.contratsMasseur = contratsMasseur;
	}

	public int getIdMotifFinContratMasseur() {
		return idMotifFinContratMasseur;
	}

	public void setIdMotifFinContratMasseur(int idMotifFinContratMasseur) {
		this.idMotifFinContratMasseur = idMotifFinContratMasseur;
	}

	public String getLibelleMotifFinContratMasseur() {
		return libelleMotifFinContratMasseur;
	}

	public void setLibelleMotifFinContratMasseur(String libelleMotifFinContratMasseur) {
		this.libelleMotifFinContratMasseur = libelleMotifFinContratMasseur;
	}

	public Set<ContratMasseur> getContratsMasseur() {
		return contratsMasseur;
	}

	public void setContratsMasseur(Set<ContratMasseur> contratsMasseur) {
		this.contratsMasseur = contratsMasseur;
	}

}