package fr.afcepf.ai107.nomads.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import javax.persistence.Table;

@Entity
@Table(name = "retour_experience_partenaire")
public class RetourExperiencePartenaire implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_retour_experience_partenaire")
	private int idRetourExperiencePartenaire;

	@Column(name = "libelle_retour_experience_partenaire")
	private String libelleRetourExperiencePartenaire;

	@Column(name = "date_retour_experience_partenaire")
	private Date dateRetourExperiencePartenaire;

	@Column(name = "note_relationnel")
	private Integer noteRelationnel;
	
	@Column(name = "note_ponctualite")
	private Integer notePonctualite;
	
	@Column(name = "note_qualite_massage")
	private Integer noteQualiteMassage;
	
	@ManyToOne
	@JoinColumn(name = "id_partenaire", referencedColumnName = "id_partenaire")
	private Partenaire partenaire; 

	@ManyToOne
	@JoinColumn(name = "id_intervention_reelle", referencedColumnName = "id_intervention_reelle")
	private InterventionReelle interventionReelle;


	/**
	 * 
	 */
	public RetourExperiencePartenaire() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param idRetourExperiencePartenaire
	 * @param libelleRetourExperiencePartenaire
	 * @param dateRetourExperiencePartenaire
	 * @param noteRelationnel
	 * @param notePonctualite
	 * @param noteQualiteMassage
	 * @param partenaire
	 * @param interventionReelle
	 */
	public RetourExperiencePartenaire(int idRetourExperiencePartenaire, String libelleRetourExperiencePartenaire,
			Date dateRetourExperiencePartenaire, Integer noteRelationnel, Integer notePonctualite,
			Integer noteQualiteMassage, Partenaire partenaire, InterventionReelle interventionReelle) {
		super();
		this.idRetourExperiencePartenaire = idRetourExperiencePartenaire;
		this.libelleRetourExperiencePartenaire = libelleRetourExperiencePartenaire;
		this.dateRetourExperiencePartenaire = dateRetourExperiencePartenaire;
		this.noteRelationnel = noteRelationnel;
		this.notePonctualite = notePonctualite;
		this.noteQualiteMassage = noteQualiteMassage;
		this.partenaire = partenaire;
		this.interventionReelle = interventionReelle;
	}

	/**
	 * @return the idRetourExperiencePartenaire
	 */
	public int getIdRetourExperiencePartenaire() {
		return idRetourExperiencePartenaire;
	}

	/**
	 * @param idRetourExperiencePartenaire the idRetourExperiencePartenaire to set
	 */
	public void setIdRetourExperiencePartenaire(int idRetourExperiencePartenaire) {
		this.idRetourExperiencePartenaire = idRetourExperiencePartenaire;
	}

	/**
	 * @return the libelleRetourExperiencePartenaire
	 */
	public String getLibelleRetourExperiencePartenaire() {
		return libelleRetourExperiencePartenaire;
	}

	/**
	 * @param libelleRetourExperiencePartenaire the libelleRetourExperiencePartenaire to set
	 */
	public void setLibelleRetourExperiencePartenaire(String libelleRetourExperiencePartenaire) {
		this.libelleRetourExperiencePartenaire = libelleRetourExperiencePartenaire;
	}

	/**
	 * @return the dateRetourExperiencePartenaire
	 */
	public Date getDateRetourExperiencePartenaire() {
		return dateRetourExperiencePartenaire;
	}

	/**
	 * @param dateRetourExperiencePartenaire the dateRetourExperiencePartenaire to set
	 */
	public void setDateRetourExperiencePartenaire(Date dateRetourExperiencePartenaire) {
		this.dateRetourExperiencePartenaire = dateRetourExperiencePartenaire;
	}

	/**
	 * @return the noteRelationnel
	 */
	public Integer getNoteRelationnel() {
		return noteRelationnel;
	}

	/**
	 * @param noteRelationnel the noteRelationnel to set
	 */
	public void setNoteRelationnel(Integer noteRelationnel) {
		this.noteRelationnel = noteRelationnel;
	}

	/**
	 * @return the notePonctualite
	 */
	public Integer getNotePonctualite() {
		return notePonctualite;
	}

	/**
	 * @param notePonctualite the notePonctualite to set
	 */
	public void setNotePonctualite(Integer notePonctualite) {
		this.notePonctualite = notePonctualite;
	}

	/**
	 * @return the noteQualiteMassage
	 */
	public Integer getNoteQualiteMassage() {
		return noteQualiteMassage;
	}

	/**
	 * @param noteQualiteMassage the noteQualiteMassage to set
	 */
	public void setNoteQualiteMassage(Integer noteQualiteMassage) {
		this.noteQualiteMassage = noteQualiteMassage;
	}

	/**
	 * @return the partenaire
	 */
	public Partenaire getPartenaire() {
		return partenaire;
	}

	/**
	 * @param partenaire the partenaire to set
	 */
	public void setPartenaire(Partenaire partenaire) {
		this.partenaire = partenaire;
	}

	/**
	 * @return the interventionReelle
	 */
	public InterventionReelle getInterventionReelle() {
		return interventionReelle;
	}

	/**
	 * @param interventionReelle the interventionReelle to set
	 */
	public void setInterventionReelle(InterventionReelle interventionReelle) {
		this.interventionReelle = interventionReelle;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
}