package fr.afcepf.ai107.nomads.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import javax.persistence.Table;

@Entity
@Table(name = "type_paiement")
public class TypePaiement implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_type_paiement")
	private int idTypePaiement;

	@Column(name = "libelle_type_paiement")
	private String libelleTypePaiement;

	@Column(name = "date_emission_type_paiement")
	private Date dateEmissionTypePaiement;

	@Column(name = "montant_facturation")
	private double montantFacturation;

	@Column(name = "date_validation_type_paiement")
	private Date dateValidationTypePaiement;

	@OneToMany(mappedBy = "typePaiement", cascade = CascadeType.ALL)
	private List<Prestation> prestation;

	public TypePaiement() {
		super();
		
	}

	public TypePaiement(int idTypePaiement, String libelleTypePaiement, Date dateEmissionTypePaiement,
			double montantFacturation, Date dateValidationTypePaiement, List<Prestation> prestation) {
		super();
		this.idTypePaiement = idTypePaiement;
		this.libelleTypePaiement = libelleTypePaiement;
		this.dateEmissionTypePaiement = dateEmissionTypePaiement;
		this.montantFacturation = montantFacturation;
		this.dateValidationTypePaiement = dateValidationTypePaiement;
		this.prestation = prestation;
	}

	/**
	 * @return the idTypePaiement
	 */
	public int getIdTypePaiement() {
		return idTypePaiement;
	}

	/**
	 * @param idTypePaiement the idTypePaiement to set
	 */
	public void setIdTypePaiement(int idTypePaiement) {
		this.idTypePaiement = idTypePaiement;
	}

	/**
	 * @return the libelleTypePaiement
	 */
	public String getLibelleTypePaiement() {
		return libelleTypePaiement;
	}

	/**
	 * @param libelleTypePaiement the libelleTypePaiement to set
	 */
	public void setLibelleTypePaiement(String libelleTypePaiement) {
		this.libelleTypePaiement = libelleTypePaiement;
	}

	/**
	 * @return the dateEmissionTypePaiement
	 */
	public Date getDateEmissionTypePaiement() {
		return dateEmissionTypePaiement;
	}

	/**
	 * @param dateEmissionTypePaiement the dateEmissionTypePaiement to set
	 */
	public void setDateEmissionTypePaiement(Date dateEmissionTypePaiement) {
		this.dateEmissionTypePaiement = dateEmissionTypePaiement;
	}

	/**
	 * @return the montantFacturation
	 */
	public double getMontantFacturation() {
		return montantFacturation;
	}

	/**
	 * @param montantFacturation the montantFacturation to set
	 */
	public void setMontantFacturation(double montantFacturation) {
		this.montantFacturation = montantFacturation;
	}

	/**
	 * @return the dateValidationTypePaiement
	 */
	public Date getDateValidationTypePaiement() {
		return dateValidationTypePaiement;
	}

	/**
	 * @param dateValidationTypePaiement the dateValidationTypePaiement to set
	 */
	public void setDateValidationTypePaiement(Date dateValidationTypePaiement) {
		this.dateValidationTypePaiement = dateValidationTypePaiement;
	}

	/**
	 * @return the prestation
	 */
	public List<Prestation> getPrestation() {
		return prestation;
	}

	/**
	 * @param prestation the prestation to set
	 */
	public void setPrestation(List<Prestation> prestation) {
		this.prestation = prestation;
	} 
	
	
}