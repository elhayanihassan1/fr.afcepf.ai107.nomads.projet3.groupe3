package fr.afcepf.ai107.nomads.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import javax.persistence.Table;

@Entity
@Table(name = "indisponibilite_partenaire")
public class IndisponibilitePartenaire implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_indisponibilite_partenaire")
	private int idIndisponibilitePartenaire;

	@Column(name = "date_debut_indisponibilite_partenaire")
	private Date dateDebutIndisponibilitePartenaire;

	@Column(name = "date_fin_indisponibilite_partenaire")
	private Date dateFinIndisponibilitePartenaire;
	
	@ManyToOne
	@JoinColumn(name = "id_partenaire", referencedColumnName = "id_partenaire")
	private Partenaire partenaire; 
	
	@ManyToOne
	@JoinColumn(name = "id_motif_indisponibilite_partenaire", referencedColumnName = "id_motif_indisponibilite_partenaire")
	private MotifIndisponibilitePartenaire motifIndisponibilitePartenaire;

	/**
	 * 
	 */
	public IndisponibilitePartenaire() {
		super();
	}
	
	/**
	 * @param idIndisponibilitePartenaire
	 * @param dateDebutIndisponibilitePartenaire
	 * @param dateFinIndisponibilitePartenaire
	 * @param partenaire
	 * @param motifIndisponibilitePartenaire
	 */
	public IndisponibilitePartenaire(int idIndisponibilitePartenaire, Date dateDebutIndisponibilitePartenaire,
			Date dateFinIndisponibilitePartenaire, Partenaire partenaire,
			MotifIndisponibilitePartenaire motifIndisponibilitePartenaire) {
		super();
		this.idIndisponibilitePartenaire = idIndisponibilitePartenaire;
		this.dateDebutIndisponibilitePartenaire = dateDebutIndisponibilitePartenaire;
		this.dateFinIndisponibilitePartenaire = dateFinIndisponibilitePartenaire;
		this.partenaire = partenaire;
		this.motifIndisponibilitePartenaire = motifIndisponibilitePartenaire;
	}

	/**
	 * @return the idIndisponibilitePartenaire
	 */
	public int getIdIndisponibilitePartenaire() {
		return idIndisponibilitePartenaire;
	}

	/**
	 * @param idIndisponibilitePartenaire the idIndisponibilitePartenaire to set
	 */
	public void setIdIndisponibilitePartenaire(int idIndisponibilitePartenaire) {
		this.idIndisponibilitePartenaire = idIndisponibilitePartenaire;
	}

	/**
	 * @return the dateDebutIndisponibilitePartenaire
	 */
	public Date getDateDebutIndisponibilitePartenaire() {
		return dateDebutIndisponibilitePartenaire;
	}

	/**
	 * @param dateDebutIndisponibilitePartenaire the dateDebutIndisponibilitePartenaire to set
	 */
	public void setDateDebutIndisponibilitePartenaire(Date dateDebutIndisponibilitePartenaire) {
		this.dateDebutIndisponibilitePartenaire = dateDebutIndisponibilitePartenaire;
	}

	/**
	 * @return the dateFinIndisponibilitePartenaire
	 */
	public Date getDateFinIndisponibilitePartenaire() {
		return dateFinIndisponibilitePartenaire;
	}

	/**
	 * @param dateFinIndisponibilitePartenaire the dateFinIndisponibilitePartenaire to set
	 */
	public void setDateFinIndisponibilitePartenaire(Date dateFinIndisponibilitePartenaire) {
		this.dateFinIndisponibilitePartenaire = dateFinIndisponibilitePartenaire;
	}

	/**
	 * @return the partenaire
	 */
	public Partenaire getPartenaire() {
		return partenaire;
	}

	/**
	 * @param partenaire the partenaire to set
	 */
	public void setPartenaire(Partenaire partenaire) {
		this.partenaire = partenaire;
	}

	/**
	 * @return the motifIndisponibilitePartenaire
	 */
	public MotifIndisponibilitePartenaire getMotifIndisponibilitePartenaire() {
		return motifIndisponibilitePartenaire;
	}

	/**
	 * @param motifIndisponibilitePartenaire the motifIndisponibilitePartenaire to set
	 */
	public void setMotifIndisponibilitePartenaire(MotifIndisponibilitePartenaire motifIndisponibilitePartenaire) {
		this.motifIndisponibilitePartenaire = motifIndisponibilitePartenaire;
	}



	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	
}